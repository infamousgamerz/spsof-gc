/* SPSOF.C - A proxy for the Gamecube version of Phantasy Star Online.

  DNS procedures (c) that guy who wrote dproxy. */

/* define _MT so that _beginthread( ) is available  */

/* Updated source to fix Gamecube compilation problem. 08/21/2007 - Sodaboy */

#ifndef _MT 
#define _MT 
#endif 

#define _CRT_SECURE_NO_DEPRECATE

// FireWall only? (Superceded by GOODY_GOODY)

//#define FIREWALL_ONLY

// Allow remote connections to proxy?

//#define REMOTE_MODE

// Disable chat windows?

//#define NO_CHAT

// Disable all windows?

//#define NO_WINDOWS

// Disable malicious things?

//#define	GOODY_GOODY

// Disable .IND support?

#define NO_IND

// Connect to Schthack by default?

#define SCHTHACK

//#define EPISODE_3

//#define USE_PRIVATE_IP

//#define SKIP_DNS

/* Cranberry mode on? (Reroutes authentication to a different login server.) */

#ifdef GOODY_GOODY
#ifndef NO_IND
#define NO_IND
#endif
#endif

int cranberry_mode = 0;
unsigned short cranberry_port;

/* Version of program. */

#define USA_VERSION

/* Japan connection parameters. */

#ifdef JAPAN_VERSION

#ifndef EPISODE_3
#define PSO_PORT 9001
#else	
#define PSO_PORT 9003
#endif

int pso_region = 0;

#endif

/* USA connection parameters. */

#ifdef USA_VERSION

#ifndef EPISODE_3
#define PSO_PORT 9100
#else
#define PSO_PORT 9103
#endif

int pso_region = 1;

#endif


/* European connection parameters. */

#ifdef EUROPE_VERSION

#ifndef EPISODE_3
#define PSO_PORT 9200
#else
#define PSO_PORT 9203
#endif

int pso_region = 2;

#endif

#define SPSOF_VERSION "2.66"

#include "dproxy.h"
#include "stdio.h"
#include "windows.h"
#include "process.h"
#include "resource.h"
#define _CRT_SECURE_NO_DEPRECATE
#include "string.h"
#include <winuser.h>
#include <winbase.h>

/* I don't even know why I can't just define the _WIN32_WINNT and include winbase.h for this one. */

WINBASEAPI
BOOL
WINAPI
IsDebuggerPresent(
    VOID
    );

/* Size check table for 0x6x commands. */

unsigned short size_check_table [] =
{ 0x00, 0x00,
0x01, 0x00,
0x02, 0x00,
0x03, 0x00,
0x04, 0x00,
0x05, 0x10,
0x06, 0x98,
0x07, 0x48,
0x08, 0x00,
0x09, 0x00,
0x0A, 0x00,
0x0B, 0x00,
0x0C, 0x00,
0x0D, 0x10,
0x0E, 0x00,
0x0F, 0x00,
0x10, 0x00,
0x11, 0x00,
0x12, 0x00,
0x13, 0x00,
0x14, 0x00,
0x15, 0x00,
0x16, 0x00,
0x17, 0x00,
0x18, 0x00,
0x19, 0x00,
0x1A, 0x00,
0x1B, 0x00,
0x1C, 0x00,
0x1D, 0x00,
0x1E, 0x00,
0x1F, 0x0C,
0x20, 0x1C,
0x21, 0x00,
0x22, 0x08,
0x23, 0x08,
0x24, 0x00,
0x25, 0x00,
0x26, 0x00,
0x27, 0x00,
0x28, 0x00,
0x29, 0x00,
0x2A, 0x00,
0x2B, 0x00,
0x2C, 0x18,
0x2D, 0x08,
0x2E, 0x00,
0x2F, 0x00,
0x30, 0x00,
0x31, 0x00,
0x32, 0x00,
0x33, 0x00,
0x34, 0x00,
0x35, 0x00,
0x36, 0x00,
0x37, 0x00,
0x38, 0x00,
0x39, 0x00,
0x3A, 0x08,
0x3B, 0x08,
0x3C, 0x00,
0x3D, 0x00,
0x3E, 0x1C,
0x3F, 0x1C,
0x40, 0x14,
0x41, 0x00,
0x42, 0x10,
0x43, 0x00,
0x44, 0x00,
0x45, 0x00,
0x46, 0x00,
0x47, 0x00,
0x48, 0x00,
0x49, 0x00,
0x4A, 0x00,
0x4B, 0x00,
0x4C, 0x00,
0x4D, 0x00,
0x4E, 0x00,
0x4F, 0x00,
0x50, 0x00,
0x51, 0x00,
0x52, 0x10,
0x53, 0x00,
0x54, 0x00,
0x55, 0x00,
0x56, 0x00,
0x57, 0x00,
0x58, 0x0C,
0x59, 0x00,
0x5A, 0x00,
0x5B, 0x00,
0x5C, 0x00,
0x5D, 0x00,
0x5E, 0x00,
0x5F, 0x00,
0x60, 0x00,
0x61, 0x00,
0x62, 0x00,
0x63, 0x00,
0x64, 0x00,
0x65, 0x00,
0x66, 0x00,
0x67, 0x00,
0x68, 0x00,
0x69, 0x00,
0x6A, 0x00,
0x6B, 0x00,
0x6C, 0x00,
0x6D, 0x00,
0x6E, 0x00,
0x6F, 0x208,
0x70, 0x00,
0x71, 0x08,
0x72, 0x00,
0x73, 0x00,
0x74, 0x24,
0x75, 0x00,
0x76, 0x00,
0x77, 0x00,
0x78, 0x00,
0x79, 0x1C,
0x7A, 0x00,
0x7B, 0x00,
0x7C, 0x00,
0x7D, 0x00,
0x7E, 0x00,
0x7F, 0x00,
0x80, 0x00,
0x81, 0x00,
0x82, 0x00,
0x83, 0x00,
0x84, 0x00,
0x85, 0x00,
0x86, 0x00,
0x87, 0x00,
0x88, 0x00,
0x89, 0x00,
0x8A, 0x00,
0x8B, 0x00,
0x8C, 0x00,
0x8D, 0x00,
0x8E, 0x00,
0x8F, 0x00,
0x90, 0x00,
0x91, 0x00,
0x92, 0x00,
0x93, 0x00,
0x94, 0x00,
0x95, 0x00,
0x96, 0x00,
0x97, 0x00,
0x98, 0x00,
0x99, 0x00,
0x9A, 0x00,
0x9B, 0x00,
0x9C, 0x00,
0x9D, 0x00,
0x9E, 0x00,
0x9F, 0x00,
0xA0, 0x00,
0xA1, 0x00,
0xA2, 0x00,
0xA3, 0x00,
0xA4, 0x00,
0xA5, 0x00,
0xA6, 0x00,
0xA7, 0x00,
0xA8, 0x00,
0xA9, 0x00,
0xAA, 0x00,
0xAB, 0x0C,
0xAC, 0x00,
0xAD, 0x00,
0xAE, 0x14,
0xAF, 0x0C,
0xB0, 0x0C,
0xB1, 0x00,
0xB2, 0x00,
0xB3, 0x00,
0xB4, 0x00,
0xB5, 0x00,
0xB6, 0x00,
0xB7, 0x00,
0xB8, 0x00,
0xB9, 0x00,
0xBA, 0x00,
0xBB, 0x00,
0xBC, 0x00,
0xBD, 0x00,
0xBE, 0x00,
0xBF, 0x0C,
0xC0, 0x00,
0xC1, 0x00,
0xC2, 0x00,
0xC3, 0x00,
0xC4, 0x00,
0xC5, 0x00,
0xC6, 0x00,
0xC7, 0x00,
0xC8, 0x00,
0xC9, 0x00,
0xCA, 0x00,
0xCB, 0x00,
0xCC, 0x00,
0xCD, 0x00,
0xCE, 0x00,
0xCF, 0x00,
0xD0, 0x00,
0xD1, 0x00,
0xD2, 0x00,
0xD3, 0x00,
0xD4, 0x00,
0xD5, 0x00,
0xD6, 0x00,
0xD7, 0x00,
0xD8, 0x00,
0xD9, 0x00,
0xDA, 0x00,
0xDB, 0x00,
0xDC, 0x00,
0xDD, 0x00,
0xDE, 0x00,
0xDF, 0x00,
0xE0, 0x00,
0xE1, 0x00,
0xE2, 0x00,
0xE3, 0x00,
0xE4, 0x00,
0xE5, 0x00,
0xE6, 0x00,
0xE7, 0x00,
0xE8, 0x00,
0xE9, 0x00,
0xEA, 0x00,
0xEB, 0x00,
0xEC, 0x00,
0xED, 0x00,
0xEE, 0x00,
0xEF, 0x00,
0xF0, 0x00,
0xF1, 0x00,
0xF2, 0x00,
0xF3, 0x00,
0xF4, 0x00,
0xF5, 0x00,
0xF6, 0x00,
0xF7, 0x00,
0xF8, 0x00,
0xF9, 0x00,
0xFA, 0x00,
0xFB, 0x00,
0xFC, 0x00,
0xFD, 0x00,
0xFE, 0x00,
0xFF, 0x00 };

unsigned char weapon_check[] = {
0x00, /* 0x00 */
0x08, /* 0x01 */
0x07, /* 0x02 */
0x09, /* 0x03 */
0x08, /* 0x04 */ 
0x08, /* 0x05 */
0x08, /* 0x06 */
0x0D, /* 0x07 */
0x07, /* 0x08 */
0x07, /* 0x09 */
0x07, /* 0x0A */
0x07, /* 0x0B */
0x07, /* 0x0C */
0x03, /* 0x0D */
0x02, /* 0x0E */
0x03, /* 0x0F */
0x07, /* 0x10 */
0x01, /* 0x11 */
0x00, /* 0x12 */
0x00, /* 0x13 */
0x02, /* 0x14 */
0x01, /* 0x15 */
0x00, /* 0x16 */
0x00, /* 0x17 */
0x00, /* 0x18 */
0x00, /* 0x19 */
0x00, /* 0x1A */
0x00, /* 0x1B */
0x00, /* 0x1C */
0x00, /* 0x1D */
0x00, /* 0x1E */
0x00, /* 0x1F */
0x01, /* 0x20 */
0x00, /* 0x21 */
0x01, /* 0x22 */
0x00, /* 0x23 */
0x00, /* 0x24 */
0x00, /* 0x25 */
0x00, /* 0x26 */
0x00, /* 0x27 */
0x00, /* 0x28 */
0x00, /* 0x29 */
0x00, /* 0x2A */
0x00, /* 0x2B */
0x00, /* 0x2C */
0x00, /* 0x2D */
0x00, /* 0x2E */
0x01, /* 0x2F */
0x01, /* 0x30 */
0x00, /* 0x31 */
0x00, /* 0x32 */
0x00, /* 0x33 */
0x00, /* 0x34 */
0x00, /* 0x35 */
0x00, /* 0x36 */
0x00, /* 0x37 */
0x00, /* 0x38 */
0x00, /* 0x39 */
0x00, /* 0x3A */
0x00, /* 0x3B */
0x00, /* 0x3C */
0x00, /* 0x3D */
0x00, /* 0x3E */
0x00, /* 0x3F */
0x00, /* 0x40 */
0x00, /* 0x41 */
0x01, /* 0x42 */
0x01, /* 0x43 */
0x00, /* 0x44 */
0x01, /* 0x45 */
0x00, /* 0x46 */
0x00, /* 0x47 */
0x00, /* 0x48 */
0x00, /* 0x49 */
0x00, /* 0x4A */
0x01, /* 0x4B */
0x00, /* 0x4C */
0x00, /* 0x4D */
0x01, /* 0x4E */
0x00, /* 0x4F */
0x00, /* 0x50 */
0x00, /* 0x51 */
0x00, /* 0x52 */
0x00, /* 0x53 */
0x00, /* 0x54 */
0x00, /* 0x55 */
0x01, /* 0x56 */
0x00, /* 0x57 */
0x00, /* 0x58 */
0x00, /* 0x59 */
0x00, /* 0x5A */
0x00, /* 0x5B */
0x00, /* 0x5C */
0x00, /* 0x5D */
0x00, /* 0x5E */
0x00, /* 0x5F */
0x00, /* 0x60 */
0x00, /* 0x61 */
0x00, /* 0x62 */
0x00, /* 0x63 */
0x00, /* 0x64 */
0x00, /* 0x65 */
0x00, /* 0x66 */
0x00, /* 0x67 */
0x00, /* 0x68 */
0x00, /* 0x69 */
0x00, /* 0x6A */
0x00, /* 0x6B */
0x00, /* 0x6C */
0x01, /* 0x6D */
0x01, /* 0x6E */
0x00, /* 0x6F */
0x00, /* 0x70 */
0x00, /* 0x71 */
0x00, /* 0x72 */
0x00, /* 0x73 */
0x00, /* 0x74 */
0x00, /* 0x75 */
0x00, /* 0x76 */
0x00, /* 0x77 */
0x00, /* 0x78 */
0x00, /* 0x79 */
0x00, /* 0x7A */
0x00, /* 0x7B */
0x00, /* 0x7C */
0x00, /* 0x7D */
0x00, /* 0x7E */
0x00, /* 0x7F */
0x00, /* 0x80 */
0x00, /* 0x81 */
0x00, /* 0x82 */
0x00, /* 0x83 */
0x00, /* 0x84 */
0x00, /* 0x85 */
0x00, /* 0x86 */
0x00, /* 0x87 */
0x00, /* 0x88 */
0x03, /* 0x89 */
0x02, /* 0x8A */
0x03, /* 0x8B */
0x04, /* 0x8C */
0x00, /* 0x8D */
0x01, /* 0x0E */
0x08, /* 0x8F */
0x08, /* 0x90 */
0x00, /* 0x91 */
0x00, /* 0x92 */
0x09, /* 0x93 */
0x00, /* 0x94 */
0x00, /* 0x95 */
0x00, /* 0x96 */
0x00, /* 0x97 */
0x00, /* 0x98 */
0x00, /* 0x99 */
0x00, /* 0x9A */
0x00, /* 0x9B */
0x00, /* 0x9C */
0x00, /* 0x9D */
0x00, /* 0x9E */
0x00, /* 0x9F */
0x00, /* 0xA0 */
0x00, /* 0xA1 */
0x02, /* 0xA2 */
0x00, /* 0xA3 */
0x00, /* 0xA4 */
0x00, /* 0xA5 */
0x00, /* 0xA6 */
0x00, /* 0xA7 */
0x00, /* 0xA8 */
0x00  /* 0xA9 */
};

unsigned short schtsrv_port = 9100;
unsigned char schtsrv_addr[4];
unsigned short sega_port = 9100;
unsigned char sega_addr[4];
int first_connect = 1;

unsigned char chat_pkt[] = { 0x06, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x45 };
unsigned char e3chat_pkt[] = { 0x06, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4E, 0x09, 0x45 };
unsigned char quest_pkt[] = { 0x10, 0x00, 0x0C, 0x00, 0x00, 0x01, 0x0E, 0x00, 0x4D, 0xFF, 0xFF, 0xFF };
unsigned char sega_pkt[] = { 0x00, 0x00, 0x12, 0x00, 0xDE, 0xAD, 0xBE, 0xEF, 0x04, 0x0F, 0x53, 0x48, 0x49, 0x50, 0x2F, 0x53, 0x45, 0x47, 0x41, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char schthack_pkt[] = { 0x00, 0x00, 0x12, 0x00, 0xDE, 0xAD, 0xBE, 0xEF, 0x04, 0x0F, 0x53, 0x48, 0x49, 0x50, 0x2F, 0x53, 0x63, 0x68, 0x74, 0x68, 0x61, 0x63, 0x6B, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char schtfixup[] = { 0x96, 0x00, 0x0C, 0x00, 0xD8, 0xBE, 0xEB, 0x0A, 0x88, 0x00, 0x00, 0x00 };
unsigned char ep3fixup[] = { 0xD6, 0x00, 0x04, 0x00 };
unsigned char noinfo_pkt[] = { 0x11, 0x67, 0x28, 0x00, 0xF0, 0xDF, 0xFF, 0xBF, 0x30, 0xE2, 0xFF, 0xBF, 0x53, 0x6F, 0x72, 0x72, 0x79, 0x2C, 0x20, 0x20, 0x20, 0x20, 0x0A, 0x4E, 0x6F, 0x20, 0x69, 0x6E, 0x66, 0x6F, 0x72, 0x6D, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x00, 0x00, 0x00 };
unsigned char schtsrv_pkt[] = { 0x19, 0xAF, 0x0C, 0x00, 0x40, 0x5F, 0x79, 0x60, 0xF0, 0x23, 0x31, 0x08 };
unsigned char silent_pkt[] = { 0x60, 0x00, 0xE8, 0x00, 0x41, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char rappy_pkt[] = { 0x10, 0x00, 0x0C, 0x00, 0x00, 0x01, 0x0E, 0x00, 0x15, 0xFF, 0xFF, 0xFF };
unsigned char warp_pkt[] = { 0x60, 0x00, 0x0C, 0x00, 0x94, 0x02, 0x00, 0x0B, 0x00, 0x00, 0x01, 0x00 };
unsigned char pk_pkt[] = { 0x60, 0x00, 0x0C, 0x00, 0x9A, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF };
unsigned char hide_pkt[] = { 0x60, 0x00, 0x08, 0x00, 0x22, 0x01, 0x00, 0x00 };
unsigned char show_pkt[] = { 0x60, 0x00, 0x08, 0x00, 0x23, 0x01, 0x00, 0x00 };
unsigned char picky_pkt[] = { 0x6D, 0x00, 0x10, 0x00, 0x58, 0x02, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 0x74, 0x00, 0x00, 0x00 };
unsigned char dc_pkt[] = { 0x6C, 0x00, 0x98, 0x00, 0x0B, 0x25, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x90, 0x47, 0x7E, 0x01, 0x42, 0x72, 0x6F, 0x6F, 0x6D, 0x6F, 0x70, 0x73, 0x20, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x33, 0x38, 0x20, 0x43, 0x6F, 0x72, 0x6E, 0x77, 0x61, 0x6C, 0x6C, 0x20, 0x47, 0x61, 0x72, 0x64, 0x65, 0x6E, 0x73, 0x20, 0x55, 0x58, 0x42, 0x52, 0x49, 0x44, 0x47, 0x45, 0x2C, 0x4D, 0x69, 0x64, 0x64, 0x6C, 0x65, 0x73, 0x65, 0x78, 0x20, 0x55, 0x42, 0x38, 0x20, 0x33, 0x4A, 0x54, 0x20, 0x2D, 0x48, 0x45, 0x57, 0x48, 0x4F, 0x43, 0x4F, 0x52, 0x52, 0x55, 0x50, 0x54, 0x53, 0x20, 0x4D, 0x65, 0x67, 0x61, 0x43, 0x6F, 0x72, 0x70, 0x2C, 0x20, 0x49, 0x6E, 0x63, 0x2E, 0x20, 0x2D, 0x20, 0x20, 0x43, 0x6F, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68, 0x74, 0xA9, 0x20, 0x32, 0x30, 0x31, 0x33, 0x20, 0x2D, 0x20, 0x77, 0x77, 0x77, 0x2E, 0x49, 0x6E, 0x66, 0x61, 0x6D, 0x6F, 0x75, 0x73, 0x47, 0x61, 0x6D, 0x65, 0x72, 0x7A, 0x2E, 0x6E, 0x65, 0x74 };
unsigned char fakequest_pkt[] = { 0x10, 0x00, 0x0C, 0x00, 0x00, 0x01, 0x0E, 0x00, 0x3A, 0xFF, 0xFF, 0xFF };
unsigned char getgame_pkt[] = { 0x08, 0x00, 0x04, 0x00 };
unsigned char bigb_pkt[] = { 0x06, 0x00, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char qfsod_pkt[] = { 0x6D, 0x00, 0x08, 0x00, 0x73, 0x01, 0x03, 0x00 };
unsigned char schat_pkt[] = { 0x60, 0x00, 0x48, 0x00, 0x07, 0x11, 0xA0, 0xDE, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x62, 0x03, 0x62, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0x09, 0x16, 0x1B, 0x00, 0x09, 0x2B, 0x1B, 0x01, 0x37, 0x20, 0x2C, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02 };
unsigned char fsod_pkt[] = { 0x6D, 0x00, 0x0C, 0x00, 0x6E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char crash_pkt[] = { 0x6D, 0x00, 0x0C, 0x00, 0x18, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char wschat_pkt[] = { 0x60, 0x00, 0x24, 0x00, 0x74, 0x08, 0x00, 0x04, 0x02, 0x00, 0x01, 0x00, 0x26, 0x01, 0xC7, 0x02, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char run_pkt[] = { 0x60, 0x00, 0x0C, 0x00, 0x58, 0x02, 0x08, 0x00, 0x02, 0x00, 0x01, 0x00 };
unsigned char walk_pkt[] = { 0x60, 0x00, 0x0C, 0x00, 0x58, 0x02, 0x08, 0x00, 0x01, 0x00, 0x01, 0x00 };
unsigned char groove_pkt[] =   { 0x60, 0x00, 0x0C, 0x00, 0x58, 0x02, 0x08, 0x00, 0x2A, 0x00, 0x01, 0x00 };
unsigned char fakekill_pkt[] = { 0x60, 0x00, 0x0C, 0x00, 0x58, 0x02, 0x04, 0x00, 0x12, 0x00, 0x00, 0x00 };
unsigned char action_pkt[] =   { 0x60, 0x00, 0x0C, 0x00, 0x58, 0x02, 0x04, 0x00, 0x12, 0x00, 0x00, 0x00 };
unsigned char npc_pkt[] = { 0x60, 0x00, 0x20, 0x00, 0x69, 0x07, 0x01, 0x01, 0x00, 0x00, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char lwarp_pkt[] = { 0x84, 0x00, 0x0C, 0x00, 0x01, 0x00, 0x1A, 0x00, 0xC0, 0xFF, 0xFF, 0xFF };
unsigned char color_data[3] = { 0x09, 0x43, 0x39 };
unsigned char reload_pkt[] = { 0x62, 0x01, 0x08, 0x00, 0x71, 0x01, 0x00, 0x00 }; 
unsigned char npcfsod_pkt[] = { 0x60, 0x00, 0x28, 0x00, 0x58, 0x02, 0x07, 0x00, 0x2C, 0x00, 0x00, 0x00, 0x69, 0x07, 0x01, 0x01, 0x00, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char juke_pkt[] = { 0x60, 0x00, 0x0C, 0x00, 0xBF, 0x02, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00 };
unsigned char rank_pkt[] = { 0xB7, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF };
unsigned char steal_schat[] = { 0x60, 0x00, 0x48, 0x00, 0x07, 0x11, 0xA0, 0xDE, 0x00, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x62, 0x03, 0x62, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0x09, 0x16, 0x1B, 0x00, 0x09, 0x2B, 0x1B, 0x01, 0x37, 0x20, 0x2C, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02, 0xFF, 0x00, 0x00, 0x02 };
unsigned char event_pkt[] = { 0xDA, 0x00, 0x04, 0x00 };

//HEWHOCORRUPTS BlueBurst Lobby FSOD
unsigned char molest_pkt[] = { 0x60, 0x00, 0x98, 0x00, 0xCD, 0x25, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x90, 0x47, 0x7E, 0x01, 0x42, 0x72, 0x6F, 0x6F, 0x6D, 0x6F, 0x70, 0x73, 0x20, 0x41, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x33, 0x38, 0x20, 0x43, 0x6F, 0x72, 0x6E, 0x77, 0x61, 0x6C, 0x6C, 0x20, 0x47, 0x61, 0x72, 0x64, 0x65, 0x6E, 0x73, 0x20, 0x55, 0x58, 0x42, 0x52, 0x49, 0x44, 0x47, 0x45, 0x2C, 0x4D, 0x69, 0x64, 0x64, 0x6C, 0x65, 0x73, 0x65, 0x78, 0x20, 0x55, 0x42, 0x38, 0x20, 0x33, 0x4A, 0x54, 0x20, 0x2D, 0x48, 0x45, 0x57, 0x48, 0x4F, 0x43, 0x4F, 0x52, 0x52, 0x55, 0x50, 0x54, 0x53, 0x20, 0x4D, 0x65, 0x67, 0x61, 0x43, 0x6F, 0x72, 0x70, 0x2C, 0x20, 0x49, 0x6E, 0x63, 0x2E, 0x20, 0x2D, 0x20, 0x20, 0x43, 0x6F, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68, 0x74, 0xA9, 0x20, 0x32, 0x30, 0x31, 0x33, 0x20, 0x2D, 0x20, 0x77, 0x77, 0x77, 0x2E, 0x49, 0x6E, 0x66, 0x61, 0x6D, 0x6F, 0x75, 0x73, 0x47, 0x61, 0x6D, 0x65, 0x72, 0x7A, 0x2E, 0x6E, 0x65, 0x74 }


unsigned char gc_data[152] = { 0x62, 0x00, 0x98, 0x00, 0x06, 0x25, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x44, 0xA0, 0x48, 0x14,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x45, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x02, 0x0A };

unsigned char gcfsod_pkt[160] = { 0x62, 0x03, 0xA0, 0x00, 0x06, 0x25, 0x1F, 0x00, 0x00, 0x00, 0x01, 0x00, 0x12, 0xB3, 0x48, 0x14, 
0x53, 0x65, 0x72, 0x61, 0x20, 0x42, 0x6C, 0x75, 0x65, 0x20, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

unsigned char arrow_pkt[] = { 0x89, 0x08, 0x04, 0x00 };
unsigned char god_pkt[] = { 0x60, 0x00, 0x0C, 0x00, 0x9A, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF };

/* This is the timeout for select.  It's needed or else user key presses won't be processed. */

const struct timeval pso_timeout = { 0, 16600 }; /* Program running @ 60hz */

/* Address of Schthack */

//unsigned char scht_addr[4] = { 207, 210, 93, 28 };

/* Address of the proxy server. */

unsigned char proxy_addr[4] = { 0xC0, 0xA8, 0x01, 0x64 };

/* String sent to server to retrieve IP address. */

char* HTTP_REQ = "GET http://www.pioneer2.net/whoami.php HTTP/1.0\r\n\r\n\r\n";

// Some of Myria's defines

#define htodl(x) (x)
#define dtohl(x) (x)

#define KEYDOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)

#define CALLING cdecl

#define BYTE unsigned char
#define DWORD unsigned

// Magic number
enum { MAGIC = 0x5D588B65 };
// Size of the table
enum { TABLE_SIZE = 521 };
// Mark point for the mangle operation
enum { MANGLE_MARK = 489 };

// The Gamecube cipher implementation
typedef struct
{
	DWORD next;
	DWORD table[TABLE_SIZE];
	DWORD *current;
	DWORD seed;
	BYTE next_offset;

	// Mangle the table; this resets current
	void (CALLING *mangle)(void);

	void (CALLING *advance)(void);

	void (CALLING *initialize)(const BYTE *key, size_t keylen);

} GamecubeCipher;

unsigned char create_pkt[] = { 
0x6C, 0x00, 0x2C, 0x00, 0x5D, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00 };

unsigned long currentitemid = 0x0081000D;


/*****************************************************************************/
/* Global variables */
/*****************************************************************************/

  int crypt_on = 0;
  unsigned char exam_buf[BUF_SIZE+32];
  unsigned char client_buf[BUF_SIZE+32];
  unsigned char server_buf[BUF_SIZE+32];
  unsigned char save_buf[12];

  /* Schthack Fix #4 */
  unsigned char fixup_buf[32];

  GamecubeCipher server_to_client, client_to_server, proxy_to_client, proxy_to_server;

int client_data_in_buf = 0, server_data_in_buf = 0;

/* function protos */
void tcp_listen (int sockfd);
int tcp_accept (int sockfd, struct sockaddr *client_addr, int *addr_len );
int tcp_sock_connect(char* dest_addr, int port);
int tcp_sock_open(struct in_addr ip, int port);
int tcp_packet_write(int sockfd, struct tcp_packet *tcp_pkt);
int tcp_packet_read(int sockfd, struct tcp_packet *tcp_pkt);
int udp_sock_open(struct in_addr ip, int port);
int udp_packet_write(int sockfd, struct udp_packet *udp_pkt);
int udp_packet_read(int sockfd, struct udp_packet *udp_pkt);
int dns_is_query(struct udp_packet *udp_pkt);
int dns_get_opcode(struct udp_packet *udp_pkt);
void dns_send_reply(int sockfd, struct udp_packet *pkt,char *query);
void dns_reverse_lookup( int sockfd, struct udp_packet *pkt, char *query );

void encode_domain_name(char *name, char encoded_name[BUF_SIZE]);
void decode_domain_name(char name[BUF_SIZE]);
void reverse_domain_name(char name[BUF_SIZE]);

/* encryption protos */

void CALLING gcmangle(void);
void CALLING gcinitialize(const BYTE *key, size_t keylen);
void CALLING gcadvance(void);
void encryptcopy(BYTE *dest, const BYTE *src, DWORD size);
void encrypt(BYTE *data, DWORD size);
void decrypt(BYTE *data, DWORD size);
void decryptcopy(BYTE *dest, const BYTE *src, DWORD size);
void peek_decrypt(DWORD *data);

/* Pointer to the cipher in use. */

GamecubeCipher *cipher_ptr;

void send_to_server(int sock, char* packet);
int receive_from_server(int sock, char* packet);

/* Fun procedure. */

void client_broadcast(char *fmt, ...);
void top_broadcast(char *fmt, ...);

int login_message = 1;

void swapfour (unsigned char* buf)
{
	unsigned short tmp;

	tmp = *(unsigned short*) &buf[2];
	*(unsigned short*) &buf[2] = *(unsigned short*) &buf[0];
	*(unsigned short*) &buf[0] = tmp;
}

  
char  hexTable[16] =
  { '0','1','2','3','4','5','6','7',
    '8','9','A','B','C','D','E','F' };

char hexTmp[3];

char* hexByte ( unsigned char z )

{
	hexTmp[0] = hexTable[z >>  4];
	hexTmp[1] = hexTable[z &  15];
	hexTmp[2] = 0;
	return &hexTmp[0];
}

char* hexWord ( unsigned short z )

{
	unsigned short y;
	
	y = z >> 8;

	hexTmp[0] = hexTable[y >>  4];
	hexTmp[1] = hexTable[y &  15];

	y = z << 8; y >>= 8;

	hexTmp[2] = hexTable[y >>  4];
	hexTmp[3] = hexTable[y &  15];
	hexTmp[4] = 0;

	return &hexTmp[0];
}

char* hexDword ( unsigned z )

{
	unsigned y = z >> 24;

	hexTmp[0] = hexTable[y >>  4];
	hexTmp[1] = hexTable[y &  15];

	y = z << 8; y >>= 24;

	hexTmp[2] = hexTable[y >>  4];
	hexTmp[3] = hexTable[y &  15];

	y = z << 16; y >>= 24;

	hexTmp[4] = hexTable[y >>  4];
	hexTmp[5] = hexTable[y &  15];

	y = z << 24; y >>= 24;

	hexTmp[6] = hexTable[y >>  4];
	hexTmp[7] = hexTable[y &  15];
	
	hexTmp[8] = 0;

	return &hexTmp[0];
}

unsigned char hexToByte ( char* hs )
{
  unsigned b;
  
  if ( hs[0] < 58 ) b = (hs[0] - 48); else b = (hs[0] - 55);
  b *= 16;
  if ( hs[1] < 58 ) b += (hs[1] - 48); else b += (hs[1] - 55);
  return (unsigned char) b;
}

char pkt_hex[BUF_SIZE*4];

void display_packet ( char* buf, int len )
{
 int c,c2,c3,c5;
 unsigned char c4;
 
	c2 = c3 = 0;
	pkt_hex[c3++] = 40;
	pkt_hex[c3++] = 48;
	pkt_hex[c3++] = 48;
	pkt_hex[c3++] = 48;
	pkt_hex[c3++] = 48;
	pkt_hex[c3++] = 41;
	pkt_hex[c3++] = 32;
	for (c=0;c<len;c++)
	{
		c4 = buf[c];
		if (c2==16)
		{
			pkt_hex[c3++] = 32;			
			c5 = c - 16;
			for (c5;c5<c;c5++)
			{
				if ((unsigned char) buf[c5] < 0x20) pkt_hex[c3++] = 46; else
					pkt_hex[c3++] = buf[c5];
			}
			pkt_hex[c3++] = 0x0A;
			pkt_hex[c3++] = 40;
			hexWord ( (unsigned short) c );
			pkt_hex[c3++] = hexTmp[0];
			pkt_hex[c3++] = hexTmp[1];
			pkt_hex[c3++] = hexTmp[2];
			pkt_hex[c3++] = hexTmp[3];
			pkt_hex[c3++] = 41;
			pkt_hex[c3++] = 32;
			c2 = 0;
		}
	    pkt_hex[c3++] = hexTable[c4 >> 4];
	    pkt_hex[c3++] = hexTable[c4 & 15];
	    pkt_hex[c3++] = 0x20;
		c2++;
	}

	if (c2)
	{
		pkt_hex[c3++] = 0x20;
		for (c5=c2;c5<16;c5++)
		{
			pkt_hex[c3++] = 0x20;
			pkt_hex[c3++] = 0x20;
			pkt_hex[c3++] = 0x20;
		}
		c5 = c - c2;
		for (c5;c5<c;c5++)
		{
			if ((unsigned char) buf[c5] < 0x20) pkt_hex[c3++] = 46; else
				pkt_hex[c3++] = buf[c5];
		}

	}

	pkt_hex[c3++] = 0x0A;
	pkt_hex[c3++] = 0x00;
	debug ("%s", &pkt_hex[0]);
}

// this function is called by a new thread 

int bDone;

unsigned char custom_buf[BUF_SIZE+32];
int custom_data_in_buf = 0;

int isHex ( char* hs )
{
 unsigned p, l = strlen (hs);

 for (p=0;p<l;p++)
 {
	 if (!(((hs[p] > 47) && (hs[p] < 58)) || ((hs[p] > 64) && (hs[p] < 71))))
	 {
		 debug ("%u not hex?", hs[p]);
		 return 0;
	 }
 }
 return 1;

}

//HWND cmdhWnd = NULL;

HWND lobbyhWnd = NULL, chathWnd = NULL;


void CreateLobbyWindow()
{
	HWND hWnd = CreateDialog(NULL, MAKEINTRESOURCE (IDD_DIALOG3), NULL, NULL);
    if ( hWnd!=NULL ) 
    { 
	    // show dialog 
        ShowWindow(hWnd,SW_SHOW); 
		lobbyhWnd = hWnd;
    } 
	else
	{
        debug("Failed to create dialog.  GetLastError: %u", GetLastError()); 
        bDone = 1; 
        return; 
    } 
}

/*
void DisplayCommands(char* cmdList)
{
	HWND hWnd = CreateDialog(NULL, MAKEINTRESOURCE (IDD_DIALOG2), NULL, NULL);
    if ( hWnd!=NULL ) 
    { 
	    // show dialog 
        ShowWindow(hWnd,SW_SHOW); 
		if ( ! SetDlgItemText ( hWnd, IDC_STATIC1, cmdList ) )
		{
			debug ("Failed to set dialog text.  GetLastError: %u", GetLastError());
		}
		cmdhWnd = hWnd;
    } 
	else
	{
        debug("Failed to create dialog.  GetLastError: %u", GetLastError()); 
        bDone = 1; 
        return; 
    } 
}
*/

void ChatThreadProc ( void* dummy )
{
    // create the dialog window 
    HWND hWnd = CreateDialog(NULL, MAKEINTRESOURCE (IDD_DIALOG4), NULL, NULL); 
    MSG msg; 
    if ( hWnd!=NULL ) 
    { 
        // show dialog 
        ShowWindow(hWnd,SW_SHOW); 
    } 
    else 
    { 
        debug("Failed to create dialog.  GetLastError: %u", GetLastError()); 
        bDone = 1; 
        return; 
    } 

	chathWnd = hWnd;

    // message loop to process user input 
	
    while(1)
    { 
		if ( PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE) ) 
        { 
            if ( msg.message==WM_KEYUP ) 
            { 
                int nVirtKey = (int) msg.wParam; 
                if ( nVirtKey== 0x000D )  /* User hit enter. */
                { 
                    // get the edit control 
                    HWND hEdit = GetDlgItem(hWnd,IDC_EDIT3);
                    if ( hEdit ) 
                    { 
                        // get the input text the user entered 
                        // and print it to the console window 
                        unsigned char WindowRetrieve[200];
						unsigned char cpkt_data[250];
						unsigned short chat_size;
						
                        int p, nSize = GetWindowText( hEdit, WindowRetrieve, 199 );
                        WindowRetrieve[nSize] = 0;
						
						if ( nSize )
						{
							for (p=0;p<nSize-1;p++)
							{
								if (WindowRetrieve[p] == 0x24)
								{
									if (WindowRetrieve[p+1] != 0x24) WindowRetrieve[p] = 0x09; else 
										WindowRetrieve[p+1] = 0x09;
								}
								if (WindowRetrieve[p] == 0x23)
								{
									if (WindowRetrieve[p+1] != 0x23) WindowRetrieve[p] = 0x0A; else 
										WindowRetrieve[p+1] = 0x09;
								}
							}

							if ( WindowRetrieve[0] == 0x40 )
							{
								memcpy ( &cpkt_data[0], &e3chat_pkt[0], sizeof (e3chat_pkt));
								sprintf (&cpkt_data[sizeof(e3chat_pkt)], "%s", (char*) &WindowRetrieve[1] );
								
								chat_size = sizeof (e3chat_pkt) + nSize;
							}
							else
							{
								memcpy ( &cpkt_data[0], &chat_pkt[0], sizeof (chat_pkt));
								sprintf (&cpkt_data[sizeof(chat_pkt)], "%s", (char*) &WindowRetrieve[0] );
								chat_size = sizeof (chat_pkt) + nSize;
							}

							cpkt_data[chat_size++] = 0x00;
						
							while (chat_size % 4)
							{
								cpkt_data[chat_size++] = 0x00;
							}
							
							*(unsigned short*) &cpkt_data[2] = chat_size;
							
							custom_data_in_buf = chat_size;
							memcpy (&custom_buf[0], &cpkt_data[0], chat_size);
							custom_buf[chat_size] = 0;
						}
						SetDlgItemText ( hWnd, IDC_EDIT3, 0x00 );
                    }
                    else 
                    { 
                        debug("Failed to get edit control"); 
                    } 
                }
            }
            // process message 
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
            // if there is no message to process, 
            // then sleep for a while to avoid burning 
            // too much CPU cycles 
            Sleep(100); 
        } 
    } 
}

void InputThreadProc( void *dummy ) 
{ 
    // create the dialog window 
    HWND hWnd = CreateDialog(NULL, MAKEINTRESOURCE (IDD_DIALOG1), NULL, NULL); 
    MSG msg; 
    if ( hWnd!=NULL ) 
    { 
        // show dialog 
        ShowWindow(hWnd,SW_SHOW); 
    } 
    else 
    { 
        debug("Failed to create dialog.  GetLastError: %u", GetLastError()); 
        bDone = 1; 
        return; 
    } 
    // message loop to process user input 
    while(1) 
    { 
	    if ( PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE) ) 
        { 
            if ( msg.message==WM_KEYUP ) 
            { 
                int nVirtKey = (int) msg.wParam; 
                if ( nVirtKey== 0x000D )  /* User hit enter. */
                { 
                    // get the edit control 
                    HWND hEdit = GetDlgItem(hWnd,IDC_EDIT1); 
                    if ( hEdit ) 
                    { 
                        // get the input text the user entered 
                        // and print it to the console window 
                        unsigned char WindowRetrieve[8193];
						unsigned char cpkt_data[8193];
						unsigned char b;
						int c;
						unsigned short nPktLen;

                        int nSize = GetWindowText( hEdit, WindowRetrieve, 8192 );
                        WindowRetrieve[nSize] = 0;
						_strupr (&WindowRetrieve[0]);
						nPktLen = 0;

						for (c=0;c<nSize;c++)
						{
							if (WindowRetrieve[c] != 0x20) cpkt_data[nPktLen++] = WindowRetrieve[c];
						}
						
						/* lol how did this disappear? */

						cpkt_data[nPktLen] = 0;

						if ( nPktLen )
						{
							if ( nPktLen % 8 )
								debug ("Invalid custom packet data entered.");
							else
								if ( !isHex(&cpkt_data[0]) )
									debug ("Input custom packet data not in HEX format."); else
								{
									for (c=0;c<nPktLen;c+=2)
									{
									  if ( cpkt_data[c] < 58 ) b = (cpkt_data[c] - 48); else b = (cpkt_data[c] - 55);
									  b *= 16;
									  if ( cpkt_data[c+1] < 58 ) b += (cpkt_data[c+1] - 48); else b += (cpkt_data[c+1] - 55);
									  custom_buf[c/2] = b;
									}
									nPktLen /= 2;
									custom_data_in_buf = nPktLen;
									custom_buf[nPktLen] = 0;
								}

						}
						

                    } 
                    else 
                    { 
                        debug("Failed to get edit control"); 
                    } 
                } 
            } 
            // process message 
            TranslateMessage(&msg); 
            DispatchMessage(&msg); 
        } 
        else 
        { 
            // if there is no message to process, 
            // then sleep for a while to avoid burning 
            // too much CPU cycles 
            Sleep(100); 
        } 
    } 
}   				

  unsigned char lobby_clone_full[12][1244];
  unsigned char my_data_full[1244];
  unsigned char stored_full[1244];
  int clone_now = -1;
  int partial_clone = 0;
  int max_stats = 0;
  unsigned lobby_gcn[12];
  unsigned char lobby_ip[12][4];
  char lobby_name[12][13];
  int lobby_user_active[12];
  unsigned char lobby_packet[(1084 * 12) + 17];
  int lobby_pktsize, lobby_erase, tlc, lobby_update = 0, team_mode = 0, reset_yes = 0, team_create = 0, schthack_mode = 0, flipped_servers = 0, allow_quest = 0;
  int steal_quest = 0;
#ifdef EPISODE_3
  ep3_fixup = 0;
#endif
  unsigned short lobby_class[12];
  unsigned my_gcn, my_clientid;
  unsigned char my_name[12];
  unsigned short my_class;
  int auto_own = 1;
  int steal_symbolnow = 0;
  struct hostent *schthack_host;  

  char chatlines[29][255];
  char chat_data[8094];
  char lobby_data[4096];

  int chat_start = 1;

void UpdateChatWindow()
{
	/* we could probably update a chat log here.. */
	char p,chat_now;
	int total = 0;

	chat_data[0] = 0;
	chat_now = chat_start;
	for (p=0;p<29;p++)
	{
		sprintf (&chat_data[total], "%s\n", (char*) &chatlines[chat_now][0] );
		chat_now++;
		if (chat_now > 28) chat_now = 0;
		total = strlen (&chat_data[0]);
	}
	SetDlgItemText ( chathWnd, IDC_CHATWINDOW, &chat_data[0] );
	UpdateWindow ( chathWnd );
	chat_start++;
	if (chat_start > 28) chat_start = 0;
}

void UpdateLobbyWindow()
{
  char p;
  int total = 0;
	
	lobby_data[0] = 0;
	sprintf (&lobby_data[0], "Lobby users [Name:Client ID:GC #]:\n\n");
	total = strlen (&lobby_data[0]);
	for (p=0;p<0x0C;p++)
		{
			if (lobby_user_active[p])
			{
				sprintf (&lobby_data[total], "%s : %u : %u : %u.%u.%u.%u\n", (char*) &lobby_name[p], p, lobby_gcn[p], lobby_ip[p][0], lobby_ip[p][1], lobby_ip[p][2], lobby_ip[p][3] );
				total = strlen (&lobby_data[0]);
			}
		}
	sprintf (&lobby_data[total], "\n");
	SetDlgItemText ( lobbyhWnd, IDC_STATIC2, &lobby_data[0] );
	UpdateWindow ( lobbyhWnd );
	lobby_update = 0;
}

unsigned zero_dword = 0;
unsigned set_dword = 0xFFFFFFFF;
unsigned lastcpy;

unsigned pktcpy ( char* dest, const char* src, unsigned size)
{
	memcpy (dest,src,size);
	lastcpy = size;
	return size;
}


unsigned auto_own_list[256];
int own_count = 0;

unsigned char null_outfit[10];

void process_lobby_data(int erase)
  
  {

	#ifndef FIREWALL_ONLY
	#ifndef GOODY_GOODY
	  int o;
	#endif
    #endif

	  int j,j4;
	  unsigned char cid;

	  if (erase)
	  {
		  for (j=0;j<0x0C;j++)
			  lobby_user_active[j] = 0;
	  }
 
	  if ( team_mode )
	  {
		  if ( team_mode == 1 )
		  {
			/* Joining a team. */
			for (j=0;j<lobby_packet[1];j++)
			{
				j4 = ((16 * 8) + 4) + (j * 32);
				cid = *(unsigned char*) &lobby_packet[j4+12];
				if (cid < 0x04)
				{
					lobby_gcn[cid] = *(unsigned *) &lobby_packet[j4+4];
					*(unsigned*) &lobby_ip[cid][0] = *(unsigned *) &lobby_packet[j4 + 8];
					memcpy (&lobby_name[cid], &lobby_packet[j4 + 16], 12);
					lobby_class[cid] = 0;
					lobby_name[cid][12] = 0;
					lobby_user_active[cid] = 1;
					if (lobby_gcn[cid] == my_gcn)
					{
						my_clientid = cid;
					}
				}
			}
		  }
		  else
		  {
			  /* Another user joining the game. */
			  j = 16;
			  cid = *(unsigned char*) &lobby_packet[j+12];
			  if (cid < 0x04)
			  {
				  lobby_gcn[cid] = *(unsigned *) &lobby_packet[j+4];
				  *(unsigned *) &lobby_ip[cid][0] = *(unsigned *) &lobby_packet[j+8];
				  memcpy ( &lobby_name[cid][0], &lobby_packet[j+16], 12 );
				  lobby_class[cid] = *(unsigned short*) &lobby_packet[j+960];
				  /* Force outfit and hair to zero for skin users. */
				  if ( lobby_packet[j+962] > 0 )
				  {
					  memcpy (&exam_buf[j+968], &null_outfit[0], 10);
				  }
				  lobby_name[cid][12] = 0;
				  lobby_user_active[cid] = 1;
				  if (lobby_gcn[cid] == my_gcn)
				  {
					  my_clientid = cid;
				  }
			  }
		  }
	  }
	  else
		  /* skip the first 16 bytes and copy user information.  data is in 1084 byte chunks per user. */
		  for (j=16;j<lobby_pktsize;j+=1084)
		  {
			memcpy (&cid, &lobby_packet[j+12], 1);
			if (cid < 0x0C)
			{
			  lobby_gcn[cid] = *(unsigned *) &lobby_packet[j+4];
			  *(unsigned *) &lobby_ip[cid][0] = *(unsigned *) &lobby_packet[j+8];
			  memcpy ( &lobby_name[cid][0], &lobby_packet[j+16], 12 );
			  lobby_class[cid] = *(unsigned short*) &lobby_packet[j+960];

				/* Force outfit and hair to zero for skin users. */
				if ( lobby_packet[j+962] > 0 )
				{
					  memcpy (&exam_buf[j+968], &null_outfit[0], 10);
				}
				lobby_name[cid][12] = 0;
				lobby_user_active[cid] = 1;
				if (lobby_gcn[cid] == my_gcn)
				{
					my_clientid = cid;
					my_class = lobby_class[cid];
					*(unsigned short*) &gcfsod_pkt[150] = my_class;
				}

#ifndef FIREWALL_ONLY
#ifndef GOODY_GOODY
				if (auto_own == 1)
				{
					for (o=0;o<own_count;o++)
					{
						if (lobby_gcn[cid] == auto_own_list[o])
						{
							molest_pkt[1] = cid;
							custom_data_in_buf += pktcpy (&custom_buf[custom_data_in_buf], &molest_pkt[0], sizeof (molest_pkt));
							debug ("%s, Show us on the doll where he touched you...", (char*) &lobby_name[cid], lobby_gcn[cid]);
                            top_broadcast ("%s, Show us on the doll where he touched you...")
							break;
						}
					}					
				}

#endif
#endif
			}
		  }

	  lobby_update = 1;

  }

int steal_schats = 1, forced_item = 0, skin_id = 0, firewall_on = 1;
int god_mode = 0, god_offset = 0, god_tick = 0;

/*
int force_equip = 0;
unsigned char equip_value;

  */

int name_override = 0;
unsigned char bb_name[4];

unsigned clientarea;
unsigned char clientxy[8];

int check_packet (unsigned char* pkt)
{ 
    unsigned char cid, wep_type, max_wep;
    unsigned short size_check, size_check2, size_check3, size_check_index, tmp_team;
    int srank;
    
    if (!firewall_on) return 0;
    
    if ( (pkt[0] == 0x9A) && (pkt[1] == 0x11)) 
    {
        
        pkt[1] = 0x02;
        debug ("check_packet: 0x11 -> 0x02 (Hunter's license expired?)");
        return 0;
    } 
    
    // Quest packets
    
    if ((pkt[0] == 0x44) || (pkt[0] == 0xA6) || (pkt[0] == 0xA7))
    {
        if (!team_mode)
        {
            pkt[0] = 0x1D;
            pkt[1] = 0x00;
            client_broadcast ("Quest data detected while in lobby.");
            return 1;
        }
        else
            if (!allow_quest)
            {
                pkt[0] = 0x1D;
                pkt[1] = 0x00;
                client_broadcast ("Double quest attempt blocked.");
            }
    }
    
    
    if ((pkt[0] == 0x10) && (pkt[5] == 0x01) && (pkt[6] == 0x0E))
    {
        if (!team_mode)
        {
            pkt[0] = 0x1D;
            pkt[1] = 0x00;
            client_broadcast ("Quest data detected while in lobby.");
            return 1;
        }
        else
            if (!allow_quest)
            {
                pkt[0] = 0x1D;
                pkt[1] = 0x00;
                client_broadcast ("Double quest attempt blocked.");
            }
    }
    
    if ((pkt[0] == 0xAC) && (allow_quest))
    {
        allow_quest = 0;
    }
    
    
    // Fun packet 
    
    if ((pkt[0] == 0x60) || (pkt[0] == 0x62) || (pkt[0] == 0x6C) || (pkt[0] == 0x6D) )
    {
        /* Check size of packet first. */
        
        size_check_index = pkt[4];
        size_check_index *= 2;
        
        size_check = *(unsigned short*) &pkt[2];
        
        size_check2 = size_check_table[size_check_index+1];
       
        if ( ( size_check != size_check2 ) && ( size_check2 != 0 ) && ( pkt[4] != 0x05 ) )
        {
            pkt[0] = 0x1D;
            pkt[1] = 0x00;
            client_broadcast ("Packet size incorrect.");
            return 1;
        }
        
        /* Kamikaze weapon creation. */
        
        if (pkt[4] == 0x5D)
        {
            currentitemid = *(unsigned *) &pkt[32];
            currentitemid += 0x00010000;
            if (pkt[20] == 0x00)
            {
                /* Check S-Rank Special. */
                
                srank = 0;
                if ((pkt[21] > 0x6F) && (pkt[21] < 0x89)) srank = 1;
                if ((pkt[21] > 0xA4) && (pkt[21] < 0xB0)) srank = 1;
                if ((srank) && (pkt[22] > 0x10))
                {
                    pkt[22] = 0x00;
                    cid = pkt[6];
                    client_broadcast ("User %s created a Kami weapon.", (char*) &lobby_name[cid][0]);
                }
                
                /* Check regular weapon indexes. */
                
                if (!srank)
                {
                    if (pkt[21] < 0xA5) 
                    {
                        if (pkt[21] == 0x00) pkt[21] = 0x01; /* Invalid weapon type. */
                        wep_type = pkt[21];
                        max_wep = weapon_check[wep_type];
                        if ( pkt[22] > max_wep ) 
                        {
                            pkt[22] = 0;
                            cid = pkt[6];
                            client_broadcast ("User %s created a Kami weapon.", (char*) &lobby_name[cid][0]);
                        }
                    }
                    else
                    {
                    /* Set to regular saber.
                    pkt[21] = 1;
                    pkt[22] = 0;
                    cid = pkt[6];
                    client_broadcast ("User %s created a Kami weapon.", (char*) &lobby_name[cid][0]);
                        */
                    }
                }
            }
        }        
        
        /* Kamikaze weapon withdrawl. */
        
        if (pkt[4] == 0x2B)
        {
            if (pkt[8] == 0x00)
            {
                srank = 0;
                if ((pkt[9] > 0x6F) && (pkt[9] < 0x89)) srank = 1;
                if ((pkt[9] > 0xA4) && (pkt[9] < 0xB0)) srank = 1;
                if ((srank) && (pkt[10] > 0x10))
                {
                    pkt[10] = 0x00;
                    cid = pkt[6];
                    client_broadcast ("User %s withdrew a Kami weapon.", (char*) &lobby_name[cid][0]);
                }
                
                if (!srank)
                {
                    if (pkt[9] < 0xA5) 
                    {
                        if (pkt[9] == 0x00) pkt[9] = 0x01; /* Invalid weapon type. */
                        wep_type = pkt[9];
                        max_wep = weapon_check[wep_type];
                        if ( pkt[10] > max_wep ) 
                        {
                            pkt[10] = 0;
                            cid = pkt[6];
                            client_broadcast ("User %s withdrew a Kami weapon.", (char*) &lobby_name[cid][0]);
                        }
                    }
                    else
                    {
                    /*
					pkt[9] = 1;
                    pkt[10] = 0;
                    cid = pkt[6];
                    client_broadcast ("User %s withdrew a Kami weapon.", (char*) &lobby_name[cid][0]);
					*/
                    }
                }
            }
        }
        
        /* Kamikaze weapon buying */ 
        
        if (pkt[4] == 0x5E)
        {
            if (pkt[8] == 0x00)
            {
                srank = 0;
                if ((pkt[9] > 0x6F) && (pkt[9] < 0x89)) srank = 1;
                if ((pkt[9] > 0xA4) && (pkt[9] < 0xB0)) srank = 1;
                if ((srank) && (pkt[10] > 0x10))
                {
                    pkt[10] = 0x00;
                    cid = pkt[6];
                    client_broadcast ("User %s bought a Kami weapon.", (char*) &lobby_name[cid][0]);
                }
                
                if (!srank)
                {
                    if (pkt[9] < 0xA5) 
                    {
                        if (pkt[9] == 0x00) pkt[9] = 0x01; /* Invalid weapon type. */
                        wep_type = pkt[9];
                        max_wep = weapon_check[wep_type];
                        if ( pkt[10] > max_wep ) 
                        {
                            pkt[10] = 0;
                            cid = pkt[6];
                            client_broadcast ("User %s bought a Kami weapon.", (char*) &lobby_name[cid][0]);
                        }
                    }
                    else
                    {
                    /*
                    pkt[9] = 1;
                    pkt[10] = 0;
                    cid = pkt[6];
                    client_broadcast ("User %s withdrew a Kami weapon.", (char*) &lobby_name[cid][0]);
					*/
                    }
                    
                }
                
            }
        }
        
        if ((pkt[2] == 0x48) && (pkt[4] == 0x07) && (pkt[5] == 0x11))
        {
            /* Steal symbol chats. */
            
            if (steal_schats)
            {
                memcpy (&steal_schat[0], pkt, 0x48);
            }
            
        }
        
        /* W/S FSOD */
        
        if ( ((pkt[4] == 0x74) || (pkt[4] == 0xBD)) && ((pkt[13] > 0x06) || ((pkt[12] == 0x00) && (pkt[13] == 0x00)) ) )
        {
            pkt[0] = 0x1D;
            pkt[1] = 0x00;
            if (pkt[7] < 0x0C)
            {
                cid = pkt[7];
                client_broadcast ("W/S FSOD detected from user %s.", (char*) &lobby_name[cid][0]);
#ifndef FIREWALL_ONLY
                memcpy (&custom_buf[custom_data_in_buf], &gcfsod_pkt[0], sizeof (gcfsod_pkt));
                custom_buf[custom_data_in_buf+1] = cid;
                custom_data_in_buf += sizeof (gcfsod_pkt);
                debug ("Removing user %s on GCN %u from lobby.", (char*) &lobby_name[cid], lobby_gcn[cid]);
                auto_own_list[own_count++] = lobby_gcn[cid];
#endif
                
            }
            return 1;
        }
        
        /* TP FSOD ??? */
        
        if ((pkt[4] == 0x68) && (pkt[5] == 0x07) && (pkt[6] == 0x1B))
        {
            if ((pkt[8] > 0x03) || (pkt[10] > 0x11))
            {
                pkt[0] = 0x1D;
                pkt[1] = 0x00;
                cid = pkt[8];
                if (cid < 0x04)
                {
                    client_broadcast ("Bad telepipe set by %s.", (char*) &lobby_name[cid][0]);
                }
                else
                    client_broadcast ("Bad telepipe set detected.");
                return 1;
            }
        }
        
        /* "Door" FSOD */
        
        if ((pkt[4] == 0x76) && (pkt[9] != 0x00))
        {
            pkt[0] = 0x1D;
            pkt[1] = 0x00;
            client_broadcast ("Door FSOD detected.");
            return 1;
        }
        
        /* Guild card FSOD */
        
        if (((pkt[4] == 0x06) && (pkt[5] == 0x25)) && ((pkt[2] != 0x98) || (pkt[3] != 0x00)))
        {
            pkt[0] = 0x1D;
            pkt[1] = 0x00;
            client_broadcast ("Guild card FSOD detected.");
            return 1;
        }
        
        // Fake QFSOD
        
        if (pkt[4] == 0x73)
        {
            pkt[0] = 0x1D;
            pkt[1] = 0x00;
            client_broadcast ("Fake QFSOD detected.");
            return 1;
        }
        
        // Crash packets
        
        if (pkt[4] == 0x18)
        {
            if ((pkt[0] != 0x60) || (!team_mode))
            {
                pkt[0] = 0x1D;
                pkt[1] = 0x00;
                client_broadcast ("Crash packet detected.");
                return 1;
            }
            else
            {
                /* Only allow in Vol Opt's area. */
                
                if (clientarea != 0x0D)
                {
                    pkt[0] = 0x1D;
                    pkt[1] = 0x00;
                    client_broadcast ("Crash packet detected.");
                    return 1;
                }
            }
        }
        
        if ((pkt[4] == 0x63) || (pkt[4] == 0x6A) || (pkt[4] == 0x6B) || (pkt[4] == 0x6C) || (pkt[4] == 0x6D) || (pkt[4] == 0x6E) || (pkt[4] == 0x70))
        {
            /* Not sure what 0x63 does.. ROFL..  0x6A is the boss teleporter. */
            
            if ((pkt[0] == 0x60) && (pkt[4] == 0x6A))
            {
                /* Check to see if it's targetting a player. */
                if (pkt[6] < 0x04)
                {
                    pkt[0] = 0x1D;
                    pkt[1] = 0x00;
                    client_broadcast ("Crash packet detected.");    
                    return 1;
                }
            }
            else
            {
                if ((pkt[4] != 0x63) || (pkt[0] != 0x60))
                {
                    size_check = *(unsigned short*) &pkt[2];
                    size_check2 = *(unsigned short*) &pkt[8];
                    size_check2 += 4;
                    if ((pkt[0] != 0x6D) || (pkt[1] != my_clientid) || (size_check != size_check2))
                    {
                        pkt[0] = 0x1D;
                        pkt[1] = 0x00;
                        client_broadcast ("Crash packet detected.");
                        return 1;
                    }
                }
            }
            
		}
        
        // Relocate packet.
        
        if ((pkt[4] == 0x17) && ((pkt[0] != 0x60) || (!team_mode)))
        {
            pkt[0] = 0x1D;
            pkt[1] = 0x00;
            client_broadcast ("Relocate packet detected.");
            return 1;
        }
        
        // Warp time.
        
        if (pkt[4] == 0x94)
        {
            if ( team_mode == 0 )
            {
                pkt[0] = 0x1D;
                pkt[1] = 0x00;
                client_broadcast ("Warp attempt detected while in lobby.");
                return 1;
            }
            else
            {
                if (pkt[5] < 0x02)
                {
                    pkt[0] = 0x1D;
                    pkt[1] = 0x00;
                    client_broadcast ("Bad warp packet detected.");
                    return 1;
                }
                else
                    if ( ( pkt[8] > 0x11) || ( pkt[0] == 0x6D ) )
                    {
                        pkt[0] = 0x1D;
                        pkt[1] = 0x00;
                        client_broadcast ("Bad warp packet detected.");
                        return 1;
                    }
            }
        }
        
        // PK packet.
        
        if ( (pkt[4] == 0x9A) && ( ((pkt[0] == 0x60) && (pkt[6] == my_clientid)) || (((pkt[0] == 0x6D) || (pkt[0] == 0x62)) && (pkt[1] == my_clientid)) ) )
        {
			if (pkt[10] < 3)
            {
				pkt[0] = 0x1D;
				pkt[1] = 0x00;
				client_broadcast ("PK attempt detected.");
				return 1;
			}
        }
        
        // Lobby reset. 0x6D is target version.  Allow only one reset when joining a team...
        
        if ((pkt[4] == 0x71) && ((pkt[0] != 0x62) || (!team_mode)))
        {
            pkt[0] = 0x1D;
            pkt[1] = 0x00;
            client_broadcast ("Character \"Reset\" detected.");
            return 1;
        }
        else
            if ((pkt[4] == 0x71) && (pkt[0] == 0x62))
            {
                if (!reset_yes)
                {
                    pkt[0] = 0x1D;
                    pkt[1] = 0x00;
                    client_broadcast ("Attempted character \"Reset\" but have already been!");
                    return 1;
                }
                else
                {
                    reset_yes = 0;
                }
                
            }
            
            // Spawn NPC packet.
            
            if (((pkt[0] == 0x6D) || (pkt[0] == 0x62)) && (pkt[4] == 0x69))
            {
                pkt[0] = 0x1D;
                pkt[1] = 0x00;
                client_broadcast ("NPC spawn detected.");
                return 1;
            }
            
            // ELENOR packet.
            
            if ( (pkt[4] == 0x58) && ((pkt[2] != 0x0C) || (pkt[3] != 0x00)) )
            {
                pkt[0] = 0x1D;
                pkt[1] = 0x00;
                client_broadcast ("NPC spawn detected.");
                return 1; 
            }
            
            /* Unknown target packet. */
            
            if (((pkt[0] == 0x6D) || (pkt[0] == 0x62)) && (pkt[2] != 0x98) && (!team_mode))
            {
                if (((pkt[4] != 0x07) || (steal_symbolnow == 0)) && (pkt[4] != 0xAE))
                {
                    pkt[0] = 0x1D;
                    pkt[1] = 0x00;
                    client_broadcast ("Unknown target packet detected while in lobby.  Command: %s", hexByte (pkt[4]));
                    return 1;
                }
                else
                {
                    if ((steal_symbolnow == 1) && (pkt[4] == 0x07)) steal_symbolnow = 0;
                }
            }                        
            
            /* Malformed packet check. */
            
            size_check2 = size_check_table[size_check_index+1];
           
            /* Check size of packet.. */
            
            size_check3 = pkt[5];
            size_check3 *= 4;
            size_check3 += 4;
            
            /* Fix for EP3 battle packets. */
            
            if ((team_mode) || ((pkt[0] == 0x60) && (pkt[4] == 0xB4))) tmp_team = 1; else tmp_team = 0;
            
            /* Fix for EP3 stage selection. */
            
            if ((pkt[4] == 0xB6) && (pkt[0] == 0x6C)) size_check3 = size_check;

#ifdef EPISODE_3

			/* Oh shit, security can be compromised here! Allow all 0xB0-0xBF packets with the Episode 3 proxy. */

			if ((pkt[4] >= 0xB0) && (pkt[4] <= 0xBF)) size_check3 = size_check;

#endif
            
            /* Fix for EP1&2 game joins. */
            
            if ((pkt[0] == 0x6D) && ((pkt[4] > 0x6A) && (pkt[4] < 0x71))) size_check3 = size_check;
            
            if (( size_check3 != size_check ) || ((size_check2 == 0) && (!tmp_team))) {
                pkt[0] = 0x1D;
                pkt[1] = 0x00;
                client_broadcast ("Packet size incorrect or unknown packet. Command: %s", hexByte (pkt[4]));
                return 1;
            }
            
            /*
            if ((size_check2 == 0) && (team_mode))
            {
                debug ("\nObtained Packet information:");
                debug ("Command: %s", hexByte(pkt[4]));
                debug ("Size: %s\n", hexWord(size_check));
            }
            */
                
                
				/* Unknown server packet. */
                
				if ((pkt[0] == 0x6C) && (!team_mode))
				{
					pkt[0] = 0x1D;
					pkt[1] = 0x00;
					client_broadcast ("\"Server\" packet detected while in lobby.  Command: %s", hexByte (pkt[4]));
					return 1;
				}
	}
                            
#ifndef EPISODE_3
                            
	// Disconnect packet
	
	if (pkt[0] == 0xC9)
	{
		pkt[0] = 0x1D;
		pkt[1] = 0x00;
		client_broadcast ("Disconnect attempt detected.");
		return 1;
	} 

#endif

	return 0;
}


void duplicate_for_all ( char* buf, char* pkt, int pktsize, int target )
{
	char p;

	for (p=0;p<0x0C;p++)
	{
		memcpy (buf, pkt, pktsize);
		buf[target] = p;
		buf += pktsize;
	}
}

unsigned getgcn ( char* cname )
{
	int p,p2,p3,p4;

	char s1[13], s2[13];

	p4 = strlen ( cname );
	
	memcpy ( &s1[0], cname, p4 + 1 );
	_strupr ( s1 );

	for (p=0;p<0x0C;p++)
	{
		p3 = 1;

		memcpy ( &s2[0], &lobby_name[p][0], 13 );
		_strupr ( s2 );
	
		for (p2=0;p2<p4;p2++)
		{
			if ( s1[p2] != s2[p2] ) p3 = 0;
		}

		if ( p3 ) return lobby_gcn[p];
	}

	return 0;

}

unsigned char getcid ( char* cname )
{
	int p,p2,p3,p4;

	char s1[13], s2[13];

	p4 = strlen ( cname );
	
	memcpy ( &s1[0], cname, p4 + 1 );
	_strupr ( s1 );

	for (p=0;p<0x0C;p++)
	{
		p3 = 1;

		memcpy ( &s2[0], &lobby_name[p][0], 13 );
		_strupr ( s2 );
	
		for (p2=0;p2<p4;p2++)
		{
			if ( s1[p2] != s2[p2] ) p3 = 0;
		}

		if ( p3 ) return p;
	}

	return 255;
}

/* Ports 10000 - 10019 have to be reserved for proxy. */

int START_PORT = 10000;

#define MAX_PORTS 20

  unsigned short listen_ports[MAX_PORTS];
  int listen_active[MAX_PORTS];
  unsigned short server_ports[MAX_PORTS];
  struct in_addr server_addrs[MAX_PORTS];
  unsigned listen_creation[MAX_PORTS];  /* Used for overwriting when all ports becomes full. */ 
  unsigned creation_num = 0;
  int listen_sockfds[MAX_PORTS]; 
  unsigned char last_used_addr[4];
  unsigned short last_used_port, my_used_port;

  void select_portidx ( unsigned short *listen_port, int *listen_idx )
  {
	  int p, p2, piu;
	  unsigned cn;

	  *listen_port = 0;
	  *listen_idx = -1;

	  /* Search for a free port. */

	  for (p=START_PORT;p<START_PORT+MAX_PORTS;p++)
	  {
		  piu = 0;
		  for (p2=0;p2<MAX_PORTS;p2++)
		  {
			  if ((listen_ports[p2] == p) && (listen_active[p2]))
			  {
				  piu = 1;
				  break;
			  }
		  }
		  if (!piu) 
		  {
			  *listen_port = p;
			  break;
		  }
	  }

	  /* No free ports?  Use the oldest created active listening port. */

	  if (! *listen_port)
	  {
		  cn = 0xFFFFFFFF;
		  for (p=0;p<MAX_PORTS;p++)
		  {
			  if ((listen_creation[p] < cn) && (listen_active[p] == 1))
			  {
				  cn = listen_creation[p];
				  *listen_idx = p;
				  *listen_port = listen_ports[p];
			  }
		  }

		  /* We definitely had to have gotten a port. */

		  closesocket (listen_sockfds[*listen_idx]);
		  listen_active[*listen_idx] = 0;

	  }
	  
	  /* Need an index for the port we got at the very beginning. */

	  if (*listen_idx == -1)
	  {
		  for (p=0;p<MAX_PORTS;p++)
		  {
			  if (listen_active[p] == 0)
			  {
				  *listen_idx = p;
				  break;
			  }
		  }

		  /* This should never occur, but whatever. */

		  if ( *listen_idx == -1 )
		  {
			  cn = 0xFFFFFFFF;
			  for (p=0;p<MAX_PORTS;p++)
			  {
				  if ((listen_creation[p] < cn) && (listen_active[p] == 1))
					{
					  cn = listen_creation [p];
					  *listen_idx = p;
				  }
			  }

		  /* We definitely had to have gotten a port. */

		  closesocket (listen_sockfds[*listen_idx]);
		  listen_active[*listen_idx] = 0;

		  }
	  }
  }


char log_file_name[256];
char chat_file_name[256];
char trail_work[256];
unsigned char tbroadcast_pkt[] = { 0xEE, 0x00, 0x68, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
unsigned char broadcast_pkt[] = { 0xB0, 0x30, 0x54, 0x00, 0x24, 0xFB, 0xFF, 0xBF, 0xE8, 0xF6, 0xFF, 0xBF };
unsigned char broadcast_pkt_bb[] = { 0xB0, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x45, 0x00 };
unsigned char work_pkt[268+1];

void top_broadcast(char *fmt, ...)
{
	unsigned short work_len;

#define MAX_BROADCAST_LEN 256
  
	 va_list args;
	 char text[ MAX_BROADCAST_LEN ];

	 FILE *fp;

	 unsigned short j;

	 va_start (args, fmt);
	 strcpy (text + vsprintf( text,fmt,args), "\n"); 
	 va_end (args);

	 fp = fopen ( &log_file_name[0], "a");
	 if (!fp)
	 {
		 printf ("Unable to log to %s\n", &log_file_name[0]);
	 }
	 fprintf (fp, "%s", text);
	 fclose (fp);
	 
	 fprintf( stderr, "%s", text);

	 memcpy (&work_pkt[0], &tbroadcast_pkt[0], sizeof (tbroadcast_pkt) );
	 work_len = sizeof ( tbroadcast_pkt );

	 for (j=0;j<strlen(text);j++)
	 {
		 if (text[j] != 0x0A)
		 {
			 if (text[j] == 0x24) 
				 work_pkt[work_len++] = 0x09; else
				 work_pkt[work_len++] = text[j];
			 work_pkt[work_len++] = 0x00;
		 }
	 }

	 work_pkt[work_len++] = 0x00;
	 work_pkt[work_len++] = 0x00;
	 work_pkt[work_len++] = 0x00;
	 work_pkt[work_len++] = 0x00;

	*(unsigned short*) &work_pkt[2] = work_len;

	 while ( work_len % 8 )
	 {
		 work_pkt[work_len++] = 0x00;
	 }

	cipher_ptr = &proxy_to_client;

	encryptcopy (&client_buf[client_data_in_buf], &work_pkt[0], work_len);

	client_data_in_buf += work_len;

	debug ("TBroadcast to Client (Decrypted:%u):", work_len );
			
	display_packet (&work_pkt[0], work_len);

}

void client_broadcast(char *fmt, ...)
{
	unsigned short work_len;

#define MAX_BROADCAST_LEN 256
  
	 va_list args;
	 char text[ MAX_BROADCAST_LEN ];

	 FILE *fp;

	 va_start (args, fmt);
	 strcpy (text + vsprintf( text,fmt,args), "\n"); 
	 va_end (args);

	 fp = fopen ( &log_file_name[0], "a");
	 if (!fp)
	 {
		 printf ("Unable to log to %s\n", &log_file_name[0]);
	 }
	 fprintf (fp, "%s", text);
	 fclose (fp);
	 
	 fprintf( stderr, "%s", text);

	 memcpy (&work_pkt[0], &broadcast_pkt[0], sizeof ( broadcast_pkt ) );
	 work_len = sizeof ( broadcast_pkt );
	 memcpy (&work_pkt[work_len], &text[0], strlen ( text ) );
	 work_len += strlen ( text );

	 work_pkt[work_len++] = 0x00;
	 work_pkt[work_len++] = 0x00;

	 *(unsigned short*) &work_pkt[2] = work_len;

	 while ( work_len % 4 )
		 work_pkt[work_len++] = 0x00;

	*(unsigned short*) &work_pkt[2] = work_len;

	cipher_ptr = &proxy_to_client;

	encryptcopy (&client_buf[client_data_in_buf], &work_pkt[0], work_len);

	client_data_in_buf += work_len;

	debug ("Broadcast to Client (Decrypted:%u):", work_len );
			
	display_packet (&work_pkt[0], work_len);

}


int exam_data_in_buf = 0;
unsigned my_meseta, my_meseta_max;


/* I got way too many fucking variables.. seriously. */

unsigned char c_exam_buf[BUF_SIZE+32];
int c_exam_data_in_buf = 0;
unsigned char rewrite_buf[BUF_SIZE+32];
unsigned char temp_buf[BUF_SIZE+32];
unsigned short program_exit = 0;
unsigned short log_counts = 0;
char myCmd[256];
char myArgs[4][256];
int cmdLen, argLen, cmdArgs;
unsigned char tid, wa, cc, nid, cid, lid;
unsigned long gcnfsod;
unsigned short rid;
int corrupt_now = 0;
SYSTEMTIME now_t;
char convert_buffer[255];
int check_boom;
int boom_time, add_time;
unsigned pgcn;
int tgcn_len;
char tgcn_name[19];
unsigned rewrite_len, old_len;
int check_bypass = 0;
int wrote_item = 0;
unsigned char item_param_1[4], item_param_2[4], item_param_3[4], item_param_4[4];
int forge_mail = 0;
int japanese_bb = 0;

/* Pointer to the autoown.txt file */

FILE *OwnFile;

/* Data being read from own file */

char OwnData[256];

void client_examine_buffer()
{
	int ch2,ch_offset;
#ifndef EPISODE_3
	unsigned char exam_secid, exam_class;
	unsigned name_color = 0xFFFFFFFF;
#endif
	unsigned short l_port;
  struct in_addr p_ip;
	int l_idx, chat_idx = 0, line_now;
  FILE* fp;
  unsigned gcn_temp;
  unsigned short bb_temp;
  unsigned short check_port;
    
    /* Reset rewrite length. */
    
    rewrite_len = 0;
    
    /* Check bypass? */
    
    check_bypass = 0;

    /* Writing an item? */
    
    wrote_item = 0;

#ifndef NO_CHAT

	/* Log mail! */

	 if ((c_exam_buf[0] == 0x81) && (c_exam_buf[1] == 0x00))
	 {
	  fp = fopen ( &chat_file_name[0], "a");
	  if (!fp)
	  {
		 printf ("Unable to log to %s\n", &chat_file_name[0]);
	  }
	  if (chat_start == 0) line_now = 28; else line_now = chat_start - 1;
	  chatlines[line_now][0] = 0;
	  fprintf (fp, "%s\n", &chatlines[line_now][0]);
	  line_now++;
	  if (line_now > 28) line_now = 0;
	  chatlines[line_now][chat_idx++] = 91;
	  chatlines[line_now][chat_idx] = 0;
	  _itoa ( now_t.wHour, &convert_buffer[0], 10 );
	  if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
	  strcat (&chatlines[line_now][0], &convert_buffer[0]);
	  strcat (&chatlines[line_now][0], ":");
	  _itoa ( now_t.wMinute, &convert_buffer[0], 10 );
	  if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
	  strcat (&chatlines[line_now][0], &convert_buffer[0]);
	  chat_idx = strlen (&chatlines[line_now][0]);
	  chatlines[line_now][chat_idx++] = 93;  /*] <*/
	  chatlines[line_now][chat_idx++] = 32;
	  gcn_temp = *(unsigned *) &c_exam_buf[28];
	  sprintf (&chatlines[line_now][chat_idx], "Sent mail as %s to %u ", &c_exam_buf[12], gcn_temp );
	  fprintf (fp, "%s\n", &chatlines[line_now][0]);
	  line_now++;
	  if (line_now > 28) line_now = 0;
	  sprintf (&chatlines[line_now][0], "\"%s\"", &c_exam_buf[34]); /* Naw mean? */
	  fprintf (fp, "%s\n", &chatlines[line_now][0]);
	  line_now++;
	  if (line_now > 28) line_now = 0;
	  chatlines[line_now][0] = 0;
	  fprintf (fp, "%s\n", &chatlines[line_now][0]);

	  /* Passed up like 3 lines.. */

	  chat_start += 3;
	  if (chat_start > 28) chat_start = chat_start - 29;
	  fclose (fp);

	  UpdateChatWindow();
  }

#endif

#ifdef EPISODE_3
	if ((c_exam_buf[0] == 0xB8) && (c_exam_buf[1] == 0x00) && (ep3_fixup))
	{

	  debug ("Forced packet (Decrypted:%u):", sizeof (ep3fixup));

	  display_packet (&ep3fixup[0], sizeof (ep3fixup) );

	  cipher_ptr = &proxy_to_server;
  
	  encryptcopy (&server_buf[server_data_in_buf], &ep3fixup[0], sizeof(ep3fixup));
	  
	  server_data_in_buf += sizeof (ep3fixup);

	  ep3_fixup = 0;

	}
#endif

	if ((c_exam_buf[0] == 0x96) && (c_exam_buf[1] == 0x00) && (first_connect))
	{
		first_connect = 0;
		debug ("Saving server switch fix-up information.");
		memcpy (&schtfixup[0], &c_exam_buf[0], sizeof(schtfixup));
	}

	if ((c_exam_buf[0] == 0x10) && (c_exam_buf[1] == 0x00))
	{
		if ((c_exam_buf[8] == 0xDE) && (c_exam_buf[9] == 0xAD))
		{
			if (!schthack_mode)
			{
				schthack_mode = 1;
				flipped_servers = 1;
				select_portidx ( &l_port, &l_idx );
				
				creation_num++;
				listen_creation[l_idx] = creation_num;
				listen_ports[l_idx] = l_port;
				listen_active[l_idx] = 1;
				
				server_ports[l_idx] = schtsrv_port;
				*(unsigned *) &server_addrs[l_idx].s_addr = *(unsigned *) &schtsrv_addr[0];
				
				debug ("PSO attempting to connect to %s : %u", inet_ntoa(server_addrs[l_idx]), server_ports[l_idx]);
				debug ("Rewriting packet.  Connection rerouted to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);
				
				*(unsigned *) &schtsrv_pkt[4] = *(unsigned *) &proxy_addr[0];
				*(unsigned short*) &schtsrv_pkt[8] = listen_ports[l_idx];
				
				/* Setup listener... */
				
				debug ("Setting up listener..");
				
				p_ip.s_addr = INADDR_ANY;
				listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );
				
				tcp_listen (listen_sockfds[l_idx]);
				
				cipher_ptr = &proxy_to_client;
				
				encryptcopy ( &client_buf[client_data_in_buf], &schtsrv_pkt[0], sizeof (schtsrv_pkt) );
				
				client_data_in_buf += sizeof (schtsrv_pkt);

				c_exam_data_in_buf = 0;
				rewrite_len = 0;			
			}
			else
			{
				schthack_mode = 0;
				flipped_servers = 1;
				select_portidx ( &l_port, &l_idx );
				
				creation_num++;
				listen_creation[l_idx] = creation_num;
				listen_ports[l_idx] = l_port;
				listen_active[l_idx] = 1;
				
				server_ports[l_idx] = sega_port;
				*(unsigned *) &server_addrs[l_idx].s_addr = *(unsigned *) &sega_addr[0];
				
				debug ("PSO attempting to connect to %s : %u", inet_ntoa(server_addrs[l_idx]), server_ports[l_idx]);
				debug ("Rewriting packet.  Connection rerouted to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);
				
				*(unsigned *) &schtsrv_pkt[4] = *(unsigned *) &proxy_addr[0];
				*(unsigned short *) &schtsrv_pkt[8] = listen_ports[l_idx];
				
				/* Setup listener... */
				
				debug ("Setting up listener..");
				
				p_ip.s_addr = INADDR_ANY;
				listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );
				
				tcp_listen (listen_sockfds[l_idx]);
				
				cipher_ptr = &proxy_to_client;
				
				encryptcopy ( &client_buf[client_data_in_buf], &schtsrv_pkt[0], sizeof (schtsrv_pkt) );
				
				client_data_in_buf += sizeof (schtsrv_pkt);

				c_exam_data_in_buf = 0;
				rewrite_len = 0;			

			}
		}
	}

	if ((c_exam_buf[0] == 0x09) && (c_exam_buf[1] == 0x00))
	{
		if ((c_exam_buf[8] == 0xDE) && (c_exam_buf[9] == 0xAD))
		{
			
			cipher_ptr = &proxy_to_client;
			
			encryptcopy ( &client_buf[client_data_in_buf], &noinfo_pkt[0], sizeof (noinfo_pkt) );
			
			client_data_in_buf += sizeof (noinfo_pkt);

			c_exam_data_in_buf = 0;
			rewrite_len = 0;			
		}
	}

	/* Edit initial authentication command to pretend like we haven't gotten a guild card # when server flips. */

	if ( c_exam_buf[0] == 0x9E )
	{
		memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
		if (flipped_servers)
		{
			rewrite_buf[rewrite_len+6] = 0xFF;
			rewrite_buf[rewrite_len+7] = 0xFF;
			rewrite_buf[rewrite_len+8] = 0xFF;
			rewrite_buf[rewrite_len+9] = 0xFF;
			rewrite_buf[rewrite_len+10] = 0xFF;
			rewrite_buf[rewrite_len+11] = 0xFF;
			/* Fixes improper data message. */
			for (ch2=0xC8;ch2<0xEC;ch2++)
				rewrite_buf[rewrite_len+ch2] = 0x00;

		}
		else
		{
			check_port = *(unsigned short*) &c_exam_buf[8];
			if (check_port == my_used_port)
			{
				// Schthack fix #3
				*(unsigned *) &rewrite_buf[rewrite_len+4] = *(unsigned *) &last_used_addr[0];
				*(unsigned short*) &rewrite_buf[rewrite_len+8] = last_used_port;
			}
		}
		rewrite_len += c_exam_data_in_buf;		
	}

	else

	/* Schthack doesn't need to know our HL passwords. */

	if ( ( c_exam_buf[0] == 0xDB ) && ( c_exam_buf[1] == 0x00 ) && ( schthack_mode ) )
	{
		memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
		rewrite_buf[176] =  78 +  ( ( c_exam_buf[176] & 0x0F ) * 3 / 4 );
		rewrite_buf[177] =  78 -  ( ( c_exam_buf[177] & 0x0F ) * 3 / 4 );
		rewrite_buf[178] =  109 + ( ( c_exam_buf[178] & 0x0F ) * 3 / 4 );
		rewrite_buf[179] =  78 -  ( ( c_exam_buf[179] & 0x0F ) * 3 / 4 );
		rewrite_buf[180] =  109 - ( ( c_exam_buf[180] & 0x0F ) * 3 / 4 );
		rewrite_buf[181] =  78 +  ( ( c_exam_buf[181] & 0x0F ) * 3 / 4 );
		rewrite_buf[182] =  109 - ( ( c_exam_buf[182] & 0x0F ) * 3 / 4 );
		rewrite_buf[183] =  52 +  ( ( c_exam_buf[183] & 0x0F ) / 4 );
		rewrite_len += c_exam_data_in_buf;
	}

	else

	/* Forge mail? */

	if ( ( c_exam_buf[0] == 0x81 ) && ( forge_mail ) )
	{
        client_broadcast ("Forged guild card # for mail.");
		memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
		*(unsigned *) &rewrite_buf[8] = *(unsigned *) &clientxy[0];
		*(unsigned *) &rewrite_buf[(34*16)+8] = *(unsigned *) &clientxy[0];
		rewrite_len += c_exam_data_in_buf;
	}

	else
    
	if ( ( c_exam_buf[0] == 0xDB ) && ( cranberry_mode ) )
	{
		/* Fix up version. 0x31 for EP1&2, 0x41 for EP3. */
		memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
#ifdef EPISODE_3
		rewrite_buf[76] = 0x41;
#else
		rewrite_buf[76] = 0x31;
#endif
		rewrite_len += c_exam_data_in_buf;
	}

	else

	if ( ( c_exam_buf[0] == 0x9E ) && ( cranberry_mode ) )
	{
		/* Fix up version. 0x31 for EP1&2, 0x41 for EP3. */
		memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
#ifdef EPISODE_3
		rewrite_buf[76] = 0x41;
#else
		rewrite_buf[76] = 0x31;
#endif
		rewrite_len += c_exam_data_in_buf;
	}

	else

#ifndef EPISODE_3
	
	if ( ( ( c_exam_buf[0] == 0x61 ) || ( c_exam_buf[0] == 0x98 ) ) ) 
	{
		exam_secid = c_exam_buf[932];
		exam_class = c_exam_buf[933];
		 /*debug ("Reporting section ID = %u", c_exam_buf[932] );
		 debug ("Reporting section Class = %u", c_exam_buf[933] );
		 debug ("Reporting section Unknown = %u", c_exam_buf[912] );
		 debug ("Reporting section Unknown2 = %u", c_exam_buf[934] );*/
		memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
		if (!schthack_mode)
		{
			rewrite_buf[912] = 0;
			rewrite_buf[934] = 0;
		}
		if ((skin_id) && (schthack_mode))
		{
		  rewrite_buf[912] = skin_id - 1;
		  rewrite_buf[934] = 0x06;
		}
		*(unsigned *) &rewrite_buf[908] = name_color;
		rewrite_len += c_exam_data_in_buf;
	}

	else
   
#endif
		
	if ( c_exam_buf[0] == 0x60 )
    {
		/* Remembering the client's position. */
        
		if ( ( ( c_exam_buf[4] == 0x40 ) && ( c_exam_buf[5] == 0x04 ) ) ||
			( ( c_exam_buf[4] == 0x42 ) && ( c_exam_buf[5] == 0x03 ) ) )
		{
			*(long long*) &clientxy[0] = *(long long*) &c_exam_buf[8];
		}
		else
			if ( ( c_exam_buf[4] == 0x3E ) && ( c_exam_buf[5] == 0x06 ) )
			{
				*(unsigned *) &clientxy[0] = *(unsigned *) &c_exam_buf[16];
				*(unsigned *) &clientxy[4] = *(unsigned *) &c_exam_buf[24];
			}
			
			if ( ( c_exam_buf[4] == 0x1F ) && ( c_exam_buf[5] == 0x02 ) )
				clientarea = *(unsigned *) &c_exam_buf[8];
			
			/* W/S FSOD */
			
			if ( ((c_exam_buf[4] == 0x74) || (c_exam_buf[4] == 0xBD)) && ((c_exam_buf[13] > 0x06) || ( (c_exam_buf[12] == 0x00) && (c_exam_buf[13] == 0x00) ) ) )
			{
				c_exam_buf[0] = 0x1D;
				c_exam_buf[1] = 0x00;
				program_exit = 1;
				debug ("Aborting program due to use of W/S FSOD.");
			}
			
			memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
			rewrite_len += c_exam_data_in_buf;
    }
    
    else
        
        /* User wanna quit? */
        
        if ( ( c_exam_buf[0] == 0x05 ) && ( c_exam_data_in_buf == 4 ) )
        {
            program_exit = 1;
            
            memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
            rewrite_len += c_exam_data_in_buf;
        }
        
        else
            
            if ( c_exam_buf[0] == 0x9E ) 
            {                
                /* Remember my name. */
                
                memcpy ( &my_name[0], &c_exam_buf[(11*16) + 12], 12 );
                
                memcpy ( &gcfsod_pkt[19], &my_name, 12 );
                
                memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
                rewrite_len += c_exam_data_in_buf;
            }
            
            else
                
                /* Curse bypass. */
                
                /* Information board. */
                
                if ( (c_exam_buf[0] == 0xD9)  && (c_exam_buf[4] == 0x09) )
                {
                    for (ch2 = 6;ch2<146;ch2++)
                    {
                        /* Replace $ with special char. */
                        
                        if (c_exam_buf[ch2] == 0x24)
                        {
                            c_exam_buf[ch2] = 0x09;
                            if (c_exam_buf[ch2+1] == 0x24)
                                ch2++;
                        }
                        if (ch2 >= c_exam_data_in_buf) break;
                    }
                    memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
                    rewrite_len += c_exam_data_in_buf;
                }
                
                else
                    
                    /* Team creation. */
                    
                    if ( ((c_exam_buf[0] == 0xEC) || (c_exam_buf[0] == 0xC1)) && (c_exam_buf[4] == 0x00) )
                    {
                        for (ch2 = 14;ch2<28;ch2++)
                        {
                            /* Replace $ with special char. */
                            
                            if (c_exam_buf[ch2] == 0x24)
                            {
                                c_exam_buf[ch2] = 0x09;
                                if (c_exam_buf[ch2+1] == 0x24)
                                    ch2++;
                            }
                            if (ch2 >= c_exam_data_in_buf) break;
                        }
                        memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
                        rewrite_len += c_exam_data_in_buf;
						team_create = 1;
                    }
                    
                    else
                        
                        /* Simple mail. */
                        
                        if ((c_exam_buf[0] == 0x81) && (c_exam_buf[4] == 0x00))
                        {
                            for (ch2 = 34;ch2<174;ch2++)
                            {
                                /* Replace $ with special char. */
                                
                                if (c_exam_buf[ch2] == 0x24)
                                {
                                    c_exam_buf[ch2] = 0x09;
                                    if (c_exam_buf[ch2+1] == 0x24)
                                        ch2++;
                                }
                                if (ch2 >= c_exam_data_in_buf) break;
                            }
                            
                            for (ch2 = 291;ch2<431;ch2++)
                            {
                                /* Replace $ with special char. */
                                
                                if (c_exam_buf[ch2] == 0x24)
                                {
                                    c_exam_buf[ch2] = 0x09;
                                    if (c_exam_buf[ch2+1] == 0x24)
                                        ch2++;
                                }
                                if (ch2 >= c_exam_data_in_buf) break;
                            }
                            memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
                            rewrite_len += c_exam_data_in_buf ;
                        }
                        
                        else
                            
                            /* Normal text. */
                            
                            if ((c_exam_buf[0] == 0x06) && (c_exam_buf[4] == 0x00))
                            {
								if (c_exam_buf[14] == 0x45) ch_offset = 1; else ch_offset = 0;

                                if ((c_exam_buf[ch_offset+14] != 0x24) || (c_exam_buf[ch_offset+15] == 0x24))
                                {
                                    for (ch2 = 14+ch_offset;ch2<c_exam_data_in_buf;ch2++)
                                    {
                                        /* Replace $ with special char. */
                                        
                                        if (c_exam_buf[ch2] == 0x24)
                                        {
                                            c_exam_buf[ch2] = 0x09;
                                            if (c_exam_buf[ch2+1] == 0x24)
                                                ch2++;
                                        }
                                        
                                        /* Replace # with line break. */
                                        
                                        if (c_exam_buf[ch2] == 0x23)
                                        {
                                            if (c_exam_buf[ch2+1] == 0x23)
                                            {
                                                c_exam_buf[ch2] = 0x09;
                                                ch2++;
                                            }
                                            else
                                                c_exam_buf[ch2] = 0x0A;
                                        }
                                    }
                                    memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
                                    rewrite_len += c_exam_data_in_buf ;
                                }
                                else
                                {
                                    /* Store! */
                                    
                                    old_len = rewrite_len;
                                    
                                    /* Process custom commands. */
                                    
                                    cmdLen = 0;
                                    
                                    for (ch2=15+ch_offset;ch2<c_exam_data_in_buf;ch2++)
                                    {
                                        if ((c_exam_buf[ch2] != 0x20) && (c_exam_buf[ch2] != 0x00))
                                        {
                                            myCmd[cmdLen++] = c_exam_buf[ch2];
                                        }
                                        else
                                            break;
                                    }
                                    
                                    myCmd[cmdLen] = 0;
                                    
                                    cmdArgs = 0;
                                    argLen = 0;
                                    
                                    ch2 ++;
                                    
                                    for (;ch2<c_exam_data_in_buf;ch2++)
                                    {
                                        if ((c_exam_buf[ch2] != 0x2C)  && (c_exam_buf[ch2] != 0x00))
                                        {
                                            myArgs[cmdArgs][argLen++] = c_exam_buf[ch2];
                                        }
                                        else
                                        {
                                            myArgs[cmdArgs][argLen] = 0;
                                            
                                            /* Was non-zero length? */
                                            
                                            if (argLen)
                                            {
                                                cmdArgs++;
                                            }
                                            
                                            argLen = 0;
                                            if ( c_exam_buf[ch2] == 0x00 ) break;
                                        }
                                    }
                                    
                                    myArgs[cmdArgs][argLen] = 0; /* Just in case it didn't terminate. */
                                   
                                    if ( ! strcmp ( &myCmd[0], "lw") )
                                    {
                                        /* Accepts a lobby to warp self to. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("LW: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            wa = atoi ( &myArgs[0][0] );
                                            if ( ( wa > 0x0F ) || ( wa == 0x00 ) ) wa = 0x01;
											if (!schthack_mode)
												wa = 0xC1 - wa;
											else
												wa--;

                                            memcpy ( &rewrite_buf[rewrite_len], &lwarp_pkt[0], sizeof ( lwarp_pkt ) );
                                            rewrite_buf[rewrite_len+8] = wa;
                                            rewrite_len += sizeof ( lwarp_pkt );                                        
                                            client_broadcast ("LW: Warped to Lobby %u.", wa);
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "clw") )
                                    {
                                        /* Accepts a card lobby to warp self to. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("CLW: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
											if (schthack_mode) wa = 0x0F; else
											{
												wa = atoi ( &myArgs[0][0] );
												if ( ( wa > 0x09 ) || ( wa == 0x00 ) )  wa = 0x01;
												wa = 0xB2 - wa;
											}
                                            memcpy ( &rewrite_buf[rewrite_len], &lwarp_pkt[0], sizeof ( lwarp_pkt ) );
                                            rewrite_buf[rewrite_len+8] = wa;
                                            rewrite_len += sizeof ( lwarp_pkt );
                                            client_broadcast ("CLW: Warped to Card Lobby %u.", wa);
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "sendgc") )
                                    {
                                        /* Accepts a client ID to send your guild card to. */   
                                        if ( cmdArgs < 2 )
                                        {
                                            client_broadcast ("SENDGC: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                gc_data[1] = tid;
                                                cc = atoi ( &myArgs[1][0] );
                                                if ( cc > 0x09 )
                                                {
                                                    if (cc != 0x0A)
                                                    {
                                                        cc = 0x37;
                                                    }
                                                    else
                                                        cc = 0x47;
                                                }
                                                else
                                                    cc += 0x30;
                                                color_data[2] = cc;
                                                *(unsigned *) &gc_data[12] = my_gcn;
												if (schthack_mode)
												{
													memcpy ( &gc_data[16], &my_name, 12 );
													memcpy ( &gc_data[42], &my_name, 12 );
												}
												else
												{
													memcpy ( &gc_data[16], &color_data[0], 3 );
													memcpy ( &gc_data[19], &my_name, 12 );
													memcpy ( &gc_data[42], &my_name, 12 );
												}
                                                *(unsigned short*) &gc_data[150] = my_class;
                                                memcpy ( &rewrite_buf[rewrite_len], &gc_data[0], sizeof ( gc_data ) );
                                                rewrite_len += sizeof ( gc_data );
                                                client_broadcast ("SENDGC: Sent Guild Card to user %s.", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("SENDGC: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "jukebox") )
                                    {
                                        /* Accepts a track # for jukebox playing (EP3) */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("JUKEBOX: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = atoi (&myArgs[0][0]);
                                            memcpy ( &rewrite_buf[rewrite_len], &juke_pkt[0], sizeof ( juke_pkt ) );
                                            rewrite_buf[rewrite_len+8] = tid;
                                            rewrite_len += sizeof ( juke_pkt );
                                            client_broadcast ("JUKEBOX: Playing track %u.", tid );
                                        }
                                    }

									if ( ! strcmp ( &myCmd[0], "arrow") )
									{
										/* Changes lobby arrow color. */
										if ( cmdArgs < 1 )
										{
											client_broadcast ("ARROW: Insufficient number of arguments for command..");
										}
										else
										{
											memcpy ( &rewrite_buf[rewrite_len], &arrow_pkt[0], sizeof (arrow_pkt) );
											lid = atoi (&myArgs[0][0]);
											if (lid > 0x0F) lid = 0x0F;
											rewrite_buf[rewrite_len+1] = lid;
											rewrite_len += sizeof (arrow_pkt);
											
											client_broadcast ("ARROW: Set lobby arrow to %u", lid);
										}
									}                                    
                                    
                                    if ( ! strcmp ( &myCmd[0], "event") )
                                    {
                                        /* Changes lobby event. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("EVENT: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            lid = atoi (&myArgs[0][0]);
                                            if (lid > 0x0E) lid = 0x0E;
                                            event_pkt[1] = lid;
                                            
                                            cipher_ptr = &proxy_to_client;

											bb_temp = sizeof (event_pkt);
                                           
                                            encryptcopy ( &client_buf[client_data_in_buf], &event_pkt[0], bb_temp );
                                            
                                            client_data_in_buf += bb_temp;
                                            
                                            client_broadcast ("EVENT: Set lobby event to %u", lid);
                                        }
                                    }

#ifndef FIREWALL_ONLY

                                    if ( ! strcmp ( &myCmd[0], "warp") )
                                    {
                                        /* Accepts a client ID and area to warp them to. */
                                        if ( cmdArgs < 2 )
                                        {
                                            client_broadcast ("WARP: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                wa = atoi ( &myArgs[1][0] );
                                                memcpy ( &rewrite_buf[rewrite_len], &warp_pkt[0], sizeof ( warp_pkt ) );
                                                rewrite_buf[rewrite_len] = 0x6D;
                                                rewrite_buf[rewrite_len+1] = tid;
                                                rewrite_buf[rewrite_len+8] = wa;
                                                rewrite_len += sizeof ( warp_pkt );
                                                client_broadcast ("WARP: Warp %s to %u.", (char*) &lobby_name[tid][0], wa);
                                                
                                            }
                                            else
                                                client_broadcast ("WARP: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "warpme") )
                                    {
                                        /* Accepts an area to warp myself to. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("WARPME: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            wa = atoi ( &myArgs[0][0] );

											warp_pkt[0] = 0x62;
											warp_pkt[1] = my_clientid;
											warp_pkt[8] = wa;

											cipher_ptr = &proxy_to_client;

											bb_temp = sizeof (warp_pkt);
											
											encryptcopy ( &client_buf[client_data_in_buf], &warp_pkt[0], bb_temp );

											client_data_in_buf += bb_temp;

                                            client_broadcast ("WARPME: Warp to %u.", wa);
                                            
                                        }
                                        
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "warpall") )
                                    {
                                        /* Warps everyone to an area */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("WARPALL: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            wa = atoi ( &myArgs[0][0] );
                                            memcpy ( &rewrite_buf[rewrite_len], &warp_pkt[0], sizeof ( warp_pkt ) );
                                            rewrite_buf[rewrite_len] = 0x60;
                                            rewrite_buf[rewrite_len+1] = 00;
                                            rewrite_buf[rewrite_len+8] = wa;
                                            rewrite_len += sizeof ( warp_pkt );
                                            client_broadcast ("WARPALL: Warped all to %u.", wa);
                                        }
                                    }

                                    if ( ! strcmp ( &myCmd[0], "getgamelist") )
                                    {
                                        /* Shows game list. */
                                        memcpy ( &rewrite_buf[rewrite_len], &getgame_pkt[0], sizeof ( getgame_pkt ) );
                                        rewrite_len += sizeof ( getgame_pkt );
                                        client_broadcast ("GETGAMELIST: Requested game list." );
                                    }

									if ( ! strcmp ( &myCmd[0], "maketech") )
									{
										/* Accepts HEX value for making weapons. */
										if ( cmdArgs < 2 )
										{
											client_broadcast ("MAKETECH: Insufficient number of arguments for command..");
										}
										else
										{
											*(unsigned *) &item_param_2 = zero_dword;
											item_param_2[0] = atoi (&myArgs[0][0]);
											create_pkt[20] = 0x03;
											create_pkt[21] = 0x02;								
											create_pkt[22] = atoi (&myArgs[1][0]);
											if (create_pkt[22]) create_pkt[22]--;
											create_pkt[23] = 0x00;
											create_pkt[6] = (unsigned char) my_clientid;
											*(unsigned *) &create_pkt[8] = clientarea;
											*(unsigned *) &create_pkt[12] = *(unsigned *) &clientxy[0];
											*(unsigned *) &create_pkt[16] = *(unsigned *) &clientxy[4];
											*(unsigned *) &create_pkt[24] = *(unsigned *) &item_param_2;
											*(unsigned *) &create_pkt[28] = zero_dword;
											*(unsigned *) &create_pkt[36] = zero_dword;
											*(unsigned *) &create_pkt[32] = currentitemid;
											
											currentitemid += 0x00010000;
											
											rewrite_len += pktcpy (&rewrite_buf[rewrite_len], &create_pkt[0], sizeof (create_pkt) );
											
											client_broadcast ("MAKETECH: Item obtained!" );
											wrote_item = 1;
										}
									}

                                    
                                    if ( ! strcmp ( &myCmd[0], "makeitem") )
                                    {
                                        /* Accepts HEX value for making weapons. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("MAKEITEM: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            if (strlen (&myArgs[0][0]) < 8)
                                            {
                                                client_broadcast ("MAKEITEM: Invalid item length.");
                                            }
                                            else
                                            {
                                                create_pkt[6] = (unsigned char) my_clientid;
                                                
                                                *(unsigned *) &create_pkt[8] = clientarea;
                                                *(unsigned *) &create_pkt[12] = *(unsigned *) &clientxy[0];
                                                *(unsigned *) &create_pkt[16] = *(unsigned *) &clientxy[4];
                                                _strupr (&myArgs[0][0]);
                                                create_pkt[20] = hexToByte (&myArgs[0][0]);
                                                create_pkt[21] = hexToByte (&myArgs[0][2]);
                                                create_pkt[22] = hexToByte (&myArgs[0][4]);
                                                create_pkt[23] = hexToByte (&myArgs[0][6]);
                                                *(unsigned *) &create_pkt[24] = *(unsigned *) &item_param_2;
                                                *(unsigned *) &create_pkt[28] = *(unsigned *) &item_param_3;
                                                *(unsigned *) &create_pkt[36] = *(unsigned *) &item_param_4;
                                                *(unsigned *) &create_pkt[32] = currentitemid;
                                                currentitemid += 0x00010000;
                                                memcpy ( &rewrite_buf[rewrite_len], &create_pkt[0], sizeof ( create_pkt ) );
                                                rewrite_len += sizeof ( create_pkt );
                                                client_broadcast ("MAKEITEM: Item created!" );
                                                wrote_item = 1;
                                            }
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "setitem2") )
                                    {
                                        /* Accepts HEX value for making weapons. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("SETITEM2: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            _strupr (&myArgs[0][0]);
                                            if (strlen(&myArgs[0][0]) < 8)
                                            {
                                                client_broadcast ("SETITEM2: Invalid parameter length.");
                                            }
                                            else
                                            {
                                                item_param_2[0] = hexToByte (&myArgs[0][0]);
                                                item_param_2[1] = hexToByte (&myArgs[0][2]);
                                                item_param_2[2] = hexToByte (&myArgs[0][4]);
                                                item_param_2[3] = hexToByte (&myArgs[0][6]);
                                                client_broadcast ("SETITEM2: Item creation parameters set." );
                                            }
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "setitem3") )
                                    {
                                        /* Accepts HEX value for making weapons. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("SETITEM3: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            _strupr (&myArgs[0][0]);
                                            if (strlen(&myArgs[0][0]) < 8)
                                            {
                                                client_broadcast ("SETITEM3: Invalid parameter length.");
                                            }
                                            else
                                            {
                                                item_param_3[0] = hexToByte (&myArgs[0][0]);
                                                item_param_3[1] = hexToByte (&myArgs[0][2]);
                                                item_param_3[2] = hexToByte (&myArgs[0][4]);
                                                item_param_3[3] = hexToByte (&myArgs[0][6]);
                                                client_broadcast ("SETITEM3: Item creation parameters set." );
                                            }
                                        }
                                    }
                                    
                                    
                                    if ( ! strcmp ( &myCmd[0], "setitem4") )
                                    {
                                        /* Accepts HEX value for making weapons. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("SETITEM4: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            _strupr (&myArgs[0][0]);
                                            if (strlen(&myArgs[0][0]) < 8)
                                            {
                                                client_broadcast ("SETITEM4: Invalid parameter length.");
                                            }
                                            else
                                            {
                                                item_param_4[0] = hexToByte (&myArgs[0][0]);
                                                item_param_4[1] = hexToByte (&myArgs[0][2]);
                                                item_param_4[2] = hexToByte (&myArgs[0][4]);
                                                item_param_4[3] = hexToByte (&myArgs[0][6]);
                                                client_broadcast ("SETITEM4: Item creation parameters set." );
                                            }
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "resetitem") )
                                    {
                                        /* Resets item creation parameters. */
                                        _strupr (&myArgs[0][0]);
                                        *(unsigned *) &item_param_2 = zero_dword;
                                        *(unsigned *) &item_param_3 = zero_dword;
                                        *(unsigned *) &item_param_4 = zero_dword;
                                        client_broadcast ("RESETITEM: All item creation parameters reset." );
                                    }

                                    if ( ! strcmp ( &myCmd[0], "rank") )
                                    {
                                        /* Accepts a number and title for EP3 rank.  As a bonus, unlocks all jukebox songs. */
                                        if ( cmdArgs < 2 )
                                        {
                                            client_broadcast ("RANK: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            rid = atoi (&myArgs[0][0]);
                                            
                                            if (strlen(&myArgs[1][0]) > 9)
                                                myArgs[1][9] = 0;
                                            tid = strlen(&myArgs[1][0]);
                                            
                                            for (ch2=8;ch2<18;ch2++)
                                            {
                                                rank_pkt[ch2] = 0;
                                            }
                                            
                                            lid = 0;
                                            
                                            *(unsigned short *) &rank_pkt[4] = rid;
                                            
                                            for (ch2=0;ch2<tid;ch2++)
                                            {
                                                cid  = 0x7F;
                                                cid -= ( ( unsigned char ) myArgs[1][ch2] - lid );
                                                rank_pkt[8+ch2] = cid;
                                                lid = myArgs[1][ch2];
                                            }
                                            
                                            *(unsigned *) &rank_pkt[18] = my_meseta;
                                            *(unsigned *) &rank_pkt[23] = my_meseta_max;
                                            
                                            cipher_ptr = &proxy_to_client;
                                            
                                            encryptcopy ( &client_buf[client_data_in_buf], &rank_pkt[0], sizeof (rank_pkt) );
                                            
                                            client_data_in_buf += sizeof (rank_pkt);
                                            
                                            client_broadcast ("RANK: Set Rank to %u:%s.", rid, (char*) &myArgs[1][0] );
                                        }
                                    }

									if ( ! strcmp ( &myCmd[0], "questlog") )
									{
										/* Enables or disables quest data logging. */
										if (steal_quest == 1)
										{
											steal_quest = 0;
											client_broadcast ("Logging of quest data disabled.");
										}
										else
										{
											steal_quest = 1;
											client_broadcast ("Logging of quest data enabled.");
											
										}
									}


									if ( ! strcmp ( &myCmd[0], "rawquest") )
									{
										/* Enables or disables quest data logging. */
										if (steal_quest == 2)
										{
											steal_quest = 0;
											client_broadcast ("Raw logging of quest data disabled.");
										}
										else
										{
											steal_quest = 2;
											client_broadcast ("Raw logging of quest data enabled.");
											
										}
									}

									if ( ! strcmp ( &myCmd[0], "godmode" ) )
									{
										if ( god_mode )
										{
											god_mode = 0;
											client_broadcast ("GODMODE: God mode has been disabled.");
										}
										else
										{
											if ( cmdArgs )
											{
												god_mode = 2;
												client_broadcast ("GODMODE: Team god mode has been enabled.");
											}
											else
											{
												god_mode = 1;
												client_broadcast ("GODMODE: God mode has been enabled.");
											}
										}
									}


#ifndef GOODY_GOODY

									if ( ! strcmp ( &myCmd[0], "ql") )
									{
										/* Loads a quest. */
										if ( cmdArgs < 1 )
										{
											client_broadcast ("QL: Insufficient number of arguments for command..");
										}
										else
										{
											lid = atoi (&myArgs[0][0]);
											quest_pkt[0x05] = 1;
											quest_pkt[0x08] = lid;
											pktcpy ( &rewrite_buf[rewrite_len], &quest_pkt[0], sizeof ( quest_pkt ) );
											rewrite_len += lastcpy;
											client_broadcast ("QL: Loading quest %u", lid);
										}
									}

									if ( ! strcmp ( &myCmd[0], "ql2") )
									{
										/* Loads a quest. */
										if ( cmdArgs < 1 )
										{
											client_broadcast ("QL2: Insufficient number of arguments for command..");
										}
										else
										{
											lid = atoi (&myArgs[0][0]);
											quest_pkt[0x05] = 2;
											quest_pkt[0x08] = lid;
											pktcpy ( &rewrite_buf[rewrite_len], &quest_pkt[0], sizeof ( quest_pkt ) );
											rewrite_len += lastcpy;
											client_broadcast ("QL2: Loading quest %u", lid);
										}
									}


                                    if ( ! strcmp ( &myCmd[0], "powpunkass") )
                                    {
                                        /* Accepts a client ID to corrupt. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("POWPUNKASS: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &reload_pkt[0], sizeof ( reload_pkt ) );
                                                rewrite_buf[rewrite_len+1] = tid;
                                                rewrite_len += sizeof ( reload_pkt );
												for (ch2=0;ch2<10;ch2++)
												{
                                                 memcpy ( &custom_buf[custom_data_in_buf], &crash_pkt[0], sizeof ( crash_pkt ) );
                                                 custom_buf[custom_data_in_buf+1] = tid;
												 custom_data_in_buf += sizeof ( crash_pkt );
												}
                                                corrupt_now = 1;
                                                GetSystemTime ( &now_t );
                                                boom_time = ( ( ( ( int ) now_t.wHour * 3600 ) ) + ( now_t.wMinute * 60 ) + ( now_t.wSecond ) );
                                                boom_time *= 1000;
                                                boom_time += now_t.wMilliseconds;
                                                if ( cmdArgs > 1 )
                                                {
                                                    add_time = atoi ( &myArgs[1][0] );
                                                }
                                                else
                                                {
                                                    add_time = 1234;
                                                }
                                                boom_time += add_time;
                                                client_broadcast ("POWPUNKASS: User %s corrupted.  Delay time: %u ms", (char*) &lobby_name[tid][0], add_time);
                                                
                                            }
                                            else
                                                client_broadcast ("POWPUNKASS: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "reload") )
                                    {
                                        /* Accepts a client ID to reload. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("RELOAD: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &reload_pkt[0], sizeof ( reload_pkt ) );
                                                rewrite_buf[rewrite_len+1] = tid;
                                                rewrite_len += sizeof ( reload_pkt );
                                                client_broadcast ("RELOAD: User %s reloaded.", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("RELOAD: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "reloadall") )
                                    {
                                        /* Reloads all users. */
                                        memcpy ( &rewrite_buf[rewrite_len], &reload_pkt[0], sizeof ( reload_pkt ) );
                                        rewrite_buf[rewrite_len] = 0x60;
                                        rewrite_len += sizeof ( reload_pkt );
                                        client_broadcast ("RELOADALL: Reloaded all users.");
                                    }
                                    
                                    
                                    if ( ! strcmp ( &myCmd[0], "pk" ) )
                                    {
                                        /* Accepts a client ID to PK. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("PK: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
												pk_pkt[6] = tid;
												pk_pkt[10] = 0;
                                                memcpy ( &rewrite_buf[rewrite_len], &pk_pkt[0], sizeof ( pk_pkt ) );
                                                rewrite_len += sizeof ( pk_pkt );
                                                client_broadcast ("PK: PK'd user %s", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("PK: No such user.");
                                        }
                                    }

                                    if ( ! strcmp ( &myCmd[0], "TP" ) )
                                    {
                                        /* Accepts a client ID to TP. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("TP: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
												pk_pkt[6] = tid;
												pk_pkt[10] = 4;
                                                memcpy ( &rewrite_buf[rewrite_len], &pk_pkt[0], sizeof ( pk_pkt ) );
                                                rewrite_len += sizeof ( pk_pkt );
                                                client_broadcast ("TP: Restored user %s TP", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("TP: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "heal" ) )
                                    {
                                        /* Accepts a client ID to HEAL. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("HEAL: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
												pk_pkt[6] = tid;
												pk_pkt[10] = 3;
                                                memcpy ( &rewrite_buf[rewrite_len], &pk_pkt[0], sizeof ( pk_pkt ) );
                                                rewrite_len += sizeof ( pk_pkt );
                                                client_broadcast ("HEAL: Healed user %s", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("HEAL: No such user.");
                                        }
                                    }
                           
                                    if ( ! strcmp ( &myCmd[0], "molesttall") )
                                    {
                                        /* Inappropriately touches the whole lobby. */
                                        memcpy ( &rewrite_buf[rewrite_len], &molest_pkt[0], sizeof ( molest_pkt ) );
                                        rewrite_len += sizeof ( molest_pkt );
										client_broadcast ("MOLEST: Inappropriately touched the entire lobby.");
									}


                                    if ( ! strcmp ( &myCmd[0], "molest") )
                                    {
                                       /* Inappropriately touches BluieBurst player X. */
										if ( cmdArgs < 1 )
										{
											client_broadcast ("MOLEST: Does not meet appropriate age limit in order for you to get off...");
										}
										else
										{
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &molest_pkt[0], sizeof ( molest_pkt ) );
                                                rewrite_buf[rewrite_len] = 0x62;
                                                rewrite_buf[rewrite_len+1] = tid;
                                                rewrite_len += sizeof ( molest_pkt );
                                                top_broadcast ("HEWHOCORRUPTS GameCube to BlueBurst Lobby FSOD - InfamousGamerz.net Ruthless Release 2013 - HEWHOCORRUPTS.skype");
                                                top_broadcast ("Broomops Information: Alex Gould - Broomop@hotmail.com - 38 Cornwall Gardens UXBRIDGE, Middlesex UB8 3JT UnitedKingdumb"
                                                client_broadcast ("MOLEST: %s has been successfully molested!" has been , (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("MOLEST: No kids are at the park...");
										}
									}
                                    
                                    if ( ! strcmp ( &myCmd[0], "hide") )
                                    {
                                        /* Accepts a client ID to make invisible. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("HIDE: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &hide_pkt[0], sizeof ( hide_pkt ) );
                                                rewrite_buf[rewrite_len+6] = tid;
                                                rewrite_len += sizeof ( hide_pkt );
                                                client_broadcast ("HIDE: Hid user %s", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("HIDE: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "show") )
                                    {
                                        /* Accepts a client ID to make visible */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("SHOW: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &show_pkt[0], sizeof ( show_pkt ) );
                                                rewrite_buf[rewrite_len+6] = tid;
                                                rewrite_len += sizeof ( show_pkt );
                                                client_broadcast ("SHOW: Showed user %s", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("SHOW: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "hideall") )
                                    {
                                        /* Hides all users. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &hide_pkt[0], sizeof ( hide_pkt ), 6 );
                                        rewrite_len += sizeof ( hide_pkt ) * 12;
                                        client_broadcast ("HIDEALL: Hiding all users." );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "showall") )
                                    {
                                        /* Shows all users. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &show_pkt[0], sizeof ( show_pkt ), 6 );
                                        rewrite_len += sizeof ( show_pkt ) * 12;
                                        client_broadcast ("SHOWALL: Showing all users." );
                                    }
                                    
                                    
                                    if ( ! strcmp ( &myCmd[0], "maketeam") )
                                    {
                                        /* Accepts team name, password, play type, difficulty, and episode. */
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "npcfsod") )
                                    {
                                        /* Accepts a client ID to NPCFSOD. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("NPCFSOD: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &npcfsod_pkt[0], sizeof ( npcfsod_pkt ) );
                                                rewrite_buf[rewrite_len] = 0x6D;
                                                rewrite_buf[rewrite_len+1] = tid;
                                                rewrite_len += sizeof ( npcfsod_pkt );
                                                client_broadcast ("NPCFSOD: User %s NPCFSOD'd", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("NPCFSOD: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "npcfsodall") )
                                    {
                                        memcpy ( &rewrite_buf[rewrite_len], &npcfsod_pkt[0], sizeof ( npcfsod_pkt ) );
                                        rewrite_len += sizeof ( npcfsod_pkt );
                                        client_broadcast ("NPCFSODALL: NPCFSOD'd all." );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "dcspsof") )
                                    {
                                        memcpy ( &rewrite_buf[rewrite_len], &dc_pkt[0], sizeof ( dc_pkt ) );
                                        rewrite_len += sizeof ( dc_pkt );
                                        client_broadcast ("DISCONNECT: SPSOF:BB Users have been disconnected." );
                                        
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "fakequest") )
                                    {
                                        /* Starts a fake quest. */
                                        memcpy ( &rewrite_buf[rewrite_len], &fakequest_pkt[0], sizeof ( fakequest_pkt ) );
                                        rewrite_len += sizeof ( fakequest_pkt );
                                        client_broadcast ("FAKEQUEST: Initiated fake quest." );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "qlc1") )
                                    {
                                        /* Begins challenge 1. */
                                        memcpy ( &rewrite_buf[rewrite_len], &fakequest_pkt[0], sizeof ( fakequest_pkt ) );
                                        rewrite_buf[rewrite_len+8] = 0xF9;
                                        rewrite_buf[rewrite_len+9] = 0xFE;
                                        rewrite_len += sizeof ( fakequest_pkt );
                                        client_broadcast ("QLC1: Initiated Challenge 1." );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "qlrappy") )
                                    {
                                        /* Begins challenge 1. */
                                        memcpy ( &rewrite_buf[rewrite_len], &rappy_pkt[0], sizeof ( rappy_pkt ) );
                                        rewrite_len += sizeof ( rappy_pkt );
                                        client_broadcast ("QLRAPPY: Started Rappy quest!" );
                                    }
                                    
                                    
                                    if ( ! strcmp ( &myCmd[0], "bigb") )
                                    {
                                        /* Disables humongous chat bubble. */
                                        memcpy ( &rewrite_buf[rewrite_len], &bigb_pkt[0], sizeof ( bigb_pkt ) );
                                        rewrite_len += sizeof ( bigb_pkt );
                                        client_broadcast ("BIGB: Displaying big bubble." );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "qfsod") )
                                    {
                                        /* Accepts a client ID to Quest FSOD. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("QFSOD: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &qfsod_pkt[0], sizeof ( qfsod_pkt ) );
                                                rewrite_buf[rewrite_len+1] = tid;
                                                rewrite_len += sizeof ( qfsod_pkt );
                                                client_broadcast ("QFSOD: User %s QFSOD'd", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("QFSOD: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "qfsodall") )
                                    {
                                        /* Quest FSODs everyone. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &qfsod_pkt[0], sizeof ( qfsod_pkt ), 1 );
                                        rewrite_len += sizeof ( qfsod_pkt ) * 12;
                                        client_broadcast ("QFSODALL: Quest FSOD's everyone.", tid );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "fsod") )
                                    {
                                        /* Accepts a client ID to FSOD. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("FSOD: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &fsod_pkt[0], sizeof ( fsod_pkt ) );
                                                rewrite_buf[rewrite_len+1] = tid;
                                                rewrite_len += sizeof ( fsod_pkt );
                                                client_broadcast ("FSOD: User %s FSOD'd", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("FSOD: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "fsodall") )
                                    {
                                        /* FSODs everyone. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &fsod_pkt[0], sizeof ( fsod_pkt ), 1 );
                                        rewrite_len += sizeof ( fsod_pkt ) * 12;
                                        client_broadcast ("FSODALL: FSOD'd all." );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "schat") )
                                    {
                                        /* Accepts a client ID for forced symbol chat. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("SCHAT: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &schat_pkt[0], sizeof ( schat_pkt ) );
                                                rewrite_buf[rewrite_len+8] = tid;
                                                rewrite_len += sizeof ( schat_pkt );
                                                client_broadcast ("SCHAT: User %s forced to symbol chat.", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("SCHAT: No such user.");
                                        }
                                        
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "schatall") )
                                    {
                                        /* Forces everyone to symbol chat. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &schat_pkt[0], sizeof ( schat_pkt ), 8 );
                                        rewrite_len += sizeof ( schat_pkt ) * 12;
                                        client_broadcast ("SCHATALL: Everyone forced to symbol chat." );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "wschat") )
                                    {
                                        /* Accepts a client ID for forced W/S chat. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("WSCHAT: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &wschat_pkt[0], sizeof ( wschat_pkt ) );
                                                rewrite_buf[rewrite_len+7] = tid;
                                                rewrite_len += sizeof ( wschat_pkt );
                                                client_broadcast ("WSCHAT: User %s forced to W/S chat.", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("WSCHAT: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "wschatall") )
                                    {
                                        /* Forces everyone to W/S chat. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &wschat_pkt[0], sizeof ( wschat_pkt ), 7 );
                                        rewrite_len += sizeof ( wschat_pkt ) * 12;
                                        client_broadcast ("WSCHATALL: Everyone forced to W/S chat." );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "groove") )
                                    {
                                        /* Accepts a client ID for forced dancing. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("GROOVE: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &groove_pkt[0], sizeof ( groove_pkt ) );
                                                rewrite_buf[rewrite_len+6] = tid;
                                                rewrite_len += sizeof ( groove_pkt );
                                                client_broadcast ("GROOVE: User %s forced to grOoOove!", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("GROOVE: No such user.");
                                        }   
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "grooveall") )
                                    {
                                        /* Forces everyone to dance. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &groove_pkt[0], sizeof ( groove_pkt ), 6 );
                                        rewrite_len += sizeof ( groove_pkt ) * 12;
                                        client_broadcast ("GROOVEALL: Everyone forced to dance!" );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "fakekill") )
                                    {
                                        /* Accepts a client ID to fake kill. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("FAKEKILL: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &fakekill_pkt[0], sizeof ( fakekill_pkt ) );
                                                rewrite_buf[rewrite_len+6] = tid;
                                                rewrite_len += sizeof ( fakekill_pkt );
                                                client_broadcast ("FAKEKILL: User %s has fainted!", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("FAKEKILL: No such user.");
                                        }
                                        
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "fakekillall") )
                                    {
                                        /* Fake kills everyone. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &fakekill_pkt[0], sizeof ( fakekill_pkt ), 6 );
                                        rewrite_len += sizeof ( fakekill_pkt ) * 12;
                                        client_broadcast ("FAKEKILLALL: Everyone forced to faint!" );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "action") )
                                    {
                                        /* Accepts a client ID to force an action upon. */
                                        if ( cmdArgs < 2 )
                                        {
                                            client_broadcast ("ACTION: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &action_pkt[0], sizeof ( action_pkt ) );
                                                rewrite_buf[rewrite_len+6] = tid;
                                                _strupr ( &myArgs[1][0] );
                                                rewrite_buf[rewrite_len+8] = hexToByte ( &myArgs[1][0] );
                                                if ( cmdArgs > 1 )
                                                    rewrite_buf[rewrite_len+10] = 1; else
                                                    rewrite_buf[rewrite_len+10] = 0;
                                                rewrite_len += sizeof ( action_pkt );
                                                client_broadcast ("ACTION: Forced user %s to do action %u.", (char*) &lobby_name[tid][0], hexToByte ( &myArgs[1][0] ) );
                                            }
                                            else
                                                client_broadcast ("ACTION: No such user.");
                                        }
                                        
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "actionall") )
                                    {
                                        /* Forces an action on everybody. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("ACTIONALL: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            _strupr ( &myArgs[0][0] );
                                            action_pkt[8] = hexToByte ( &myArgs[0][0] );
                                            if ( cmdArgs > 1 )
                                                action_pkt[10] = 1; else
                                                action_pkt[10] = 0;
                                            duplicate_for_all ( &rewrite_buf[rewrite_len], &action_pkt[0], sizeof ( action_pkt ), 6 );
                                            rewrite_len += sizeof ( action_pkt ) * 12;
                                            client_broadcast ("ACTIONALL: Everyone forced to do action %u!", hexToByte ( &myArgs[0][0]) );
                                        }
                                    }
                                    
                                    
                                    if ( ! strcmp ( &myCmd[0], "walk") )
                                    {
                                        /* Accepts a client ID for forced walking. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("WALK: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &walk_pkt[0], sizeof ( walk_pkt ) );
                                                rewrite_buf[rewrite_len+6] = tid;
                                                rewrite_len += sizeof ( walk_pkt );
                                                client_broadcast ("WALK: User %s forced to walk!", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("WALK: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "walkall") )
                                    {
                                        /* Forces everyone to walk. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &walk_pkt[0], sizeof ( walk_pkt ), 6 );
                                        rewrite_len += sizeof ( walk_pkt ) * 12;
                                        client_broadcast ("WALKALL: Everyone forced to walk!" );
                                    }
                                    
                                    
                                    
                                    if ( ! strcmp ( &myCmd[0], "run") )
                                    {
                                        /* Accepts a client ID for forced running. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("RUN: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &run_pkt[0], sizeof ( run_pkt ) );
                                                rewrite_buf[rewrite_len+6] = tid;
                                                rewrite_len += sizeof ( run_pkt );
                                                client_broadcast ("RUN: User %s forced to run!", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("RUN: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "runall") )
                                    {
                                        /* Forces everyone to run. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &run_pkt[0], sizeof ( run_pkt ), 6 );
                                        rewrite_len += sizeof ( run_pkt ) * 12;
                                        client_broadcast ("RUNALL: Everyone forced to run!" );
                                    }                                 
                                    
                                    if ( ! strcmp ( &myCmd[0], "passgc") )
                                    {
                                        /* Accepts a client ID,name, and GCN# to pass a guild card to. */   
                                        if ( cmdArgs < 3 )
                                        {
                                            client_broadcast ("PASSGC: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                gc_data[1] = tid;
                                                color_data[2] = 0x37;
                                                pgcn = atoi ( &myArgs[1][0] );
                                                /* Assume USA guild card when length is only 6 digits. */
                                                if ((pgcn > 99999) && (pgcn < 1000000))
                                                    pgcn += 340000000;
                                                tgcn_len = strlen ( &myArgs[2][0] );
                                                if (tgcn_len > 18) tgcn_len = 18;
                                                memcpy ( &tgcn_name[0], &myArgs[2][0], tgcn_len );
                                                for (ch2=tgcn_len;ch2<19;ch2++)
                                                    tgcn_name[ch2] = 0x00;
                                                gc_data[150] = 0;
                                                gc_data[151] = 0;
                                                *(unsigned *) &gc_data[12] = pgcn;
												if (schthack_mode)
												{
													/* Behavior has to be modified slightly for Schthack servers..** Doesn't work ** */
													memcpy ( &gc_data[16], &my_name[0], 12 );
													strcat ( &gc_data[42], "Card: ");
													memcpy ( &gc_data[48], &tgcn_name[0], tgcn_len );
													gc_data[48+tgcn_len] = 0;
												}
												else
												{
													memcpy ( &gc_data[19], &tgcn_name[0], 18 );
													memcpy ( &gc_data[42], &tgcn_name[0], 18 );
													if (cmdArgs > 3)
													{
														cc = atoi ( &myArgs[3][0] );
														if ( cc > 0x09 )
														{
															if (cc != 0x0A)
															{
																cc = 0x37;
															}
															else
																cc = 0x47;
														}
														else
															cc += 0x30;
														color_data[2] = cc;
													}
													memcpy ( &gc_data[16], &color_data[0], 3 );
												}
                                                memcpy ( &rewrite_buf[rewrite_len], &gc_data[0], sizeof ( gc_data ) );
                                                rewrite_len += sizeof ( gc_data );
                                                client_broadcast ("PASSGC: Sent Guild Card %u (%s) to user %s.", pgcn, (char*) &tgcn_name[0], (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("PASSGC: No such user.");
                                        }
                                    }                               
                                    
                                    if ( ! strcmp ( &myCmd[0], "stealgc") )
                                    {
                                        /* Accepts a client ID to steal a guild card from */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("STEALGC: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                gc_data[1] = my_clientid;
                                                *(unsigned *) &gc_data[12] = lobby_gcn[tid];
                                                memcpy ( &gc_data[16], &lobby_name[tid], 12 );
                                                memcpy ( &gc_data[42], &lobby_name[tid], 12 );
                                                for (ch2=28;ch2<34;ch2++)
                                                {
                                                    gc_data[ch2] = 0;
                                                }
                                                for (ch2=54;ch2<60;ch2++)
                                                {
                                                    gc_data[ch2] = 0;
                                                }
                                                *(unsigned short *) &gc_data[150] = lobby_class[tid];
                                                /*memcpy ( &rewrite_buf[rewrite_len], &gc_data[0], sizeof ( gc_data ) );
                                                rewrite_len += sizeof ( gc_data );*/

												cipher_ptr = &proxy_to_client;

												bb_temp = sizeof (gc_data);
                                           
												encryptcopy ( &client_buf[client_data_in_buf], &gc_data[0], bb_temp );

												client_data_in_buf += bb_temp;

                                                client_broadcast ("STEALGC: Stole Guild Card from user %s.", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("STEALGC: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "npc") )
                                    {
                                        /* Turns somebody to an NPC. */
                                        if ( cmdArgs < 3 )
                                        {
                                            client_broadcast ("NPC: Insufficient number of arguments for command. ");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            check_bypass = atoi ( &myArgs[2][0] );
                                            if ( tid != 0xFF )
                                            {
                                                npc_pkt[16] = tid;
                                                nid = atoi ( &myArgs[1][0] );
                                                npc_pkt[22] = nid;
                                                memcpy ( &rewrite_buf[rewrite_len], &npc_pkt[0], sizeof ( npc_pkt ) );
                                                rewrite_len += sizeof ( npc_pkt );
                                                client_broadcast ("NPC: NPC %u spawned to stalk %s", nid, (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("NPC: No such user.");
                                        }   
                                    }                                                                       
                                   
                                    if ( ! strcmp ( &myCmd[0], "givesym") )
                                    {
                                        /* Gives last symbol chat used with capture flag off. */
										steal_symbolnow = 1;
                                        memcpy ( &rewrite_buf[rewrite_len], &steal_schat[0], sizeof ( steal_schat ) );
                                        rewrite_buf[rewrite_len] = 0x6D;
                                        rewrite_buf[rewrite_len+1] = my_clientid;
                                        rewrite_buf[rewrite_len+8] = my_clientid;
                                        rewrite_buf[rewrite_len+13] = 2;
                                        rewrite_len += sizeof ( steal_schat );
                                        client_broadcast ("GIVESYM: Displaying last stolen symbol chat.");
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "lschat") )
                                    {
                                        /* Accepts a client ID for forced symbol chat (last stolen). */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("LSCHAT: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &steal_schat[0], sizeof ( steal_schat ) );
                                                rewrite_buf[rewrite_len+8] = tid;
                                                rewrite_len += sizeof ( steal_schat );
                                                client_broadcast ("LSCHAT: User %s forced to symbol chat.", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("LSCHAT: No such user.");
                                        }
                                        
                                    }
                                    
                                    
                                    if ( ! strcmp ( &myCmd[0], "lschatall") )
                                    {
                                        /* Forces everyone to symbol chat (last stolen). */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &steal_schat[0], sizeof ( steal_schat ), 8 );
                                        rewrite_len += sizeof ( steal_schat ) * 12;
                                        client_broadcast ("LSCHATALL: Everyone forced to symbol chat." );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "togglesteal") )
                                    {
                                        /* Enables or disables symbol chat theft. */
                                        if (steal_schats == 1)
                                        {
                                            steal_schats = 0;
                                            client_broadcast ("Symbol chat theft disabled.");
                                        }
                                        else
                                        {
                                            steal_schats = 1;
                                            client_broadcast ("Symbol chat theft enabled.");
                                            
                                        }
                                    }
									
									if ( ! strcmp ( &myCmd[0], "firewall") )
									{
										/* Enables or disables the firewall. */
										if (firewall_on)
										{
											firewall_on = 0;
											client_broadcast ("Firewall disabled.");
										}
										else
										{
											firewall_on = 1;
											client_broadcast ("Firewall enabled.");
											
										}
									}


                                   
                                    if ( ! strcmp ( &myCmd[0], "toggleown") )
                                    {
                                        /* Enables or disables auto ownage. */
                                        if (auto_own == 1)
                                        {
                                            auto_own = 0;
                                            client_broadcast ("Auto ownage turned OFF.");
                                        }
                                        else
                                        {
                                            auto_own = 1;
                                            client_broadcast ("Auto ownage turned ON.");
                                            
                                        }
                                    }

                                    if ( ! strcmp ( &myCmd[0], "forgemail") )
                                    {
                                        /* Enables or disables mail GCN forging. */
                                        if (forge_mail == 1)
                                        {
                                            forge_mail = 0;
                                            client_broadcast ("No longer inputing random GC#s into mail.");
                                        }
                                        else
                                        {
                                            forge_mail = 1;
                                            client_broadcast ("Inputing random GC#s into mail.");
                                            
                                        }
                                    }

                                    
                                    if ( ! strcmp ( &myCmd[0], "gcfsod") )
                                    {
                                        /* Accepts a client ID for Guild Card FSOD. */
                                        if ( cmdArgs < 1 )
                                        {
                                            client_broadcast ("GCFSOD: Insufficient number of arguments for command..");
                                        }
                                        else
                                        {
                                            tid = getcid ( &myArgs[0][0] );
                                            if ( tid != 0xFF )
                                            {
                                                memcpy ( &rewrite_buf[rewrite_len], &gcfsod_pkt[0], sizeof ( gcfsod_pkt ) );
                                                rewrite_buf[rewrite_len+1] = tid;
                                                rewrite_len += sizeof ( gcfsod_pkt );
                                                client_broadcast ("GCFSOD: User %s guild card FSOD'd!", (char*) &lobby_name[tid][0] );
                                            }
                                            else
                                                client_broadcast ("GCFSOD: No such user.");
                                        }
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "gcfsodall") )
                                    {
                                        /* Guild Card FSODs everyone. */
                                        duplicate_for_all ( &rewrite_buf[rewrite_len], &gcfsod_pkt[0], sizeof ( gcfsod_pkt ), 1 );
                                        rewrite_len += sizeof ( gcfsod_pkt ) * 12;
                                        client_broadcast ("GCFSODALL: Everyone guild card FSOD'd!" );
                                    }
                                    
                                    if ( ! strcmp ( &myCmd[0], "reparseown") )
                                    {
                                        /* Reparses the autoown.txt */
                                        debug ("Dumped old shit list data.");
                                        own_count = 0;
                                        debug ("Loading shit list file: autoown.txt  ");
                                        if (  ( OwnFile = fopen ("autoown.txt", "rb") ) == NULL )
                                        {
                                            debug ("File not present.\n");
                                        }
                                        else
                                        {
                                            while (fgets (&OwnData[0], 255, OwnFile) != NULL)
                                            {
                                                auto_own_list[own_count++] = atoi (&OwnData[0]);
                                                debug ("GCN # %u added to shit list.", auto_own_list[own_count-1]);
                                                if (fgets (&OwnData[0], 255, OwnFile) == NULL)
                                                    break;
                                            }
                                            fclose ( OwnFile );
                                        }
                                        client_broadcast ("REPARSEOWN: Reparsed autoown.txt" );
                                    }

#endif

#endif
                                    
                                    
                                    /* Resend 0x60 packets to the client if they're not destructive. */
                                    
                                    if ( rewrite_len != old_len )
                                    {
                                        if ( ( rewrite_buf[old_len] == 0x60 ) || ( wrote_item == 1 ) )
                                        {                                   
                                            memcpy (&temp_buf[0], &rewrite_buf[old_len], rewrite_len - old_len );
                                           
                                            if ( ( ! check_packet ( &temp_buf[0] ) ) || ( check_bypass == 1 ) )
                                            {
                                                cipher_ptr = &proxy_to_client;
                                                
                                                if ( wrote_item == 1 ) 
                                                {
                                                    cid = rewrite_buf[old_len + 6];
                                                    rewrite_buf[old_len + 6] = 0x03 - rewrite_buf[old_len + 6];
                                                    debug ("Forged client ID to %u for MAKEITEM.", rewrite_buf[old_len + 6] );
                                                }
                                                
												bb_temp = rewrite_len - old_len;

                                                encryptcopy ( &client_buf[client_data_in_buf], &rewrite_buf[old_len], bb_temp );
                                                
                                                if ( wrote_item == 1 )
                                                {
                                                    rewrite_buf[old_len + 6] = cid;
                                                }
                                                
                                                client_data_in_buf += bb_temp;
                                            }
                                        }
                                    }
                                    
                                }
                            }
                            
                            else
                            {
                                memcpy ( &rewrite_buf[rewrite_len], &c_exam_buf[0], c_exam_data_in_buf );
                                rewrite_len += c_exam_data_in_buf;
                            }
                            
                            
                            /* Use the proxy's encryption indexes to reencrypt data and pass it through to server. */
                            
                            if ( rewrite_len != 0 )
                            {
                                cipher_ptr = &proxy_to_server;
                               
                                debug ("Client (Decrypted:%u):", rewrite_len );

								if (rewrite_buf[0] == 0x9E)
								{
									memcpy (&save_buf[0], &rewrite_buf[0x4C], 12);
									for (ch2=0x4C;ch2<0x58;ch2++)
										rewrite_buf[ch2] = 0x00;
									for (ch2=0x8C;ch2<0x98;ch2++)
										rewrite_buf[ch2] = 0x00;
								}

								if (rewrite_buf[0] == 0xDB)
								{
									memcpy (&save_buf[0], &rewrite_buf[0x34], 12);
									for (ch2=0x34;ch2<0x40;ch2++)
										rewrite_buf[ch2] = 0x00;
									for (ch2=0x80;ch2<0x8C;ch2++)
										rewrite_buf[ch2] = 0x00;
								}
                                
                                display_packet (&rewrite_buf[0], rewrite_len );

								if (rewrite_buf[0] == 0x9E)
								{
									memcpy (&rewrite_buf[0x4C], &save_buf[0], 12);
									memcpy (&rewrite_buf[0x8C], &save_buf[0], 12);
								}

								if (rewrite_buf[0] == 0xDB)
								{
									memcpy (&rewrite_buf[0x34], &save_buf[0], 12);
									memcpy (&rewrite_buf[0x80], &save_buf[0], 12);
								}

//								if ( ( rewrite_buf[0] == 0x61 ) || ( rewrite_buf[1] == 0x98 ) ) exit ( 1 );
                               
                                encryptcopy (&server_buf[server_data_in_buf], &rewrite_buf[0], rewrite_len);
                               
                                server_data_in_buf += rewrite_len;
                            }
                            
                            c_exam_data_in_buf = 0;
}

//#define DEBUG_SKIN

unsigned test_class = 0x0C;

char quest_file_name[0x10];
unsigned quest_packet_size;

void unencrypt_examine()
{ 
  struct in_addr p_ip;
  unsigned short lp;
  int li;

	if (schthack_mode) 
	{
		if (exam_buf[0] == 0x19)
		{
			select_portidx ( &lp, &li );
			
			creation_num++;
			listen_creation[li] = creation_num;
			listen_ports[li] = lp;
			listen_active[li] = 1;
			
			server_ports[li] = *(unsigned short *) &exam_buf[8];
			*(unsigned *) &server_addrs[li].s_addr = *(unsigned *) &exam_buf[4];
			
			debug ("PSO attempting to connect to %s : %u", inet_ntoa(server_addrs[li]), server_ports[li]);
			debug ("Rewriting packet.  Connection rerouted to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[li]);
			
			*(unsigned *) &exam_buf[4] = *(unsigned *) &proxy_addr[0];
			*(unsigned short *) &exam_buf[8] = listen_ports[li];
			
			/* Setup listener... */
			
			debug ("Setting up listener..");
			
			p_ip.s_addr = INADDR_ANY;
			listen_sockfds[li] = tcp_sock_open( p_ip, listen_ports[li] );
			
			tcp_listen (listen_sockfds[li]);
			
			debug ("Entering Schthack mode.");
			cipher_ptr = &server_to_client;
			cipher_ptr->initialize(&gcfsod_pkt[155], sizeof(DWORD));
			cipher_ptr = &proxy_to_client;
			cipher_ptr->initialize(&gcfsod_pkt[155], sizeof(DWORD));
			cipher_ptr = &client_to_server;
			cipher_ptr->initialize(&gcfsod_pkt[155], sizeof(DWORD));
			cipher_ptr = &proxy_to_server;
			cipher_ptr->initialize(&gcfsod_pkt[155], sizeof(DWORD));
			crypt_on = 1;
		}
		
	}
	
	/* Data received from server needs to be sent to the client. */
	
	memcpy ( &client_buf[client_data_in_buf], &exam_buf[0], exam_data_in_buf );
	
	/* New keys! */
		
	if ((exam_buf[0] == 0x17) || (exam_buf[0] == 0x02))
	{
		debug ("New encryption keys obtained.");
		cipher_ptr = &server_to_client;
		cipher_ptr->initialize(&exam_buf[68], sizeof(DWORD));
		cipher_ptr = &proxy_to_client;
		cipher_ptr->initialize(&exam_buf[68], sizeof(DWORD));
		cipher_ptr = &client_to_server;
		cipher_ptr->initialize(&exam_buf[72], sizeof(DWORD));
		cipher_ptr = &proxy_to_server;
		cipher_ptr->initialize(&exam_buf[72], sizeof(DWORD));
		crypt_on = 1;
	}

	client_data_in_buf += exam_data_in_buf;
}

void server_examine_buffer()
{
  FILE *fp;
  int l_idx = 0, packet_filtered = 0, p, line_now, name_stop = 0, chat_idx = 0, name_start = 0;
  unsigned short ship_size;
  unsigned short l_port, server_port;
  struct in_addr p_ip;
  unsigned juke_hack = 0xFFFFFFFF;
  unsigned statmax = 0x08310831;
  unsigned save_times;
  unsigned gcn_temp;
  unsigned quest_offset = 0;
  unsigned char qlv_query = 0;
  int qofs = 0;

  /* Display packet before FireWall checks. */

  debug ("Server (Decrypted:%u):", exam_data_in_buf);
  display_packet (&exam_buf[0], exam_data_in_buf);
 
  if ( ( exam_buf[0] == 0x13 ) && ( steal_quest == 2 ) )
  {
	  memcpy ( &quest_file_name[0], &exam_buf[0x04+quest_offset], 0x10 );
	  qofs = strlen (&quest_file_name[0] );
	  quest_file_name[qofs-3] = 0; /* Chop! */
	  strcat (&quest_file_name[0], "raw");
	  fp = fopen ( &quest_file_name[0], "ab");
	  fwrite ( &exam_buf[0], 1, exam_data_in_buf, fp );
	  fclose ( fp );
  }

  if ( ( exam_buf[0] == 0x44 ) && ( steal_quest == 2 ) )
  {
	  memcpy ( &quest_file_name[0], &exam_buf[0x28+quest_offset], 0x10 );
	  qofs = strlen (&quest_file_name[0] );
	  quest_file_name[qofs-3] = 0; /* Chop! */
	  strcat (&quest_file_name[0], "raw");
	  fp = fopen ( &quest_file_name[0], "ab");
	  fwrite ( &exam_buf[0], 1, exam_data_in_buf, fp );
	  fclose ( fp );
  }

#ifndef NO_CHAT

  if ((exam_buf[0] == 0x81) && (exam_buf[1] == 0x00))
  {
	  fp = fopen ( &chat_file_name[0], "a");
	  if (!fp)
	  {
		 printf ("Unable to log to %s\n", &chat_file_name[0]);
	  }
	  if (chat_start == 0) line_now = 28; else line_now = chat_start - 1;
	  chatlines[line_now][0] = 0;
	  fprintf (fp, "%s\n", &chatlines[line_now][0]);
	  line_now++;
	  if (line_now > 28) line_now = 0;
	  chatlines[line_now][chat_idx++] = 91;
	  chatlines[line_now][chat_idx] = 0;
	  _itoa ( now_t.wHour, &convert_buffer[0], 10 );
	  if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
	  strcat (&chatlines[line_now][0], &convert_buffer[0]);
	  strcat (&chatlines[line_now][0], ":");
	  _itoa ( now_t.wMinute, &convert_buffer[0], 10 );
	  if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
	  strcat (&chatlines[line_now][0], &convert_buffer[0]);
	  chat_idx = strlen (&chatlines[line_now][0]);
	  chatlines[line_now][chat_idx++] = 93;  /*] <*/
	  chatlines[line_now][chat_idx++] = 32;
	  gcn_temp = *(unsigned *) &exam_buf[8];
	  sprintf (&chatlines[line_now][chat_idx], "Received mail from %s:%u ", &exam_buf[12], gcn_temp );
	  fprintf (fp, "%s\n", &chatlines[line_now][0]);
	  line_now++;
	  if (line_now > 28) line_now = 0;
	  sprintf (&chatlines[line_now][0], "\"%s\"", &exam_buf[34]); /* Naw mean? */
	  fprintf (fp, "%s\n", &chatlines[line_now][0]);
	  line_now++;
	  if (line_now > 28) line_now = 0;
	  chatlines[line_now][0] = 0;
	  fprintf (fp, "%s\n", &chatlines[line_now][0]);

	  /* Passed up like 3 lines.. */

	  chat_start += 3;
	  if (chat_start > 28) chat_start = chat_start - 29;
	  fclose (fp);

	  UpdateChatWindow();
  }

  if ((exam_buf[0] == 0x06) && (exam_buf[1] == 0x00))
  {
	  chat_idx = 0;
	  if (chat_start == 0) line_now = 28; else line_now = chat_start - 1;

	  /* Time Stampage -- Hope this isn't too slow, lawlz.. */

	  GetLocalTime ( &now_t );
	  chatlines[line_now][chat_idx++] = 91;
	  chatlines[line_now][chat_idx] = 0;
	  _itoa ( now_t.wHour, &convert_buffer[0], 10 );
	  if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
	  strcat (&chatlines[line_now][0], &convert_buffer[0]);
	  strcat (&chatlines[line_now][0], ":");
	  _itoa ( now_t.wMinute, &convert_buffer[0], 10 );
	  if (convert_buffer[1] == 0x00) strcat (&chatlines[line_now][0], "0");
	  strcat (&chatlines[line_now][0], &convert_buffer[0]);
	  chat_idx = strlen (&chatlines[line_now][0]);
	  chatlines[line_now][chat_idx++] = 93;  /*] <*/
	  chatlines[line_now][chat_idx++] = 32;
	  chatlines[line_now][chat_idx++] = 60;

	  p = 12;

	  name_start = chat_idx;

	  while ((!name_stop) && (p < exam_data_in_buf))
	  {
		  if (exam_buf[p] != 0x09)
		  {
			  chatlines[line_now][chat_idx++] = exam_buf[p];
		  }
		  else
			  name_stop = 1;
		  p++;
	  }

	  chatlines[line_now][chat_idx] = 0;

	  _itoa ( getgcn (&chatlines[line_now][name_start]), &convert_buffer[0], 10 );
	  chatlines[line_now][chat_idx++] = 58;
	  chatlines[line_now][chat_idx++] = 0;
	  strcat (&chatlines[line_now][0], &convert_buffer[0]);

	  chat_idx = strlen (&chatlines[line_now][0]);

	  chatlines[line_now][chat_idx++] = 62;
	  chatlines[line_now][chat_idx++] = 32;

	  /* Name and GCN acquired */

	  name_stop = 0;
	  while ((!name_stop) && (p < exam_data_in_buf))
	  {
		  if ((exam_buf[p] == 0x09) || (exam_buf[p] == 0x45)) name_stop = 1;
		  p++;
	  }

	  if (exam_buf[p] == 0x45) 
		  p++;  /* Blah, compatibility. */

	  name_stop = 0;

	  while ((!name_stop) && (p < exam_data_in_buf))
	  {
		  if (exam_buf[p] != 0x00)
		  {
			  if ((exam_buf[p] != 0x09) && (exam_buf[p] != 0x0A))
			  chatlines[line_now][chat_idx++] = exam_buf[p];
		  }
		  else
			  name_stop = 1;
		  p++;
	  }

	  chatlines[line_now][chat_idx] = 0;

	  fp = fopen ( &chat_file_name[0], "a");
	  if (!fp)
	  {
		 printf ("Unable to log to %s\n", &chat_file_name[0]);
	  }
	  fprintf (fp, "%s\n", &chatlines[line_now][0]);
	  fclose (fp);

	  UpdateChatWindow();

  }

#endif

  if ((exam_buf[0] == 0xE6) && (exam_buf[1] == 0x00))
  {
	  my_gcn = *(unsigned *) &exam_buf[16];
  }

  if ((exam_buf[0] == 0x04) && (exam_buf[1] == 0x00) && (exam_buf[2] == 0x2C))
  {
    /* Remember my guild card # */
                
    my_gcn = *(unsigned *) &exam_buf[8];
    *(unsigned *) &gcfsod_pkt[12] = my_gcn;
  }
  /* Schthack server fix */
  
  if ((exam_buf[0] == 0x04) && (exam_buf[1] == 0x00) && (flipped_servers))
  {	  
	  debug ("Applying server-switch fixup.");
	  
	  flipped_servers = 0;

#ifdef EPISODE_3
	  ep3_fixup = 1;
#endif
	  
	  cipher_ptr = &proxy_to_server;

	  save_times = *(unsigned *) &schtfixup[8];
	  save_times ++;
	  *(unsigned *) &schtfixup[8] = save_times;

	  debug ("Forced packet (Decrypted:%u):", sizeof (schtfixup));

	  display_packet (&schtfixup[0], sizeof (schtfixup) );
	  
	  encryptcopy (&server_buf[server_data_in_buf], &schtfixup[0], sizeof(schtfixup));
	  
	  server_data_in_buf += sizeof (schtfixup);	  
  }
  
  /* Ship select packet. */

  if (((exam_buf[0] == 0x07) || (exam_buf[0] == 0xA1)) && (exam_buf[1] != 0x0A) && (exam_buf[1] != 0x01) && (!schthack_mode))
  {
	  ship_size = *(unsigned short *) &exam_buf[2];
	  ship_size += sizeof(schthack_pkt);
	  *(unsigned short *) &exam_buf[2] = ship_size;
	  debug ("Adding Ship/Schthack to the list of ships.");
	  exam_buf[1]++;
	  memcpy (&exam_buf[exam_data_in_buf], &schthack_pkt, sizeof(schthack_pkt));
	  exam_data_in_buf+= sizeof(schthack_pkt);
	  display_packet (&exam_buf[0], exam_data_in_buf);
  }

  /* Ship select packet for Schthack. */

  if (((exam_buf[0] == 0x07) || (exam_buf[0] == 0xA1)) && (exam_buf[1] > 0x02) && (schthack_mode))
  {
	  ship_size = *(unsigned short*) &exam_buf[2];
	  ship_size += sizeof(sega_pkt);
	  *(unsigned short*) &exam_buf[2] = ship_size;
	  debug ("Adding Ship/Sega to the list of ships.");
	  exam_buf[1]++;
	  memcpy (&exam_buf[exam_data_in_buf], &sega_pkt, sizeof(sega_pkt));
	  exam_data_in_buf+= sizeof(sega_pkt);
	  display_packet (&exam_buf[0], exam_data_in_buf);
  }


  /* Rank packet. */
  
	if (exam_buf[0] == 0xB7)
	{
		my_meseta = *(unsigned *) &exam_buf[18];
		my_meseta_max = *(unsigned *) &exam_buf[23];

		/* Unlock all jukebox songs */

		*(unsigned *) &exam_buf[28] = juke_hack;
	}

   /* Ship select packet. */

	if (exam_buf[0] == 0x19)
	{
		select_portidx ( &l_port, &l_idx );
		
		creation_num++;
		listen_creation[l_idx] = creation_num;
		listen_ports[l_idx] = l_port;
		listen_active[l_idx] = 1;
		
		server_ports[l_idx] = *(unsigned short *) &exam_buf[8];
		*(unsigned *) &server_addrs[l_idx].s_addr = *(unsigned *) &exam_buf[4];
		
		debug ("PSO attempting to connect to %s : %u", inet_ntoa(server_addrs[l_idx]), server_ports[l_idx]);
		debug ("Rewriting packet.  Connection rerouted to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);
		
		/* Schthack fix #2 */

		exam_buf[2] = 0x0C;
		exam_data_in_buf = 0x0C;

		*(unsigned *) &exam_buf[4] = *(unsigned *) &proxy_addr[0];
		*(unsigned short *) &exam_buf[8] = listen_ports[l_idx];
		
		/* Setup listener... */
		
		debug ("Setting up listener..");
		
		p_ip.s_addr = INADDR_ANY;
		listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );
		
		tcp_listen (listen_sockfds[l_idx]);

	}
	
	/* Block full. */
	
	if ((exam_buf[0] == 0x11) && ( exam_buf[2] == 0x48 ) && ( exam_buf[3] == 0x00 ) )
	{
		server_port = *(unsigned short *) &exam_buf[exam_data_in_buf-4];
		
		if ((server_port >= 9000) && (server_port < 10000))
		{						
			select_portidx ( &l_port, &l_idx );
			
			server_ports[l_idx] = server_port;
			
			creation_num++;
			listen_creation[l_idx] = creation_num;
			listen_ports[l_idx] = l_port;
			listen_active[l_idx] = 1;
			
			*(unsigned *) &server_addrs[l_idx].s_addr = *(unsigned *) &exam_buf[exam_data_in_buf-8];
			
			debug ("PSO attempting to connect to %s : %u", inet_ntoa(server_addrs[l_idx]), server_port);
			debug ("Rewriting packet.  Connection rerouted to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);
			
			*(unsigned *) &exam_buf[exam_data_in_buf-8] = *(unsigned *) &proxy_addr[0];
			*(unsigned short*) &exam_buf[exam_data_in_buf-4] = listen_ports[l_idx];
			
			/* Setup listener... */
			
			debug ("Setting up listener..");
			
			p_ip.s_addr = INADDR_ANY;
			listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );

			tcp_listen (listen_sockfds[l_idx]);

		}
		
	}
	
	/* Choice search. */
	
	if (exam_buf[0] == 0xC4)
	{
		for (p=0x04;p<exam_data_in_buf;)
		{
			server_port  = *(unsigned short *) &exam_buf[p+108];
			if ( ( server_port >= 9000 ) && ( server_port < 10000 ) )
			{
				select_portidx ( &l_port, &l_idx );
				
				creation_num++;
				listen_creation[l_idx] = creation_num;
				listen_ports[l_idx] = l_port;
				listen_active[l_idx] = 1;
				
				*(unsigned *) &server_addrs[l_idx].s_addr = *(unsigned *) &exam_buf[p+104];
				server_ports[l_idx] = server_port;
				
				debug ("Choice search result %s : %u", inet_ntoa(server_addrs[l_idx]), server_ports[l_idx]);
				debug ("Rewriting packet.  Result changed to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);
				
				*(unsigned *) &exam_buf[p+104] = *(unsigned *) &proxy_addr[0];
				*(unsigned short *) &exam_buf[p+108] = listen_ports[l_idx];
				
				// Setting up listener.
				
				debug ("Setting up listener..");
				
				p_ip.s_addr = INADDR_ANY;
				listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );
				
				tcp_listen (listen_sockfds[l_idx]);
			}
			p += 212;
		}
		
		
	}

	/* Meet the user packet. */

	if ((exam_buf[0] == 0x41) && (exam_buf[4] == 0x00))
	{
		select_portidx ( &l_port, &l_idx );
		
		creation_num++;
		listen_creation[l_idx] = creation_num;
		listen_ports[l_idx] = l_port;
		listen_active[l_idx] = 1;
		
		*(unsigned *) &server_addrs[l_idx].s_addr = *(unsigned *) &exam_buf[20];
		server_ports[l_idx] = *(unsigned short*) &exam_buf[24];
		
		debug ("Meet the user result %s : %u", inet_ntoa(server_addrs[l_idx]), server_ports[l_idx]);
		debug ("Rewriting packet.  Result changed to %u.%u.%u.%u : %u", proxy_addr[0], proxy_addr[1], proxy_addr[2], proxy_addr[3], listen_ports[l_idx]);
		
		*(unsigned *) &exam_buf[20] = *(unsigned *) &proxy_addr[0];
		*(unsigned short *) &exam_buf[24] = listen_ports[l_idx];
		
		// Setting up listener.
		
		debug ("Setting up listener..");
		
		p_ip.s_addr = INADDR_ANY;
		listen_sockfds[l_idx] = tcp_sock_open( p_ip, listen_ports[l_idx] );
		
		tcp_listen (listen_sockfds[l_idx]);
	}

#ifdef EPISODE_3
	
	if ((exam_buf[0] == 0xE7) || (exam_buf[0] == 0xE8))
	{
		/* Need to process Episode 3 data here. */
		allow_quest = 1;
		team_mode = 1;
		lobby_update = 1;

		for (p=0;p<0x0C;p++)
			lobby_user_active[p] = 0;
	}

#endif
	
	/* Users joining or syncing up with lobby? */
	
	if ((exam_buf[0] == 0x67) || (exam_buf[0] == 0x68))
	{
		team_create = 0;
		reset_yes = 0;
		team_mode = 0;
		allow_quest = 0;
		lobby_pktsize = exam_data_in_buf;
		
		memcpy ( &lobby_packet[0], &exam_buf[0], exam_data_in_buf );
		lobby_packet[exam_data_in_buf] = 0x00;
		
		/* If so, go ahead and process the lobby data. */
		
		if (exam_buf[0] == 0x67) lobby_erase = 1; else lobby_erase = 0;
		
		process_lobby_data(lobby_erase);
	}
	
	/* User has left the lobby. */
	
	if ((exam_buf[0] == 0x69) && ( exam_buf[1] < 0x0C))
	{
		if (schthack_mode)
			tlc = exam_buf[4]; else
			tlc = exam_buf[1];
		lobby_user_active[tlc] = 0;
		lobby_update = 1;
	}
	
	/* Joining a team or another user has joined this team. */
	
	if ((exam_buf[0] == 0x64) || (exam_buf[0] == 0x65))
	{								
		memcpy ( &lobby_packet[0], &exam_buf[0], exam_data_in_buf );
		lobby_packet[exam_data_in_buf] = 0x00;
		lobby_pktsize = exam_data_in_buf;
		
		
		if ( exam_buf[0] == 0x64 )
		{
			if (!team_create)
			{
				reset_yes = 1;
			}
			allow_quest = 1;
			team_mode = 1;
			lobby_erase = 1;
		}
		else
		{
			team_mode = 2;
			lobby_erase = 0;
		}
		
		process_lobby_data(lobby_erase);
	}
	
	/* A user has left the team. */
	
	if ((exam_buf[0] == 0x66) && ( exam_buf[1] < 0x04))
	{
		if (schthack_mode)
			tlc = exam_buf[4]; else
			tlc = exam_buf[1];
		lobby_user_active[tlc] = 0;
		lobby_update = 1;
	}
	
	// debug ("Firewall: Analyzing sub-packet %s (%u)", hexByte (exam_buf[0]), exam_data_in_buf );
	

	if ( check_packet ( &exam_buf[0] ) )
		packet_filtered = 1;

	/* Reencrypt data and pass it through to client. */
	
	cipher_ptr = &proxy_to_client;
	
	encryptcopy (&client_buf[client_data_in_buf], &exam_buf[0], exam_data_in_buf );
	
	client_data_in_buf += exam_data_in_buf; /* lol */

	if ( packet_filtered )
	{
		debug ("Firewall: Filtered out bad data.");
	}
	
	exam_data_in_buf = 0;

}

int isinstr ( const char *sstr, char* s )
{
	int l, l2, j;
	char buf[512];
	
	l = strlen ( sstr );
	l2 = strlen ( s );
	buf [l] = 0;
	for (j=0;j<l2-l;j++)
	{
		memcpy ( &buf[0], &s[j], l );
		if ( !strcmp ( &buf[0], sstr ) ) return 1;
	}
	return 0;
}


/* MAC address grabbing stuff. */

unsigned char client_macaddr[13];

typedef struct _ASTAT_
{
	
	ADAPTER_STATUS adapt;
	NAME_BUFFER    NameBuff [30];
	
}ASTAT, * PASTAT;

ASTAT Adapter;

void grabmacaddr (void)
{
	NCB Ncb;
	UCHAR uRetCode;
	//char NetName[50];
	LANA_ENUM   lenum;
	int      i;
	
	memset( &Ncb, 0, sizeof(Ncb) );
	Ncb.ncb_command = NCBENUM;
	Ncb.ncb_buffer = (UCHAR *)&lenum;
	Ncb.ncb_length = sizeof(lenum);
	uRetCode = Netbios( &Ncb );
	//printf( "The NCBENUM return code is: 0x%x \n", uRetCode );
	
	for(i=0; i < lenum.length ;i++)
	{
		memset( &Ncb, 0, sizeof(Ncb) );
		Ncb.ncb_command = NCBRESET;
		Ncb.ncb_lana_num = lenum.lana[i];
		
		uRetCode = Netbios( &Ncb );
		//printf( "The NCBRESET on LANA %d return code is: 0x%x \n",
		//        lenum.lana[i], uRetCode );
		
		memset( &Ncb, 0, sizeof (Ncb) );
		Ncb.ncb_command = NCBASTAT;
		Ncb.ncb_lana_num = lenum.lana[i];
		
		strcpy( Ncb.ncb_callname,  "*               " );
		Ncb.ncb_buffer = (char *) &Adapter;
		Ncb.ncb_length = sizeof(Adapter);
		
		uRetCode = Netbios( &Ncb );
		//printf( "The NCBASTAT on LANA %d return code is: 0x%x \n",
		//        lenum.lana[i], uRetCode );
		if ( uRetCode == 0 )
		{
			client_macaddr[0] = hexTable[Adapter.adapt.adapter_address[0] >> 4];
			client_macaddr[1] = hexTable[Adapter.adapt.adapter_address[0] & 15];
			client_macaddr[2] = hexTable[Adapter.adapt.adapter_address[1] >> 4];
			client_macaddr[3] = hexTable[Adapter.adapt.adapter_address[1] & 15];
			client_macaddr[4] = hexTable[Adapter.adapt.adapter_address[2] >> 4];
			client_macaddr[5] = hexTable[Adapter.adapt.adapter_address[2] & 15];
			client_macaddr[6] = hexTable[Adapter.adapt.adapter_address[3] >> 4];
			client_macaddr[7] = hexTable[Adapter.adapt.adapter_address[3] & 15];
			client_macaddr[8] = hexTable[Adapter.adapt.adapter_address[4] >> 4];
			client_macaddr[9] = hexTable[Adapter.adapt.adapter_address[4] & 15];
			client_macaddr[10] = hexTable[Adapter.adapt.adapter_address[5] >> 4];
			client_macaddr[11] = hexTable[Adapter.adapt.adapter_address[5] & 15];
			client_macaddr[12] = 0;
			
		}
	}
	
}

/* MAC address stuff end. */

void CP (char* pkt, unsigned pkl)
{
	// Sends a custom packet to BOTH the client and server WITHOUT checking if it's volatile.

	debug ("Custom Packet (%u):", pkl);

	display_packet (pkt, pkl);

	cipher_ptr = &proxy_to_server;
	encryptcopy (&server_buf[server_data_in_buf], pkt, pkl);
	server_data_in_buf += pkl;
				
	cipher_ptr = &proxy_to_client;
	encryptcopy ( &client_buf[client_data_in_buf], pkt, pkl );
	client_data_in_buf += pkl;
}


  /* Pointer to the actual .IND file. */

  FILE *INDFile;

  /* Using v1.1 IND? */

  int INDv11 = 0;

  /* Storage space for the identifier value which is right at the beginning of an .IND file. */

  unsigned INDIdentifier;

  /* Variable for the length of the command line description text. */

  unsigned INDDescLen;

  /* Pointer to the actual command line description text. */

  char* INDDesc;

  /* Variable for the number of commands inside of the .IND file.

  Note: Editing this text is not supported in the .IND file editor.  You can use INDMerge for that.

  */

  unsigned INDNumCommands = 0;

  /* Variables for the packet lengths of each packet. */

  unsigned INDPktLen[256];
 
  /* Variables for the actual packets themselves. */
  
  unsigned char INDPkt[256][8192];

  /* Packet key as read from the file. */

  unsigned short INDPktKeys[256];

  /* Targets */

  unsigned short INDTarget[256];
  char query_string[BUF_SIZE];
  unsigned char auth_buf[BUF_SIZE+32];
  unsigned char decrypt_buf[BUF_SIZE+32];
  unsigned char encrypt_buf[BUF_SIZE+32];
  unsigned char c_encrypt_buf[BUF_SIZE+32];
  unsigned char s_encrypt_buf[BUF_SIZE+32];
  int encrypt_idx, header_idx = 0, c_header_idx = 0;
  unsigned short c_header_left = 4, header_left = 4;
  int got_header = 0, c_got_header = 0;
  int wserror;
  int tmp_sockfd,  a_sockfd, p_sockfd, pc_sockfd, s_sockfd, pkt_len, left_over = 0;
  struct in_addr p_ip;
  struct sockaddr_in csa;
  struct sockaddr_in autha;
  struct sockaddr_in mya;
  int mya_len = sizeof (mya);
  unsigned int csa_len;
  fd_set ReadFDs, WriteFDs, ExceptFDs;
  //unsigned char *check_buf;
  int bytes_sent = 0;
  int ch;
  int pktKeys_deleted = 0;
  unsigned char my_target = 0;
  int process_UserPkts = 0;
  int shift_down = 0;
  int tab_down = 0;
  struct in_addr pso_ip;
  struct hostent *vs_host;
  unsigned client_addr, compare_addr;  
  unsigned short pso_p, original_p;
  char PSO_IP[17];
  char pb_addr[4];
  unsigned short pkt_bytes_left = 0;
  unsigned short cpkt_bytes_left = 0;
  struct in_addr ip;
  int numread, sockfd;
  struct udp_packet pkt;
  int no_windows = 0, arg_idx;

/*****************************************************************************/
int main(int argc, char *argv[])
{ 
#ifndef FIREWALL_ONLY

  /* F keys included. */

  unsigned short FKeys_Down[256];

#ifndef REMOTE_MODE

#ifndef NO_IND
  
  /* Dummy variable used during file reads to ensure that data was read correctly. */

  unsigned readOK;

  unsigned short tPos;

  int kidx;

  /* Keys down? */

  unsigned short INDPktKeys_Down[256];


#endif

#endif


#ifndef REMOTE_MODE
  MSG msg;
#endif

#endif

/* Packet read counter */

  unsigned p,ip_s1,ip_s2,ip_s3,ip_s4,use_public_ip;


  WSADATA winsock_data;

  for (ch=0;ch<0x0C;ch++)
  {
	  lobby_user_active[ch] = 0;
  }

  log_file_name[0] = 0;
  chat_file_name[0] = 0;
  trail_work[0] = 0;

  strcat (&log_file_name[0], "spsof ");
  strcat (&chat_file_name[0], "chat ");
  GetLocalTime ( &now_t );
  _itoa ( now_t.wMonth, &convert_buffer[0], 10 );
  strcat (&trail_work[0], &convert_buffer[0]);
  strcat (&trail_work[0], "_");
  _itoa ( now_t.wDay, &convert_buffer[0], 10 );
  strcat (&trail_work[0], &convert_buffer[0]);
  strcat (&trail_work[0], "_");
  _itoa ( now_t.wYear, &convert_buffer[0], 10 );
  strcat (&trail_work[0], &convert_buffer[0]);
  strcat (&trail_work[0], "_");
  _itoa ( now_t.wHour, &convert_buffer[0], 10 );
  strcat (&trail_work[0], &convert_buffer[0]);
  strcat (&trail_work[0], "_");
  _itoa ( now_t.wMinute, &convert_buffer[0], 10 );
  strcat (&trail_work[0], &convert_buffer[0]);
  strcat (&trail_work[0], ".txt");
  strcat (&log_file_name[0], &trail_work[0]);
  strcat (&chat_file_name[0], &trail_work[0]);
  debug ("Logging session to: %s", &log_file_name[0] );

#ifndef NO_CHAT
  debug ("Logging chat to: %s", &chat_file_name[0] );
#endif

  WSAStartup(MAKEWORD(1,1), &winsock_data);
 

#ifdef NO_WINDOWS
	START_PORT = 10020;
#endif

#ifdef REMOTE_MODE
	START_PORT = 10020;
#endif

  debug ("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");

#ifndef FIREWALL_ONLY
#ifndef GOODY_GOODY
  debug ("Phantasy Star Online Gamecube FireWall & Packet Editor v%s", SPSOF_VERSION);
#else
  debug ("Phantasy Star Online Gamecube FireWall & Cheat Tool v%s", SPSOF_VERSION);
#endif

#else
  debug ("Phantasy Star Online Gamecube FireWall Only Edition v%s", SPSOF_VERSION);
#endif

  debug ("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
  debug ("DNS functions by Matthew Pratt (dproxy author http://dproxy.sourceforge.net/)");
  debug ("PSO encryption functions by Myria");
  debug ("Proxy, FireWall & Packet code by Sodaboy");
  debug ("Program (c) 2005,2007 Sodaboy (ragnarok@vengefulsoda.com)");
  debug ("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
#ifdef REMOTE_MODE 
  debug ("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
	debug ("This version is compiled to accept a remote connection.");
  debug ("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=\n");
#endif


#ifndef NO_IND

#ifndef FIREWALL_ONLY

  /* Grab packets from editor file */

  /* Attempt to open up the .IND file for reading. */
	
	debug ("Loading packet control file: spsofedit.ind  ");
	if (  ( INDFile = fopen ("spsofedit.ind", "rb") ) == NULL )
	{
		debug_perror ("Failed to open spsofedit.ind for editing.\n");
		exit (1);
	}
	readOK = fread ( &INDIdentifier, 4, 1, INDFile );
	if ( readOK != 1 )
	{
		debug_perror ("Unexpected end of file.\n");
		exit (2);
	}

	if (INDIdentifier == 0xDEADBEEF)
	{
		INDv11 = 1;
	}

	/* Read the length of the command line description text. */
	
	readOK = fread ( &INDDescLen, 4, 1, INDFile );
	if ( readOK != 1 )
	{
		debug_perror ("Unexpected end of file.\n");
		exit (2);
	}

	/* Allocate memory for the description text and read it from the file. */
	
	INDDesc = (char*) malloc ( INDDescLen );
	
	readOK = fread ( INDDesc, INDDescLen, 1, INDFile );
	if ( readOK != 1 )
	{
		debug_perror ("Unexpected end of file.\n");
		exit (2);
	}

	INDDesc[INDDescLen] = 0;

	/* Read the number of packet commands from the file. */
	
	readOK = fread ( &INDNumCommands, 4, 1, INDFile );
	if ( readOK != 1 )
	{
		debug_perror ("Unexpected end of file.\n");
		exit (2);
	}

	pktKeys_deleted = 0;

	/* Read all packets from the file. */

	for (p=0;p<INDNumCommands;p++)
	{
		kidx = p - pktKeys_deleted;

		/* Read virtual key assigned to packet. */

		readOK = fread ( &INDPktKeys[kidx], 2, 1, INDFile );
		if ( readOK != 1 )
		{
			debug_perror ("Unexpected end of file.\n");
			exit (2);
		}

		if ((( INDPktKeys[kidx] < 0x0070 ) && ( INDPktKeys[kidx] > 0x007B )) || (INDPktKeys[kidx] == 0x00A1) || (INDPktKeys[kidx] == 0x00A3) || (INDPktKeys[kidx] == 0x0009))
		{
			debug ("A packet was ignored in the .IND file.\nPlease do not use reserved keys. (Fx, Right Ctrl, Right Shift, TAB)");
			pktKeys_deleted++;
		}
			
		/* Get key state. */
		
		GetAsyncKeyState(INDPktKeys[kidx]);

		/* Set keys down variable for key to 0. */

		INDPktKeys_Down[kidx] = 0;

		/* Supporting targetting? */

		if ( INDv11 )
		{
			readOK = fread ( &INDTarget[kidx], 2, 1, INDFile );
			if ( readOK != 1 )
			{
				debug_perror ("Unexpected end of file.\n");
				exit (2);
			}
		}
		else
		{
			INDTarget[kidx] = 0;
		}

		/* Read length of packet. */

		readOK = fread ( &INDPktLen[kidx], 4, 1, INDFile );
		if ( readOK != 1 )
		{
			debug_perror ("Unexpected end of file.\n");
			exit (2);
		}

		/* Read the actual packet. */
		readOK = fread ( &INDPkt[kidx][0], INDPktLen[kidx], 1, INDFile );
		if ( readOK != 1 )
		{
			debug_perror ("Unexpected end of file.\n");
			exit (2);
		}

	}

	fclose (INDFile);

	/* Adjust for # of commands deleted. */

	INDNumCommands -= pktKeys_deleted;

#endif

#endif

  pso_p = PSO_PORT;
  
  for (p=0;p<10;p++) 
	  null_outfit[p] = 0x00;

		  for (arg_idx=1;arg_idx<argc;arg_idx++)
		  {
			  switch (argv[arg_idx][0])
			  {
			  case 45:
				  if ((argv[arg_idx][1] == 119) || (argv[arg_idx][1] == 87))
				  {
					  debug ("All windows disabled.\n");
					  no_windows = 1;
				  }

				  if ((argv[arg_idx][1] == 80) || (argv[arg_idx][1] == 112))
				  {
					  if (strlen(&argv[arg_idx][2]) > 2)
					  {
						  cranberry_port = atoi ( &argv[arg_idx][2] );
						  cranberry_mode = 1;
						  debug ("Cranberry mode activated.  Forcing listen port to %u.", cranberry_port);
					  }
				  }

				  break;
#ifndef EPISODE_3
				  case 85:
					  if (strlen(argv[arg_idx]) > 1)
					  {
						  pso_region = 1;
						  if (argv[arg_idx][1] == 80)
						  {
							  pso_p = 9103;
							  debug ("Connection parameters set to USA version PSO Plus.");
						  }
						  else
						  {
							  pso_p = 9100;
							  debug ("Connection parameters set to USA version PSO v1.0 or v1.1");
						  }
					  }
					  break;

				  case 74:
					  if (strlen(argv[arg_idx]) > 1)
					  {
						  pso_region = 0;
						  if (argv[arg_idx][1] == 80)
						  {
							  pso_p = 9001;
							  debug ("Connection parameters set to JP version 1.1 or PSO Plus.");
						  }
						  else
						  {
							  pso_p = 9000;
							  debug ("Connection parameters set to JP version 1.0");
						  }
					  }
					  break;

				  case 69:
					  if (strlen(argv[arg_idx]) > 1)
					  {
						  pso_region = 2;
						  if (argv[arg_idx][1] == 80)
						  {
							  pso_p = 9201;
							  debug ("Connection parameters set to EU version 1.1");
						  }
						  else
						  {
							  pso_p = 9200;
							  debug ("Connection parameters set to EU version 1.0");
						  }
					  }
					  break;


#else

				  case 85:
					  if (strlen(argv[arg_idx]) > 1)
					  {
						  pso_region = 1;
						  pso_p = 9103;
						  debug ("Connection parameters set to USA version PSO Episode III.");
					  }
					  break;

				  case 74:
					  if (strlen(argv[arg_idx]) > 1)
					  {
						  pso_region = 0;
						  pso_p = 9003;
						  debug ("Connection parameters set to JP version PSO Episode III.");
					  }
					  break;

				  case 69:
					  if (strlen(argv[arg_idx]) > 1)
					  {
						  pso_region = 2;
						  pso_p = 9203;
						  debug ("Connection parameters set to EU version PSO Episode III.");
					  }
					  break;

#endif
				  default:
					  break;
			  }
		  }


  schthack_host = gethostbyname ( "gsproduc.ath.cx" );

  
  if (!schthack_host) {
	  debug_perror ("Unable to resolve gsproduc.ath.cx\n");
	  exit (1);
  }


  memcpy(&pso_ip.s_addr, schthack_host->h_addr, schthack_host->h_length);
  *(unsigned *) &schtsrv_addr[0] = *(unsigned *) &pso_ip.s_addr;
  
#ifdef SCHTHACK

  schthack_mode = 1;

#endif

	vs_host = gethostbyname ( "www.pioneer2.net" );
	if (!vs_host) {
		debug_perror ("Cannot obtain remote IP information.\n");
		exit (1);
	}

	/* Create a reliable, stream socket using TCP */
	if ((a_sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	{
		debug_perror ("Unable to create socket.");
		exit(1);
	}

    /* Construct the server address structure */
    memset(&autha, 0, sizeof(autha)); /* Zero out structure */
    autha.sin_family      = AF_INET; /* Internet address family */

	memcpy(&autha.sin_addr.s_addr, vs_host->h_addr, 4); /* Server IP address */

    autha.sin_port        = htons(80); /* Server port */

    /* Establish the connection to the SPSOF authentication server. */

    if (connect(a_sockfd, (struct sockaddr *) &autha, sizeof(autha)) < 0)
	{
		debug_perror ("Cannot connect to authorization server!");
		exit(1);
	}

	/* Mrr? */

	getsockname ( a_sockfd, (struct sockaddr*) &mya, &mya_len );

	/* Copy proxy server address. */

#ifdef EPISODE_3

	debug ("This version of the proxy is designed to be used for Episode 3.\nIt will not work with Episode I & II.\n");

#endif

use_public_ip = 0;

if (pso_p == 9103)
	use_public_ip = 1;

#ifdef EPISODE_3
 use_public_ip = 1;
#endif

#ifdef REMOTE_MODE
 use_public_ip = 1;
#endif

#ifdef USE_PRIVATE_IP
 use_public_ip = 0;
#endif


	if (use_public_ip)
	{
		send_to_server ( a_sockfd, HTTP_REQ );
		
		receive_from_server ( a_sockfd, &auth_buf[0] );
		
		ip_s1 = 0;
		ip_s2 = 0;
		ip_s3 = 0;
		ip_s4 = 0;
		
		
		for (p=0;p<strlen(&auth_buf[0]);p++)
		{
			if ((auth_buf[p] == 0x20) && (auth_buf[p+1] == 105) && (auth_buf[p+2] == 115) && (auth_buf[p+3] == 0x20))
			{
				ip_s1 = p+4;
				break;
			}
		}
		
		if (!ip_s1) 
		{
			receive_from_server ( a_sockfd, &auth_buf[0] );
			for (p=0;p<strlen(&auth_buf[0]);p++)
			{
				if ((auth_buf[p] == 0x20) && (auth_buf[p+1] == 105) && (auth_buf[p+2] == 115) && (auth_buf[p+3] == 0x20))
				{
					ip_s1 = p+4;
					break;
				}
			}

			if (!ip_s1)
			{
				debug ("Can't process remote IP information. (Try again?)\n");
				exit (1);
			}
		}
		
		for (p=ip_s1;p<strlen(&auth_buf[0]);p++)
		{
			if (auth_buf[p] == 46)
			{
				if (ip_s2 == 0) ip_s2 = p+1; else
					if (ip_s3 == 0) ip_s3 = p+1; else
						ip_s4 = p+1;
			}
		}
		
		//debug ("ip_s1 = %u, ip_s2 = %u, ip_s3 = %u, ip_s4 = %u", ip_s1, ip_s2, ip_s3, ip_s4);
		
		for (p=ip_s4;p<strlen(&auth_buf[0]);p++)
		{
			if (auth_buf[p] == 60) 
			{
				auth_buf[p] = 0;
				break;
			}
		}
		
		auth_buf[ip_s3-1] = 0;
		auth_buf[ip_s2-1] = 0;
		
		ip_s1 = atoi (&auth_buf[ip_s1]);
		ip_s2 = atoi (&auth_buf[ip_s2]);
		ip_s3 = atoi (&auth_buf[ip_s3]);
		ip_s4 = atoi (&auth_buf[ip_s4]);
		
		//debug ("ip_s1 = %u, ip_s2 = %u, ip_s3 = %u, ip_s4 = %u\n", ip_s1, ip_s2, ip_s3, ip_s4);
		
		pb_addr[0] = (unsigned char) ip_s1;
		pb_addr[1] = (unsigned char) ip_s2;
		pb_addr[2] = (unsigned char) ip_s3;
		pb_addr[3] = (unsigned char) ip_s4;
		
		*(unsigned *) &mya.sin_addr.S_un.S_addr = *(unsigned *) &pb_addr;
		
	}

	debug ("Proxy server IP: %s", inet_ntoa (mya.sin_addr) );

	*(unsigned *) &proxy_addr[0] = *(unsigned *) &mya.sin_addr.S_un.S_addr;


  closesocket (a_sockfd);

#ifndef SKIP_DNS

  /* Open port 53 for DNS requests. */

  debug ("Opening UDP port 53 for DNS requests.");

  ip.s_addr = INADDR_ANY;
  sockfd = udp_sock_open( ip, PORT );

  debug ("Waiting for DNS request...");

  /* when the child process that handles the actual response exits, we must
	* call wait so that we dont get a zombie */
	numread = udp_packet_read( sockfd, &pkt );	 
	if( numread < 0 ) {
		debug_perror("no data ...\n");
		exit(1);
		
	}
	if(numread < sizeof(struct dns_header)+1 ) {
		debug_perror("got packet with invalid data size...\n");
		exit(1);
	}
	debug("DNS query from %s:%d", inet_ntoa(pkt.src_ip), pkt.src_port);


  strcpy( query_string, pkt.buf );
  decode_domain_name( query_string );
  debug("query: %s", query_string );
 
  /* check if it is a query.... And the type... */
  if (dns_is_query(&pkt)) {
	 switch (dns_get_opcode(&pkt)) { 
	 case 0:
		dns_send_reply( sockfd, &pkt, query_string );
		break;
	 case 1:
		debug_perror ("Reverse lookup disabled.\n");
		exit(1);
		//dns_reverse_lookup( sockfd, &pkt, query_string );
		break;
	 default:
		debug_perror("unsupported opcode");
		exit(1);
		break;
	 }
  }
  debug("Done.");

  debug ("Closing DNS port.\n");

  /* Close DNS socket. */
  closesocket(sockfd);

#else

  debug ("Skipping DNS procedure.\n");

#endif

  if ( cranberry_mode )
  {
	  /* Force a port and region. */
	  
	  original_p = pso_p;
	  
	  pso_p = cranberry_port;

  }

  debug ("Opening TCP port %u for connections.", pso_p);

  /* Wait for a connection on TCP port [PSO LOGIN SERVER] */

  p_ip.s_addr = INADDR_ANY;
  p_sockfd = tcp_sock_open( p_ip, pso_p );

  /* Listen for connection. */

  debug ("Waiting for connection.");

  tcp_listen (p_sockfd);

  /* Accept connection. */

  debug ("Attempting to accept connection.");

  csa_len = sizeof (csa);
 
  pc_sockfd = tcp_accept ( p_sockfd, (struct sockaddr*) &csa, &csa_len );

  debug ("tcp_accept %s:%u", inet_ntoa (csa.sin_addr), csa.sin_port );

  *(unsigned *) &client_addr = *(unsigned *) &csa.sin_addr.S_un.S_addr;

  debug ("Connection successful.");

  /* Make a connection to it... */

  if ( pso_region == 0 )
  {
	  sega_addr[0] = 218;
	  sega_addr[1] = 42;
	  sega_addr[2] = 128;
	  sega_addr[3] = 17;
	  strcpy ( &PSO_IP[0], "218.42.128.17\0" );
  }

  if ( pso_region == 1 )
  {
	  sega_addr[0] = 64;
	  sega_addr[1] = 95;
	  sega_addr[2] = 121;
	  sega_addr[3] = 96;
	  strcpy ( &PSO_IP[0], "64.95.121.96\0" );
  }

  if ( pso_region == 2 )
  {
  	  sega_addr[0] = 62;
	  sega_addr[1] = 73;
	  sega_addr[2] = 186;
	  sega_addr[3] = 200;
	  strcpy ( &PSO_IP[0], "62.73.186.200\0" );
  }

#ifndef SCHTHACK

  if ( cranberry_mode )
  {
	  /* Flip switch back to original port. */
	  pso_p = original_p;
  }

  debug ("Attempting connection to %s:%u ...", PSO_IP, pso_p );
  s_sockfd = tcp_sock_connect ( PSO_IP, pso_p );

#else

  debug ("Attempting connection to %s:%u ...", inet_ntoa (pso_ip), pso_p );

  s_sockfd = tcp_sock_connect ( inet_ntoa (pso_ip) , pso_p );

#endif

 schtsrv_port = pso_p;

 sega_port = pso_p;


  debug ("Connection successful.\n");

  /* Start proxying! */

  debug ("Begin data parsing.\n");

  /* Set up pointers to procedures */
  
  server_to_client.mangle = &gcmangle;
  server_to_client.advance = &gcadvance;
  server_to_client.initialize = &gcinitialize;

  proxy_to_client.mangle = &gcmangle;
  proxy_to_client.advance = &gcadvance;
  proxy_to_client.initialize = &gcinitialize;

  client_to_server.mangle = &gcmangle;
  client_to_server.advance = &gcadvance;
  client_to_server.initialize = &gcinitialize;

  proxy_to_server.mangle = &gcmangle;
  proxy_to_server.advance = &gcadvance;
  proxy_to_server.initialize = &gcinitialize;
 
  *(unsigned *) &item_param_2 = zero_dword;
  *(unsigned *) &item_param_3 = zero_dword;
  *(unsigned *) &item_param_4 = zero_dword;
  
#ifndef FIREWALL_ONLY

#ifndef GOODY_GOODY
  
  /* Loading autoown.txt */

	debug ("Loading shit list file: autoown.txt  ");
	if (  ( OwnFile = fopen ("autoown.txt", "rb") ) == NULL )
	{
		debug ("File not present.\n");
	}
	else
	{
		while (fgets (&OwnData[0], 255, OwnFile) != NULL)
		{
			auto_own_list[own_count++] = atoi (&OwnData[0]);
			debug ("GCN # %u added to shit list.", auto_own_list[own_count-1]);
			if (fgets (&OwnData[0], 255, OwnFile) == NULL)
				break;
		}
		fclose ( OwnFile );
	}

	/* Reset status of F keys. */

	for (p=0;p<0x0C;p++)
	{
		GetAsyncKeyState (p+0x70);
		FKeys_Down[p] = 0;
	}

  /* Reset status of Right Shift. */

  GetAsyncKeyState (0x00A1);

  /* Reset status of Right Ctrl. */

  GetAsyncKeyState (0x00A3);

  shift_down = 0;
  
  /* Reset status of TAB. */

  GetAsyncKeyState (0x0009);

  tab_down = 0;

#endif

#endif

  for (p=0;p<MAX_PORTS;p++)
  {
	  listen_ports[p] = 0;
	  listen_creation[p] = 0;
	  listen_active[p] = 0;
  }

// We need to close the original login port.  (Remote use)

  listen_ports[0] = pso_p;
  listen_active[0] = 2;

#ifdef NO_WINDOWS
  no_windows = 1;
#endif


  if (!no_windows) {
	  
#ifndef FIREWALL_ONLY
	  
#ifndef REMOTE_MODE
	  
#ifndef GOODY_GOODY
	  
	  // create a new thread to allow user input 
	  if( _beginthread(InputThreadProc, 0, NULL )==-1) 
	  { 
		  debug_perror("Failed to create thread"); 
		  exit(1);
	  }	
	  
#endif
	  
	  /*DisplayCommands(INDDesc);*/
	  
	  CreateLobbyWindow();
	  
	  for (p=0;p<29;p++)
		  chatlines[p][0] = 0;
	  
#endif
	  
#endif
	  
#ifndef REMOTE_MODE
	  
#ifndef NO_CHAT
	  
	  // create a new thread to allow user input 
	  if( _beginthread(ChatThreadProc, 0, NULL )==-1) 
	  { 
		  debug_perror("Failed to create thread"); 
		  exit(1);
	  }
	  
#endif
	  
#endif
	  
  }



  for (;;)
  {
	if (IsDebuggerPresent())
	{
		printf ("Debugger attached.  Aborting program!\n");
		exit(1);
	}

#ifndef REMOTE_MODE

	 /* Check for custom data in buffer. */

	if ( ( god_mode ) && ( team_mode ) )
	{

		god_offset = 0;

		god_tick++;
		
		if (god_tick == 30)
		{
			if ( god_mode == 2 )
			{
				// God mode for ALL (HP++)
				// 6+god_offset = client ID
				// 10+god_offset = recovery mode (3 = HP, 4 = TP)

				god_pkt[6+god_offset] = 0;
				god_pkt[10+god_offset] = 3;
				
				CP ( &god_pkt[0], sizeof (god_pkt) );

				god_pkt[6+god_offset] = 1;
				
				CP ( &god_pkt[0], sizeof (god_pkt) );
							
				god_pkt[6+god_offset] = 2;
				
				CP ( &god_pkt[0], sizeof (god_pkt) );
				
				god_pkt[6+god_offset] = 3;
				
				CP ( &god_pkt[0], sizeof (god_pkt) );				
			}
			else
			{
				// God mode for user only. (HP++)

				god_pkt[6+god_offset] = my_clientid;
				god_pkt[10+god_offset] = 3;

				CP ( &god_pkt[0], sizeof (god_pkt) );				
			}
		}
		if (god_tick == 60)
		{
			if (god_mode == 2)
			{
				// God mode for ALL (TP++)

				god_pkt[6+god_offset] = 0;
				god_pkt[10+god_offset] = 4;
				
				CP ( &god_pkt[0], sizeof (god_pkt) );

				god_pkt[6+god_offset] = 1;
				
				CP ( &god_pkt[0], sizeof (god_pkt) );
							
				god_pkt[6+god_offset] = 2;
				
				CP ( &god_pkt[0], sizeof (god_pkt) );
				
				god_pkt[6+god_offset] = 3;

				CP ( &god_pkt[0], sizeof (god_pkt) );				
				
			}
			else
			{
				// God mode for user only. (TP++)

				god_pkt[6+god_offset] = my_clientid;
				god_pkt[10+god_offset] = 4;

				CP ( &god_pkt[0], sizeof (god_pkt) );				
			}
			
			god_tick = 0;
		}
	}

	if ( ( custom_data_in_buf ) && ( !corrupt_now ) )
	{
		debug ("Custom Packet (%u):", custom_data_in_buf);

		display_packet (&custom_buf[0], custom_data_in_buf);

		cipher_ptr = &proxy_to_server;

		encryptcopy (&server_buf[server_data_in_buf], &custom_buf[0], custom_data_in_buf);

		server_data_in_buf += custom_data_in_buf;

		/* Also send the packet to the client */

		if ( custom_buf[0] == 0x60 )
		{
			if ( !check_packet ( &custom_buf[0] ) )
			{
				cipher_ptr = &proxy_to_client;

				encryptcopy (&client_buf[client_data_in_buf], &custom_buf[0], custom_data_in_buf);

				client_data_in_buf += custom_data_in_buf;
			}
		}

		custom_data_in_buf = 0;
	}

#ifndef FIREWALL_ONLY

	else
	{
		/* Some delays are needed to properly corrupt someone. */

		if ( corrupt_now )
		{
			GetSystemTime ( &now_t );
			check_boom = ( ( ( ( int ) now_t.wHour * 3600 ) ) + ( now_t.wMinute * 60 ) + ( now_t.wSecond ) );
			check_boom *= 1000;
			check_boom += now_t.wMilliseconds;
			if ( check_boom >= boom_time )
			{
				corrupt_now = 0;
			}
		}
	}


	if (!no_windows)
	{
		
		/* Might as well stop the stupid windows from freezing. */
		
		if ( lobbyhWnd != NULL )
		{
			if ( lobby_update == 1 ) {
				UpdateLobbyWindow();
			}
			PeekMessage(&msg, lobbyhWnd, 0, 0, PM_REMOVE);
			TranslateMessage (&msg);
			DispatchMessage (&msg);
		}
		
		/*
		if ( cmdhWnd != NULL )
		{
		PeekMessage(&msg, cmdhWnd, 0, 0, PM_REMOVE);
		TranslateMessage (&msg);
		DispatchMessage (&msg);
	}  */
		
	}


#ifndef NO_IND
	
	/* Check key presses. */

	if (process_UserPkts) 
	{		
		for (p=0;p<INDNumCommands;p++)	
		{
			if KEYDOWN(INDPktKeys[p])
			{
				if (INDPktKeys_Down[p] == 0)
				{
					INDPktKeys_Down[p] = 1;
					
					pkt_len = INDPktLen[p];
					
					debug ("User Initiated Packet (%u):", pkt_len);
					
					/* Target appropriate user. */
					
					if (INDTarget[p])
					{
						debug ("Client ID target: %s", hexByte ( my_target ) );
						tPos = INDTarget[p];
						INDPkt[p][tPos] = my_target;
					}
					
					display_packet (&INDPkt[p][0], pkt_len);
					
					cipher_ptr = &proxy_to_server;
				
					encryptcopy (&server_buf[server_data_in_buf], &INDPkt[p][0], pkt_len);
					
					server_data_in_buf += pkt_len;
					
					/* Also send the packet to the client */
					
					memcpy ( &decrypt_buf[0], &INDPkt[p][0], pkt_len );
					
					if ( decrypt_buf[0] == 0x60 )
					{
						if ( ! check_packet ( &decrypt_buf[0] ) )
						{
							cipher_ptr = &proxy_to_client;
							
							encryptcopy (&client_buf[client_data_in_buf], &INDPkt[p][0], pkt_len);
							
							client_data_in_buf += pkt_len;
						}
					}
				}
				
			}
			else
			{
				/* Reset keys down status. */
				
				INDPktKeys_Down[p] = 0;
			}
		}
		
		/* Targets ? */
		
		for (p=0;p<0x0C;p++)	
		{
			if KEYDOWN((p + 0x70))
			{
				if (FKeys_Down[p] == 0)
				{
					FKeys_Down[p] = 1;
					
					my_target = p;
					
					debug ("You are now targetting client ID: %s", hexByte ( my_target ) );
				}
				
			}
			else
			{
				/* Reset keys down status. */
				
				FKeys_Down[p] = 0;
			}
		}
		
		/* Card steal! */
		
		if KEYDOWN(0x0009)
		{
			if ( tab_down == 0 )
			{
				if (lobby_user_active[my_target])
				{
					debug ("Stealing card of %s. (152):", &lobby_name[my_target]);
					gc_data[1] = my_clientid;
					*(unsigned *) &gc_data[12] = lobby_gcn[my_target];
					memcpy (&gc_data[16], &lobby_name[my_target], 12);
					memcpy (&gc_data[42], &lobby_name[my_target], 12);
					*(unsigned short *) &gc_data[150] = lobby_class[my_target];
					display_packet (&gc_data[0], 152);
					
					cipher_ptr = &proxy_to_server;
					
					encryptcopy (&server_buf[server_data_in_buf], &gc_data[0], 152);
					
					server_data_in_buf += 152;
				}
				else
				{
					debug ("No user to steal card from.");
				}
				tab_down = 1;
			}
		}
		else
		{
			tab_down = 0;
		}
		
   }

   if (!no_windows)
   {
	   
	   /* Toggle toggle for commands */
	   
	   if (KEYDOWN(0x00A1) && KEYDOWN(0x00A3))
	   {
		   if ( shift_down == 0 )
		   {
			   if (process_UserPkts) 
			   {
				   debug ("No longer processing user-initated packets.\nPress RIGHT SHIFT and RIGHT CONTROL to re-enable.");
				   process_UserPkts = 0;
			   }
			   else 
			   {
				   debug ("Now processing user-initiated packets.\nPress RIGHT SHIFT and RIGHT CONTROL to disable.");
				   process_UserPkts = 1;
			   }
			   shift_down = 1;
		   }
	   }
	   else
	   {
		   shift_down = 0;
	   }
	   
   }

#endif

#endif

#endif

	/* For fuck's sake... */
	  
	FD_ZERO (&ReadFDs);
	FD_ZERO (&WriteFDs);
	FD_ZERO (&ExceptFDs);

	/* Ship select in progress? */

	for (p=0;p<MAX_PORTS;p++)
	{
		if (listen_active[p] == 1)
		{
			FD_SET (listen_sockfds[p], &ReadFDs);
			FD_SET (listen_sockfds[p], &ExceptFDs);
		}
	}

	/* Got room in the client buffer for server data? */

	if ( client_data_in_buf < BUF_SIZE )
	{
		FD_SET (s_sockfd, &ReadFDs);
	}

	/* If we have data to send to the server, see if we can write it this turn around */

	if ( server_data_in_buf > 0 )
	{
		FD_SET (s_sockfd, &WriteFDs);
	}

	FD_SET (s_sockfd, &ExceptFDs);

	/* Got room in the server buffer for client data? */

	if ( server_data_in_buf < BUF_SIZE )
	{
		FD_SET (pc_sockfd, &ReadFDs);
	}

	/* If we have data to send to the client, see if we can write it this turn around */

	if ( client_data_in_buf > 0 )
	{
		FD_SET (pc_sockfd, &WriteFDs);
	}

	FD_SET (pc_sockfd, &ExceptFDs);

	/* Check sockets for activity. */

	if ( select ( 0, &ReadFDs, &WriteFDs, &ExceptFDs, &pso_timeout ) > 0 ) 
	{
		/* Client activity? */

		if ( FD_ISSET (pc_sockfd, &ExceptFDs) )
		{
			debug_perror ("Client socket error");
			exit (1);
			FD_CLR ((unsigned) pc_sockfd, &ExceptFDs);
		}
		else
		{
			if (FD_ISSET (pc_sockfd, &ReadFDs) )
			{

				cipher_ptr = &client_to_server;

				/* Read client data */

				if ( (pkt_len = recv (pc_sockfd, &encrypt_buf[0], BUF_SIZE - server_data_in_buf - 1, 0)) <= 0)
				{
					/* Fail 
					debug ("client::recv() Received and ignored invalid packet from the client? (%i)\n", (int) pkt_len); */
				}
				else
				{
					if (crypt_on == 1)
					{
						encrypt_idx = 0;

						while ( pkt_len )
						{
							/* Three step process. */

							/* STEP 1: Grab four bytes. */

							if ( c_got_header == 0 )
							{
								if ( pkt_len > c_header_left )
									ch = c_header_left; else
									ch = pkt_len;
								memcpy ( &c_encrypt_buf[c_header_idx], &encrypt_buf[encrypt_idx], ch );
								c_header_left -= ch;
								c_header_idx += ch;
								encrypt_idx += ch;
								pkt_len -= ch;
								if ( c_header_left == 0 )
									c_got_header = 1;
							}

							/* STEP 2: Decrypt to grab header and expected size of data. */
							
							if ( c_got_header == 1 )
							{
								cipher_ptr = &client_to_server;
								decryptcopy ( &c_exam_buf[0], &c_encrypt_buf[0], 4 );
								c_exam_data_in_buf = 4;
								c_header_left = *(unsigned short *) &c_exam_buf[2];
								c_header_left -= 4;
								cpkt_bytes_left = c_header_left;
								c_header_idx = 0;
								c_got_header = 2;
							}

							/* STEP 3: Now grab data, rounding up by 4. */
							
							if ( c_got_header == 2 )
							{
								if ( pkt_len > c_header_left )
									ch = c_header_left; else 
									ch = pkt_len;
								memcpy ( &c_encrypt_buf[c_header_idx], &encrypt_buf[encrypt_idx], ch );
								c_header_left -= ch;
								c_header_idx += ch;
								encrypt_idx += ch;
								pkt_len -= ch;
								if ( c_header_left == 0 )
								{
									c_got_header = 0;
									c_header_left = 4;
									c_header_idx = 0;
									if ( cpkt_bytes_left )
									{
										c_exam_data_in_buf += cpkt_bytes_left;
										cipher_ptr = &client_to_server;
										decryptcopy (&c_exam_buf[4], &c_encrypt_buf[0], cpkt_bytes_left );
									}
									client_examine_buffer();
								}
							}
						}
					}
					else
					{
						debug ("Client (%u):", pkt_len);

						display_packet ( &encrypt_buf[0], pkt_len );

					    /* Data received from client needs to be sent to the server. */

						memcpy ( &server_buf[server_data_in_buf], &encrypt_buf[0], pkt_len );

						server_data_in_buf += pkt_len;

					}

				}

				FD_CLR ((unsigned) pc_sockfd, &ReadFDs);
			}

			if (FD_ISSET (pc_sockfd, &WriteFDs) )
			{

				/* Write client data */

				/*debug ("Data sent to client (%u):", client_data_in_buf);

				display_packet (&client_buf[0], client_data_in_buf );*/

				bytes_sent = send (pc_sockfd, &client_buf[0], client_data_in_buf, 0);

				if (bytes_sent == SOCKET_ERROR)
				{
					wserror = WSAGetLastError();
					debug_perror ("client:send() failed!");
					debug ("Socket Error %u in an attempt to send %u bytes.", wserror, client_data_in_buf );
					exit (1);
				}
				else
				{
					//debug ("client::send(): Bytes sent %i out of %u\n", bytes_sent, client_data_in_buf);
					if ( bytes_sent < client_data_in_buf)
					{
						client_data_in_buf -= bytes_sent;
						memmove (&client_buf[0], &client_buf[bytes_sent], client_data_in_buf);
						debug ("client::send(): Not all data sent.");
					}
					else
					{
						client_data_in_buf = 0;  /* All sent. */
					}
				}

				FD_CLR ((unsigned) pc_sockfd, &WriteFDs);
			}
		}

		/* Server activity? */

		if ( FD_ISSET (s_sockfd, &ExceptFDs) )
		{
			debug_perror ("Server socket error");
			exit (1);
			FD_CLR ((unsigned) s_sockfd, &ExceptFDs);
		}
		else
		{
			if (FD_ISSET (s_sockfd, &ReadFDs) )
			{
				cipher_ptr = &server_to_client;

				/* Read server data */

				if ( (pkt_len = recv (s_sockfd, &encrypt_buf[0], BUF_SIZE - client_data_in_buf - 1, 0)) <= 0)
				{
					/* Fail
					debug ("server::recv() Received and ignored invalid packet from the server? (%i)\n", pkt_len); */
				}
				else
				{
					if (crypt_on == 1)
					{
						encrypt_idx = 0;

						while ( pkt_len )
						{
							/* Three step process. */

							/* STEP 1: Grab four bytes. */

							if ( got_header == 0 )
							{
								if ( pkt_len > header_left )
									ch = header_left; else
									ch = pkt_len;
								memcpy ( &s_encrypt_buf[header_idx], &encrypt_buf[encrypt_idx], ch );
								header_left -= ch;
								header_idx += ch;
								encrypt_idx += ch;
								pkt_len -= ch;
								if ( header_left == 0 )
									got_header = 1;
							}

							/* STEP 2: Decrypt to grab header and expected size of data. */
							
							if ( got_header == 1 )
							{
								cipher_ptr = &server_to_client;
								decryptcopy ( &exam_buf[0], &s_encrypt_buf[0], 4 );
								exam_data_in_buf = 4;
								header_left = *(unsigned short *) &exam_buf[2];
								header_left -= 4;
								pkt_bytes_left = header_left;
								header_idx = 0;
								got_header = 2;
							}

							/* STEP 3: Now grab data, rounding up by 4. */
							
							if ( got_header == 2 )
							{
								if ( pkt_len > header_left )
									ch = header_left; else 
									ch = pkt_len;
								memcpy ( &s_encrypt_buf[header_idx], &encrypt_buf[encrypt_idx], ch );
								header_left -= ch;
								header_idx += ch;
								encrypt_idx += ch;
								pkt_len -= ch;
								if ( header_left == 0 )
								{
									got_header = 0;
									header_left = 4;
									header_idx = 0;
									if ( pkt_bytes_left )
									{
										exam_data_in_buf += pkt_bytes_left;
										cipher_ptr = &server_to_client;
										decryptcopy (&exam_buf[4], &s_encrypt_buf[0], pkt_bytes_left );
									}
									server_examine_buffer();
								}
							}
						} 
					}
					else
					{
						encrypt_idx = 0;

						while ( pkt_len )
						{
							/* Three step process. */

							/* STEP 1: Grab bytes. */

							if ( got_header == 0 )
							{
								if ( pkt_len > header_left )
									ch = header_left; else
									ch = pkt_len;
								memcpy ( &s_encrypt_buf[header_idx], &encrypt_buf[encrypt_idx], ch );
								header_left -= ch;
								header_idx += ch;
								encrypt_idx += ch;
								pkt_len -= ch;
								if ( header_left == 0 )
									got_header = 1;
							}

							/* STEP 2: Decrypt to grab header and expected size of data. */
							
							if ( got_header == 1 )
							{
								cipher_ptr = &server_to_client;
								*(unsigned *) &exam_buf[0] = *(unsigned *) &s_encrypt_buf[0];
								exam_data_in_buf = 4;
								header_left = *(unsigned short *) &exam_buf[2];
								header_left -= 4;
								pkt_bytes_left = header_left;
								header_idx = 0;
								got_header = 2;
							}

							/* STEP 3: Now grab data. */
							
							if ( got_header == 2 )
							{
								if ( pkt_len > header_left )
									ch = header_left; else 
									ch = pkt_len;
								memcpy ( &s_encrypt_buf[header_idx], &encrypt_buf[encrypt_idx], ch );
								header_left -= ch;
								header_idx += ch;
								encrypt_idx += ch;
								pkt_len -= ch;
								if ( header_left == 0 )
								{
									got_header = 0;
									header_left = 4;
									header_idx = 0;
									if ( pkt_bytes_left )
									{
										exam_data_in_buf += pkt_bytes_left;
										cipher_ptr = &server_to_client;
										memcpy (&exam_buf[4], &s_encrypt_buf[0], pkt_bytes_left );
									}
									debug ("Server (%u):", exam_data_in_buf);
									display_packet ( &exam_buf[0], exam_data_in_buf );
									unencrypt_examine();
								}
							}
						} 
					}
				}

				FD_CLR ((unsigned) s_sockfd, &ReadFDs);
			}

			if (FD_ISSET (s_sockfd, &WriteFDs) )
			{

				/* Write server data */

				/*debug ("Data sent to server (%u):", server_data_in_buf);

				display_packet (&server_buf[0], server_data_in_buf );*/

				bytes_sent = send (s_sockfd, &server_buf[0], server_data_in_buf, 0);

				if (bytes_sent == SOCKET_ERROR)
				{
					wserror = WSAGetLastError();
					debug_perror ("server:send() failed!");
					debug ("Socket Error %u in an attempt to send %u bytes.", wserror, server_data_in_buf);
					exit (1);
				}
				else
				{
					//debug ("server::send(): Bytes sent %i out of %u\n", bytes_sent, server_data_in_buf);
					if ( bytes_sent < server_data_in_buf)
					{
						server_data_in_buf -= bytes_sent;
						memmove (&server_buf[0], &server_buf[bytes_sent], server_data_in_buf);
						debug ("server::send(): Not all data sent.");
					}
					else
					{
						server_data_in_buf = 0;  /* All sent. */
					}
				}

				FD_CLR ((unsigned) s_sockfd, &WriteFDs);
			}
		}

		/* Listener activity? */

		for (p=0;p<MAX_PORTS;p++)
		{
			if ( (listen_active[p] == 1) && ( FD_ISSET (listen_sockfds[p], &ExceptFDs) ) )
			{
				debug_perror ("Listener socket error");
				exit (1);
				FD_CLR ((unsigned) listen_sockfds[p], &ExceptFDs);
			}
			else 
				if ( listen_active[p] == 1 )
				{
					//debug ("Port open %u", listen_ports[p] );
					if (FD_ISSET (listen_sockfds[p], &ReadFDs) )
					{
						my_used_port = listen_ports[p];

						/* Accept connection. */

						csa_len = sizeof (csa);

						tmp_sockfd = tcp_accept ( listen_sockfds[p], (struct sockaddr*) &csa, &csa_len );

						debug ("Accepted connection from %s:%u", inet_ntoa (csa.sin_addr), csa.sin_port );

						*(unsigned *) &compare_addr = *(unsigned *) &csa.sin_addr.S_un.S_addr;

						if ( compare_addr != client_addr )
						{
							debug ("Connection from the wrong user?  Doing nothing.");
							closesocket (tmp_sockfd);
						}
						else
						{
							/* Close old listening socket. */
							
							for (ch=0;ch<MAX_PORTS;ch++)
							{	
								if (listen_active[ch] == 2)
								{
									debug ("Closing old listening socket.");
									closesocket(p_sockfd);
									listen_active[ch] = 0;
								}
							}
							
							/* Closing old proxy client socket. */
							
							debug ("Closing old proxy client socket.");
							
							closesocket(pc_sockfd);
							
							/* Copy socket. */
							
							pc_sockfd = tmp_sockfd;
							
							/* Closing old proxy server socket. */
							
							debug ("Closing old proxy server socket.");
							
							closesocket (s_sockfd);
							
							/* Connect to the server */
							
							s_sockfd = tcp_sock_connect ( inet_ntoa (server_addrs[p]), server_ports[p] );

							*(unsigned *) &last_used_addr[0] = *(unsigned *) &server_addrs[p].s_addr;
							last_used_port = server_ports[p];
							
							/* Copy socket */
							
							p_sockfd = listen_sockfds[p];
							
							/* Set state to accepted connection. */
							
							listen_active[p] = 2;
						
							// Need to turn encryption off to receive new keys.
							
							crypt_on = 0;
						}

						FD_CLR ((unsigned) listen_sockfds[p], &ExceptFDs);
					}
				}		
		}

	}


	if (program_exit == 1)
	{
		debug ("Client has requested to exit.");

		/* Close all sockets */
		closesocket (p_sockfd);
		closesocket (pc_sockfd);
		closesocket (s_sockfd);
		break;
	}

  } 

  return 0;
}

void tcp_listen (int sockfd)
{
	if (listen(sockfd, 10) < 0)
	{
		debug_perror ("Could not listen for connection");
		exit(1);
	}
}

int tcp_accept (int sockfd, struct sockaddr *client_addr, int *addr_len )
{
	int fd;

	if ((fd = accept (sockfd, client_addr, addr_len)) < 0)
	{
		debug_perror ("Could not accept connection");
		exit(1);
	}

	return (fd);
}

int tcp_sock_connect(char* dest_addr, int port)
{
  int fd;
  struct sockaddr_in sa;
  
  /* Clear it out */
  memset((void *)&sa, 0, sizeof(sa));
    
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

  /* Error */
  if( fd < 0 ){
	 debug_perror("Could not create socket");
	 exit(1);
  } 

  memset (&sa, 0, sizeof(sa));
  sa.sin_family = AF_INET;
  sa.sin_addr.s_addr = inet_addr (dest_addr);
  sa.sin_port = htons((unsigned short) port);

  if (connect(fd, (struct sockaddr*) &sa, sizeof(sa)) < 0)
  {
	  debug_perror("Could not make TCP connection");
	  exit(1);
  } 

 //debug ("tcp_sock_connect %s:%u", inet_ntoa (sa.sin_addr), sa.sin_port );

  return(fd);
}

/*****************************************************************************/
int tcp_sock_open(struct in_addr ip, int port)
{
  int fd, turn_on_option_flag = 1, rcSockopt;

  struct sockaddr_in sa;
  
  /* Clear it out */
  memset((void *)&sa, 0, sizeof(sa));
    
  fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

  /* Error */
  if( fd < 0 ){
	 debug_perror("Could not create socket");
	 exit(1);
  } 

  sa.sin_family = AF_INET;
  memcpy((void *)&sa.sin_addr, (void *)&ip, sizeof(struct in_addr));
  sa.sin_port = htons((unsigned short) port);
  
  /* Reuse port (ICS?) */

  rcSockopt = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *) &turn_on_option_flag, sizeof(turn_on_option_flag));

  /* bind() the socket to the interface */
  if (bind(fd, (struct sockaddr *)&sa, sizeof(struct sockaddr)) < 0){
	 debug_perror("Could not bind to port");
	 exit(1);
  }

  return(fd);
}

/*****************************************************************************/
int tcp_packet_write(int sockfd, struct tcp_packet *tcp_pkt)
{
  struct sockaddr_in sa;

  /* Zero it out */
  memset((void *)&sa, 0, sizeof(sa));

  /* Fill in the information */
  sa.sin_addr.s_addr = tcp_pkt->src_ip.s_addr;
  sa.sin_port = htons((unsigned short) tcp_pkt->src_port);
  sa.sin_family = AF_INET;

  return(sendto(sockfd, (const char *)&tcp_pkt->buf[0], tcp_pkt->len, 0, 
					 (struct sockaddr *)&sa, sizeof(sa)));
}
/*****************************************************************************/
int tcp_packet_read(int sockfd, struct tcp_packet *tcp_pkt )
{
  int numread;
  struct sockaddr_in sa;
  int salen;

  /* Read in the actual packet */
  salen = sizeof(sa);
  if ((numread = recvfrom(sockfd, (char *)&tcp_pkt->buf[0], sizeof(tcp_pkt->buf),0,
	(struct sockaddr *)&sa, &salen)) < 0) {
    debug_perror("tcp_read_read: recvfrom");
	 debug("tcp_read_read: recvfrom");
	 exit(1);
    return -1;
  }

  /* Then record where the packet came from */
  memcpy((void *)&tcp_pkt->src_ip, (void *)&sa.sin_addr, sizeof(struct in_addr));
  tcp_pkt->src_port = ntohs(sa.sin_port);

  tcp_pkt->len = numread;

  return numread;
}

/*****************************************************************************/
int udp_sock_open(struct in_addr ip, int port)
{
  int fd, turn_on_option_flag = 1, rcSockopt;
  struct sockaddr_in sa;
  
  /* Clear it out */
  memset((void *)&sa, 0, sizeof(sa));
    
  fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  /* Error */
  if( fd < 0 ){
	 debug_perror("Could not create socket");
	 exit(1);
  } 

  sa.sin_family = AF_INET;
  memcpy((void *)&sa.sin_addr, (void *)&ip, sizeof(struct in_addr));
  sa.sin_port = htons((unsigned short) port);

  /* Reuse port (ICS?) */

  rcSockopt = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *) &turn_on_option_flag, sizeof(turn_on_option_flag));
  
  /* bind() the socket to the interface */
  if (bind(fd, (struct sockaddr *)&sa, sizeof(struct sockaddr)) < 0){
	 debug_perror("Could not bind to port");
	 exit(1);
  }

  return(fd);
}

/*****************************************************************************/
int udp_packet_write(int sockfd, struct udp_packet *udp_pkt)
{
  struct sockaddr_in sa;

  /* Zero it out */
  memset((void *)&sa, 0, sizeof(sa));

  /* Fill in the information */
  sa.sin_addr.s_addr = udp_pkt->src_ip.s_addr;
  sa.sin_port = htons((unsigned short) udp_pkt->src_port);
  sa.sin_family = AF_INET;

  return(sendto(sockfd, (const char *)&udp_pkt->dns_hdr, udp_pkt->len, 0, 
					 (struct sockaddr *)&sa, sizeof(sa)));
}
/*****************************************************************************/
int udp_packet_read(int sockfd, struct udp_packet *udp_pkt )
{
  int numread;
  struct sockaddr_in sa;
  int salen;

  /* Read in the actual packet */
  salen = sizeof(sa);
  if ((numread = recvfrom(sockfd, (char *)&udp_pkt->dns_hdr, sizeof(udp_pkt->buf),0,
	(struct sockaddr *)&sa, &salen)) < 0) {
    debug_perror("udp_read_read: recvfrom");
	 debug("udp_read_read: recvfrom");
    return -1;
  }


  /* Then record where the packet came from */
  memcpy((void *)&udp_pkt->src_ip, (void *)&sa.sin_addr, sizeof(struct in_addr));
  udp_pkt->src_port = ntohs(sa.sin_port);

  udp_pkt->len = numread;

  return numread;
}
/*****************************************************************************/
/* Checks the QR bit from a dns query
 */
int dns_is_query(struct udp_packet *udp_pkt)
{
	return ( !((udp_pkt->buf[ strlen(udp_pkt->buf) +2 ] & 0x80)/0x80));
}
/*****************************************************************************/
/* Extracts the opcode from a dns query
 */
int dns_get_opcode(struct udp_packet *udp_pkt)
{
	return ( ((udp_pkt->buf[ strlen(udp_pkt->buf) +2 ]) & 0x38)/8);
}
/*****************************************************************************/
/* Change query flag field to response flag field
 */
int dns_set_flags(int err, struct udp_packet *udp_pkt)
{
	/* Keep opcode and rd flags set all others to 0 */
	udp_pkt->dns_hdr.dns_flags=udp_pkt->dns_hdr.dns_flags & 0x7900;
	/* Set QR to response and RA to recursion available */
	udp_pkt->dns_hdr.dns_flags=udp_pkt->dns_hdr.dns_flags | 0x8080;
	if (err) {
		/* Set RCODE to refused.
		   maybe this should be 0x0002=Server failure??? */
		udp_pkt->dns_hdr.dns_flags=udp_pkt->dns_hdr.dns_flags | 0x0005;
	
	}
	return 0;
}
/*****************************************************************************/
/* adds the data to the paket buff and updates the pointer to the end of the 
	packet_buf */
void packet_append(struct udp_packet *pkt, void *data, int data_len)
{
  memcpy( (char*) &pkt->dns_hdr + pkt->len, data, data_len);
  pkt->len += data_len;
}
/*****************************************************************************/
void dns_send_reply(int sockfd, struct udp_packet *pkt, char *query)
{
  struct dns_answer dns_ans;
  struct in_addr ip;
  
 /* do lookup */

  *(unsigned *) &ip.s_addr = *(unsigned *) &proxy_addr;
  debug("dns_send_reply %s", inet_ntoa( ip ) ); 

  dns_set_flags(0, pkt); /* set flags to success */
  pkt->dns_hdr.dns_no_answers  = htons(1);

  dns_ans.dns_name = htons(0xc00c);
  dns_ans.dns_type = htons(1);   //Host name 
  dns_ans.dns_class = htons(1);  //inet
  dns_ans.dns_time_to_live = htons(60);
  dns_ans.dns_data_len = htons(4);

  packet_append( pkt, &dns_ans, sizeof(dns_ans) );
  packet_append( pkt, &ip, sizeof(ip));

  udp_packet_write(sockfd, pkt);	
  
}

/*****************************************************************************/
int inet_aton(const char *cp, struct in_addr *addr)
{
        unsigned val;
        int base, n;
        char c;
        unsigned int parts[4];
        unsigned int *pp = parts;

        c = *cp;
        for (;;) {
                /*
                 * Collect number up to ``.''.
                 * Values are specified as for C:
                 * 0x=hex, 0=octal, isdigit=decimal.
                 */
                if (!isdigit(c))
                        return (0);
                val = 0; base = 10;
                if (c == '0') {
                        c = *++cp;
                        if (c == 'x' || c == 'X')
                                base = 16, c = *++cp;
                        else
                                base = 8;
                }
                for (;;) {
                        if (isascii(c) && isdigit(c)) {
                                val = (val * base) + (c - '0');
                                c = *++cp;
                        } else if (base == 16 && isascii(c) && isxdigit(c)) {
                                val = (val << 4) |
                                        (c + 10 - (islower(c) ? 'a' : 'A'));
                                c = *++cp;
                        } else
                                break;
                }
                if (c == '.') {
                        /*
                         * Internet format:
                         *        a.b.c.d
                         *        a.b.c        (with c treated as 16 bits)
                         *        a.b        (with b treated as 24 bits)
                         */
                        if (pp >= parts + 3)
                                return (0);
                        *pp++ = val;
                        c = *++cp;
                } else
                        break;
        }
        /*
         * Check for trailing characters.
         */
        if (c != '\0' && (!isascii(c) || !isspace(c)))
                return (0);
        /*
         * Concoct the address according to
         * the number of parts specified.
         */
        n = pp - parts + 1;
        switch (n) {

        case 0:
                return (0);                /* initial nondigit */

        case 1:                                /* a -- 32 bits */
                break;

        case 2:                                /* a.b -- 8.24 bits */
                if ((val > 0xffffff) || (parts[0] > 0xff))
                        return (0);
                val |= parts[0] << 24;
                break;

        case 3:                                /* a.b.c -- 8.8.16 bits */
                if ((val > 0xffff) || (parts[0] > 0xff) || (parts[1] > 0xff))
                        return (0);
                val |= (parts[0] << 24) | (parts[1] << 16);
                break;

        case 4:                                /* a.b.c.d -- 8.8.8.8 bits */
                if ((val > 0xff) || (parts[0] > 0xff) || (parts[1] > 0xff) || (parts[2] > 0xff))
                        return (0);
                val |= (parts[0] << 24) | (parts[1] << 16) | (parts[2] << 8);
                break;
        }
        if (addr)
                addr->s_addr = htonl(val);
        return (1);
}
/*****************************************************************************/
void dns_reverse_lookup( int sockfd, struct udp_packet *pkt, char *query )
{
  struct dns_answer dns_ans;
  struct hostent *host = NULL;
  struct in_addr ip;
  char *name, encoded_name[BUF_SIZE];

  reverse_domain_name(query);

 
  debug("Reversed: %s", query);

	 /* do lookup */
	 inet_aton( query, &ip);
	 host = gethostbyaddr( (char *)&ip, sizeof(ip), AF_INET );
	 
	 if( !host ){
		debug( "dns_reverse_lookup: host not found");
		dns_set_flags(1, pkt);
		udp_packet_write(sockfd, pkt);
		return;
	 }else{	

		name = host->h_name;
		debug("dns_reverse_lookup: %s", inet_ntoa( ip ) );
	 }

  encode_domain_name( name, encoded_name );

  dns_set_flags(0, pkt); /* set flags to success */
  pkt->dns_hdr.dns_no_answers  = htons(1);

  dns_ans.dns_name = htons(0xc00c);
  dns_ans.dns_type = htons(0x000c); //Domain name pointer
  dns_ans.dns_class = htons(1);     //inet
  dns_ans.dns_time_to_live = htons(60);
  dns_ans.dns_data_len = htons( (unsigned short) (strlen(encoded_name)+1) );

  packet_append( pkt, &dns_ans, sizeof(dns_ans) );
  
  packet_append( pkt, encoded_name, strlen(encoded_name) +1 );

  udp_packet_write(sockfd, pkt);
}
/*****************************************************************************/
/* this function encode the plain string in name to the domain name encoding 
 * see decode_domain_name for more details on what this function does. */
void encode_domain_name(char *name, char encoded_name[BUF_SIZE])
{
  int i,j,k,n;

  k = 0; /* k is the index to temp */
  i = 0; /* i is the index to name */
  while( name[i] ){

	 /* find the dist to the next '.' or the end of the string and add it*/
	 for( j = 0; name[i+j] && name[i+j] != '.'; j++);
	 encoded_name[k++] = j;

	 /* now copy the text till the next dot */
	 for( n = 0; n < j; n++)
		encoded_name[k++] = name[i+n];
	
	 /* now move to the next dot */ 
	 i += j + 1;

	 /* check to see if last dot was not the end of the string */
	 if(!name[i-1])break;
  }
  encoded_name[k++] = 0;
}
/*****************************************************************************/
/* reverse lookups encode the IP in reverse, so we need to turn it around
 * example 2.0.168.192.in-addr.arpa => 192.168.0.2
 * this function only returns the first four numbers in the IP
 *  NOTE: it assumes that name points to a buffer BUF_SIZE long
 */
void reverse_domain_name(char name[BUF_SIZE])
{
  char temp[BUF_SIZE];
  int i,j,k;
  
  k = 0;
  
  for( j = 0, i = 0; j<3; i++) if( name[i] == '.' )j++;
  for( ; name[i] != '.'; i++) temp[k++] = name[i];
  temp[k++] = '.';

  name[i] = 0;

  for( j = 0, i = 0; j<2; i++) if( name[i] == '.' )j++;
  for( ; name[i] != '.'; i++) temp[k++] = name[i];
  temp[k++] = '.';

  for( j = 0, i = 0; j<1; i++) if( name[i] == '.' )j++;
  for( ; name[i] != '.'; i++) temp[k++] = name[i];
  temp[k++] = '.';

  for( i = 0; name[i] != '.'; i++) temp[k++] = name[i];
  
  temp[k] = 0;

  strcpy( name, temp );
}
/*****************************************************************************
 * same as debug_perror but writes to debug output.
 * 
*****************************************************************************/
void debug_perror( char * msg ) {
	debug( "%s : %s\n" , msg , strerror(errno) );
}
/*****************************************************************************/
void debug(char *fmt, ...)
{
#define MAX_MESG_LEN BUF_SIZE*4
  
	 va_list args;
	 char text[ MAX_MESG_LEN ];

	 FILE *fp;
	 
	 va_start (args, fmt);
	 strcpy (text + vsprintf( text,fmt,args), "\r\n"); 
	 va_end (args);

	 fp = fopen ( &log_file_name[0], "a");
	 if (!fp)
	 {
		 printf ("Unable to log to %s\n", &log_file_name[0]);
	 }
	 fprintf (fp, "%s", text);
	 fclose (fp);
	 
  fprintf( stderr, "%s", text);
}
/*****************************************************************************/
/* Queries are encoded such that there is and integer specifying how long 
 * the next block of text will be before the actuall text. For eaxmple:
 *             www.linux.com => \03www\05linux\03com\0
 * This function replaces the query in the array with its dot '.' seperated
 * equivalent. */
void decode_domain_name(char query[BUF_SIZE])
{
  char temp[BUF_SIZE];
  int i, k, len, j;

  i = 0;
  k = 0;
  while( query[i] ){
         len = query[i];
         i++;
         for( j = 0; j<len ; j++)
                temp[k++] = query[i++];
         temp[k++] = '.';
  }
  temp[k-1] = 0;
  strcpy( query, temp );
}

/*****************************************************************************/
/* Encryption procedures start
 */

// Mangle the table; this resets current
void CALLING gcmangle(void)
{
	DWORD *r5, *r6, *r7;

	cipher_ptr->current = &cipher_ptr->table[0];
	r5 = &cipher_ptr->table[0];
	r7 = &cipher_ptr->table[0];

	r6 = &r7[MANGLE_MARK];

	while (r6 != &cipher_ptr->table[TABLE_SIZE])
		*r5++ ^= *r6++;

	while (r5 != &cipher_ptr->table[TABLE_SIZE])
		*r5++ ^= *r7++;
}


// Initialize the cipher with a particular seed
void CALLING gcinitialize(const BYTE *key, size_t keylen)
{
	DWORD r7, r8, ctr, salt;
	DWORD *pr7, *pr8, *pr9;
	
	if (keylen < sizeof(DWORD))
	{
		perror ("key size less than 4");
		exit (1);
	}

	salt = *(unsigned *) key;

	// Reset next offset
	cipher_ptr->next_offset = 0;

	// Store the seed for later
	cipher_ptr->seed = salt;

	// Set the current pointer to the beginning
	cipher_ptr->current = &cipher_ptr->table[0];

	// Generate 17 random values
	for (r7 = 0, r8 = 0; r8 <= 16; r8++)
	{
		// Generate random bits for r7 by using the high bit of the product
		// of a prime number and the original seed.  The salt++ appears to
		// be there to solve the problem of a 00000000 salt producing weird
		// results.
		for (ctr = 0; ctr < 32; ctr++)
		{
			salt *= MAGIC;
			salt++;
			r7 = (r7 >> 1) | (salt & 0x80000000);
		}

		// Add it to the table
		*cipher_ptr->current++ = r7;
	}

	// I have no idea what this is about, but we need it.
	cipher_ptr->current--;
	*cipher_ptr->current = (*cipher_ptr->current << 23) ^ (cipher_ptr->table[0] >> 9) ^ cipher_ptr->table[15];

	// Set up pointers for the rest of the table init
	pr7 = &cipher_ptr->table[0];
	pr8 = &cipher_ptr->table[1];
	pr9 = cipher_ptr->current++;

	while (cipher_ptr->current != &cipher_ptr->table[TABLE_SIZE])
		*cipher_ptr->current++ = (*pr7++ << 23) ^ (*pr8++ >> 9) ^ *pr9++;

	// Mangle 3 times
	cipher_ptr->mangle();
	cipher_ptr->mangle();
	cipher_ptr->mangle();

	// Initialize the "next" value.  The original does not do this.
	cipher_ptr->mangle();
	cipher_ptr->next = *cipher_ptr->current;
}


// Advances to the next DWORD, setting "next"
void CALLING gcadvance(void)
{
	if (++cipher_ptr->current >= &cipher_ptr->table[TABLE_SIZE])
		cipher_ptr->mangle();

	cipher_ptr->next = *cipher_ptr->current;
	cipher_ptr->next_offset = 0;
}


// Encrypts a stream of bytes while copying.
// This routine is common to both the Dreamcast and Gamecube ciphers.

void encryptcopy(BYTE *dest, const BYTE *src, DWORD size)
{
	BYTE* cipher_key;

	cipher_key = (BYTE*) &cipher_ptr->next;
	cipher_key += cipher_ptr->next_offset;

	/* Decrypt a byte at a time, advancing the key index every 4 bytes. */

	while (size)
	{
		*dest = *src ^ *cipher_key;
		dest++;
		src++;
		cipher_key++;
		cipher_ptr->next_offset++;
		if (cipher_ptr->next_offset == 4)
		{
			cipher_ptr->advance();
			cipher_key = (BYTE*) &cipher_ptr->next;
		}
		size--;
	}
	if (cipher_ptr->next_offset)
		cipher_ptr->advance();
}


// Encrypt a stream of bytes
void encrypt(BYTE *data, DWORD size)
{
	encryptcopy(data, data, size);
}


// Decrypt a stream of bytes
void decrypt(BYTE *data, DWORD size)
{
	encryptcopy(data, data, size);
}


// Decrypt a stream of bytes while copying
void decryptcopy(BYTE *dest, const BYTE *src, DWORD size)
{
	encryptcopy(dest, src, size);
}


// Decrypt a single DWORD without affecting the stream
void peek_decrypt(DWORD *data)
{
	*data = htodl(dtohl(*data) ^ cipher_ptr->next);
}

void send_to_server(int sock, char* packet)
{
 int pktlen;

 pktlen = strlen (packet);

	if (send(sock, packet, pktlen, 0) != pktlen)
	{
	  debug_perror ("send_to_server(): failure");
	  exit(1);
	}

}

int receive_from_server(int sock, char* packet)
{
 int pktlen;

  if ((pktlen = recv(sock, packet, BUF_SIZE - 1, 0)) <= 0)
	{
	  debug_perror ("receive_from_server(): failure");
	  exit(1);
	}
  packet[pktlen] = 0;
  return pktlen;
}
